#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 09:38:20 2022

@author: samy
"""

import bert_model_train as bmt
import util as ut

from deep_translator import GoogleTranslator
import matplotlib.pyplot as plt
from collections import Counter
import seaborn as sns
import pandas as pd
import numpy as np
import collections
import random
import pickle
import json
import time
import html
import copy
import csv
import os
import re
import networkx as nx


all_topics = ['bolsonaro27',
              'menciones-20-27marzo',
              'impeachment-5-10',
              'menciones-1-10enero',
              'mothersday', #do not use it
              'area51', 
              'OTDirecto20E',
              'bolsonaro28', 
              'VanduMuruganAJITH',
              'nintendo',
              'messicumple', 
              'wrestlemania',
              'kingjacksonday',
              'kavanaugh06-08',
              'bolsonaro30', 
              'notredam',
              'Thanksgiving',
              'halsey', 
              'feliznatal', 
              'kavanaugh16',
              'kavanaugh02-05',
              'EXODEUX', 
              'bigil', 
              'lula_moro_chats',
              'menciones-05-11abril',
              'LeadersDebate',
              'championsasia', 
              'menciones05-11mayo',
              'pelosi', 
              'SeungWooBirthday',
              'menciones-11-18marzo']
# all_topics = ['pelosi', 'SeungWooBirthday']

code_xp_LABEL_QC = 'LC_8A'

def clean_text_v2(tweet):
    #remove retweet
    tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
    
    #remove URL
    tweet = re.sub(r'http\S+', r'URL', tweet)
    
    # # remove @
    tweet = re.sub(r'@[^\s]+',r'HASHTAG', tweet)
    
    #remove non-ascii words and characters
    # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
    # tweet = ''.join(tweet)
    # tweet = re.sub(r'_[\S]?',r'', tweet)
    # tweet = re.sub(r'\n',r' ', tweet)
    
    # lower case and strip white spaces at both ends
    # tweet = tweet.lower()
    # tweet = tweet.strip()
    
    tweet = html.unescape(tweet)
        
    return tweet

"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df
    
"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels

def plot_cum_distr(x, l):
    n_bins = 100
    fig, ax = plt.subplots(figsize=(8, 4))
    
    # plot the cumulative histogram
    for xx, ll in zip(x,l):
        n, bins, patches = ax.hist(xx, n_bins, density=True, histtype='step',
                                    cumulative=True, label='label='+ str(ll))        
    # tidy up the figure
    ax.grid(True)
    ax.legend(loc='right')
    ax.set_title('Cumulative step histograms')
    ax.set_xlabel('Active user score')
    ax.set_ylabel('Likelihood of occurrence')
    
    plt.show()
    return 1


if __name__ == "__main__":
    print('-------- START 1 --------')
    cpt=1
    t1 = time.time()
    for topic in all_topics:
        print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
        from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + '/data_ZARATE_translated_v3/' + topic + '.csv' #data_ZARATE_translated_v2
        to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_ZARATE_for_analysis/' + topic + '.csv'
        
        data_obj_path = ut.CURRENT_DATA_FOLD + ut.object_folder + 'S_1A/' + topic + '/RETWEET/data.pickle'
        
        label_qc_path = ut.CURRENT_DATA_FOLD + ut.object_folder + 'LABELS/' +code_xp_LABEL_QC + '/' + topic + '/RETWEET/' + topic+'_labels_'+code_xp_LABEL_QC+'.json'
        
        if(os.path.isfile(to_path) == True):
            print(' <<<<<< data already created >>>>>>')
        else:
            #1. load data
            print("1. load data")
            if(os.path.isfile(data_obj_path) == True):
                _, dict_nodes, _, dict_new_nodes, _, dict_user_to_id, dict_id_to_user, _, dict_node_label_metis, _, _, _ = load_pickle(data_obj_path)
                dict_user_label = {dict_id_to_user[k]:v for k,v in dict_node_label_metis.items()}
                
            dict_label_unsupervised = {}
            if(os.path.isfile(label_qc_path) == True):
                  dict_label_unsupervised = load_json(label_qc_path)
                 
            if(os.path.isfile(from_path) == True):   
                init_df = load_data_csv(from_path)
            
            final_df = pd.DataFrame({'topic': [],
                                      'user_id': [],
                                      'tweet_id': [],
                                      'is_retweet': [],
                                      'text': [],
                                      'metis_label': [],
                                      'LABEL_QC_label': []})
                        
            #3. add metis labels
            print('2. add metis+unsupervised label')
            not_found_metis = 0
            unsup_usr = []
            
            tweets_done = []
            
            for idx, row in init_df.iterrows():
                still = True
                if(row['is_retweet'] == False):
                    u_id = row['user_id']
                    t_id = row['status_id']
                    is_rt = row['is_retweet']
                    text = row['text_translated']
                    
                    if(t_id in tweets_done):
                        still = False
                    else:
                        #metis label
                        if(u_id in dict_user_label.keys()):
                            metis_label = dict_user_label[u_id]
                        else:
                            metis_label = -10
                            not_found_metis+=1
                            
                        #label_qc
                        if(u_id in dict_label_unsupervised.keys()):
                            LABELQC_labels = dict_label_unsupervised[u_id]
                            unsup_usr.append(u_id)
                        else:
                            LABELQC_labels = -20                 
                       
                else:
                    u_id = row['retweet_user_id']
                    t_id = row['retweet_status_id']
                    is_rt = row['is_retweet']
                    text = row['text_translated']
                    
                    if(t_id in tweets_done):
                        still = False
                    else:
                        #metis label
                        if(u_id in dict_user_label.keys()):
                            metis_label = dict_user_label[u_id]
                        else:
                            metis_label = -10
                            not_found_metis+=1
                            
                        #label_qc
                        if(u_id in dict_label_unsupervised.keys()):
                            LABELQC_labels = dict_label_unsupervised[u_id]
                            unsup_usr.append(u_id)
                        else:
                            LABELQC_labels = -20     
                
                if(still == True):
                    final_df.loc[len(final_df)] = [topic, u_id, t_id, is_rt, text, metis_label, LABELQC_labels]
                    tweets_done.append(t_id)
                                    
            print(' > NOT found metis label =', not_found_metis)
            print(' > found unsupervised label =', len(list(set(unsup_usr))))

    
            # 4. write in files
            print('4. write result')
            print('verif size:', len(tweets_done), len(final_df))
            create_csv_file(final_df, to_path)
            
        cpt+=1
        print('--------------------------')
        
    print('-------- END --------')
            

#####################################################################################################
    
# if __name__ == "__main__":
#     print('-------- START 1: concat --------')
#     cpt=1
#     length = 0
#     final_df = None
    
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/TWEETS_ALL_TOPICS.csv'
    
#     #1. concat results
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/' + topic + '.csv'

#         df = load_data_csv(from_path)
#         length += len(df)
        
#         x =  len(df) == len(list(set(df['tweet_id'].tolist())))
#         print('   >', x)
        
#         if(final_df is None):
#             final_df = df
#         else:
#             final_df = pd.concat([final_df, df])
#         cpt+=1
            
            
#     # 2. write in files
#     print('4. write result')
#     print('verif size:', length, len(final_df))
    
#     create_csv_file(final_df, to_path)
            
#####################################################################################################

# if __name__ == "__main__":
#     print('-------- START 2: clean text --------')
#     cpt=1
#     length = 0
#     final_df = None
        
#     #1. concat results
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/' + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis_2/' + topic + '.csv'

#         df = load_data_csv(from_path)
#         new_df = df.copy()
        
#         new_df['clean_text'] = new_df.apply(lambda x: clean_text_v2(x['text']), axis=1)
        
#         cpt+=1
            
#         # 2. write in files
#         print('verif size:', len(df), len(new_df))
    
#         create_csv_file(new_df, to_path)


#####################################################################################################
    
# if __name__ == "__main__":
#     print('-------- START 3: add topic label --------')
#     cpt=1
#     length = 0
#     final_df = None
    
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LIWC.csv'

#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'

#     print('1. load')
#     df = load_data_csv(from_path)
    
#     print('2. apply')
#     df['controversy_LABEL'] = df.apply(lambda x: ut.data_zarate_labels[x['topic']], axis=1)
    
#     print('3. write')
#     create_csv_file(df, to_path)
            

#####################################################################################################

# if __name__ == "__main__":
#     print('-------- START influencers: --------')
    
#     topic = 'pelosi'
#     fraction = 0.01
#     alpha_pr = 0.85
        
    
#     object_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+topic+'/RETWEET/data.pickle'
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_LIWC.csv'
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_INFLUENCER_LIWC.csv'
    
#     dict_tweets, dict_nodes, dict_edges, _, _, _, _, _, _, _, _, _ = load_pickle(object_path)
    
#     df1 = load_data_csv(from_path)
#     df_temp1 = df1.drop(df1[df1['metis_label'] == -10].index)
#     list_user_with_tweets = list(set(df_temp['user_id'].tolist()))

#     list_edges = [v for _,v in dict_edges.items()]
#     G = nx.DiGraph(list_edges)
#     print('G nx : nodes->', len(G.nodes()))
    
#     user_ranking = nx.pagerank(G, alpha=alpha_pr)
#     user_ranking_with_tweets = {k:user_ranking[k] for k in list_user_with_tweets}
#     values = [v for _,v in user_ranking_with_tweets.items()]
    
#     # print(' >> first value:', min(values), 'represent', round((len([v for v in values if(v==min(values))])/len(values))*100, 2), '%')
    
#     # #PLOT DISTRIB
#     # print(' > plot')
#     # to_plot = np.array([v for v in values if(v < 0.00025)])
#     # plot_cum_distr([to_plot], ['alpha:'+str(alpha_pr)])


#     user_ranking = {k: v for k, v in sorted(user_ranking_with_tweets.items(), key=lambda item: item[1], reverse=True)} #sort
#     all_user = list(user_ranking_with_tweets.keys())
#     influencer = all_user[:int(len(all_user)*fraction)]
#     normal = all_user[int(len(all_user)*fraction):]
    
#     total_pr_infl = sum([user_ranking_with_tweets[k] for k in influencer])
#     total_pr_normal = sum([user_ranking_with_tweets[k] for k in normal])
#     total_pr = sum([user_ranking_with_tweets[k] for k in user_ranking.keys()])
    
#     print(' > normal=', round((total_pr_normal/total_pr)*100, 2), '% of total_pageRank (', len(normal), 'users)')
#     print(' > influencer=', round((total_pr_infl/total_pr)*100, 2), '% of total_pageRank (', len(influencer), 'users)')

# # ----------------------------------------------------------------------------------------------------------------------------------


# def get_score_by_idx(x, idx_non_active, idx_active):
#     if(x['score'] == -30):
#         return -30
#     else:
#         if(x.name in idx_non_active):
#             return 0
#         elif(x.name in idx_active):
#             return 1
#         else:
#             return -1

# def get_score(x, dict_score2, non_act_max, act_min):
#     if(x['user_id'] not in dict_score2):
#         return -30
#     else:
#         sc = dict_score2[x['user_id']]
#         if(sc <= non_act_max):
#             return 0
#         elif(sc >= act_min):
#             return 1
#         else:
#             return -1



# """
# define active user: high degree (in an out)
# """
# if __name__ == "__main__":
#     print('-------- START 4: add active label --------')
#     cpt=1
#     topic = 'pelosi'
#     is_by_idx = True
        
#     object_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/pelosi/RETWEET/data.pickle'
#     plot_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/'
    
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_LIWC.csv'
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_ACTIVE_LIWC_v2.csv'
    
#     dict_tweets, dict_nodes, dict_edges, _, _, _, _, _, _, _, _, _ = load_pickle(object_path)
#     df = load_data_csv(from_path)
    
#     df_temp = df.drop(df[df['metis_label'] == -10].index)
    
#     list_user_with_tweets = list(set(df_temp['user_id'].tolist()))
    
#     dict_degree_by_user = {}
#     dict_in_degree_by_user = {}
#     dict_out_degree_by_user = {}
#     print('>start')
#     for k,v in dict_edges.items():  
#         #d_in
#         if(v[0] not in dict_in_degree_by_user.keys()):
#             dict_in_degree_by_user[v[0]] = 0
#         dict_in_degree_by_user[v[0]] += 1
      
#         #d_out
#         if(v[1] not in dict_out_degree_by_user.keys()):
#             dict_out_degree_by_user[v[1]] = 0
#         dict_out_degree_by_user[v[1]] += 1
    
#         #all        
#         if(v[0] not in dict_degree_by_user.keys()):
#             dict_degree_by_user[v[0]] = 0
#         dict_degree_by_user[v[0]] += 1
        
#         if(v[1] not in dict_degree_by_user.keys()):
#             dict_degree_by_user[v[1]] = 0
#         dict_degree_by_user[v[1]] += 1
    
#     print('> add usr not present')
#     not_in=0
#     not_out=0
#     for k in list_user_with_tweets:
#         if(k not in dict_in_degree_by_user.keys()):
#             dict_in_degree_by_user[k] = 0
#             not_in+=1
#             if(not_in==1):
#                 weird = k
#         if(k not in dict_out_degree_by_user.keys()):
#             dict_out_degree_by_user[k] = 0
#             not_out+=1
#     print(' >> not_in:', not_in, '| not_out:', not_out, 'len(list)=', len(list_user_with_tweets))
    
#     dict_nbs_tweets = {}
#     for k,v in dict_nodes.items():
#         if(v is None):
#             dict_nbs_tweets[k] = 0
#         else:
#             dict_nbs_tweets[k] = len(v)
                
#     print('keep user not in list')
#     dict_in_degree_by_user = {k:dict_in_degree_by_user[k] for k in list_user_with_tweets}
#     dict_out_degree_by_user = {k:dict_out_degree_by_user[k] for k in list_user_with_tweets}
#     dict_degree_by_user = {k:dict_degree_by_user[k] for k in list_user_with_tweets}
#     dict_nbs_tweets = {k:dict_nbs_tweets[k] for k in list_user_with_tweets}


#     print('> compute mean & median')
#     arr = [v for _,v in dict_in_degree_by_user.items()]
#     tot_d_in = sum(arr)
#     print('- d_in (',tot_d_in,') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', np.quantile(arr, 0.25), ',',
#           np.quantile(arr, 0.50),',', np.quantile(arr, 0.75),',', np.quantile(arr, 1), ')')
    
#     arr = [v for _,v in dict_out_degree_by_user.items()]
#     tot_d_out = sum(arr)
#     print('- d_out (',tot_d_out,') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', np.quantile(arr, 0.25), ',',
#           np.quantile(arr, 0.50),',', np.quantile(arr, 0.75),',', np.quantile(arr, 1), ')')
    
#     arr = [v for _,v in dict_degree_by_user.items()]
#     tot_d = sum(arr)
#     print('- d (',tot_d,') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', np.quantile(arr, 0.25), ',',
#           np.quantile(arr, 0.50),',', np.quantile(arr, 0.75),',', np.quantile(arr, 1), ')')
    
#     arr = [v for _,v in dict_nbs_tweets.items()]
#     tot_nbs_tweets = sum(arr)
#     print('- nbs_tweets (',tot_nbs_tweets,') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', np.quantile(arr, 0.25), ',',
#           np.quantile(arr, 0.50),',', np.quantile(arr, 0.75),',', np.quantile(arr, 1), ')')
    
#     print('> normalized new measure')
#     in_deg = copy.deepcopy(dict_in_degree_by_user)
#     out_deg = copy.deepcopy(dict_out_degree_by_user)
#     deg = copy.deepcopy(dict_degree_by_user)
#     tweets = copy.deepcopy(dict_nbs_tweets)
    
#     dict_in_degree_by_user = {k:round(v/tot_d_in, 5) for k,v in dict_in_degree_by_user.items()}
#     dict_out_degree_by_user = {k:round(v/tot_d_out, 5) for k,v in dict_out_degree_by_user.items()}
#     dict_degree_by_user = {k:round(v/tot_d, 5) for k,v in dict_degree_by_user.items()}
#     dict_nbs_tweets = {k:round(v/tot_nbs_tweets, 5) for k,v in dict_nbs_tweets.items()}
    
    
#     #######################################
#     print('> create new measure')
#     the_dict = dict_nbs_tweets
#     # l = 0.2
#     # print(' >> l=',l)
#     # dict_score0 = {}
#     # for k,v in the_dict.items():
#     #     dict_score0[k] = (l * v + (1-l)*dict_nbs_tweets[k])*100
#     # arr = [v for _,v in dict_score0.items() if(v < 0.5)]
#     # tot = sum(arr)
#     # print('- dict_',l,'(',round(tot,4),') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', round(np.quantile(arr, 0.25), 4), ',',
#     #       round(np.quantile(arr, 0.50),4),',', round(np.quantile(arr, 0.75),4),',', round(np.quantile(arr, 1),4), ') ++ [',
#     #       round(np.quantile(arr, 0.1),4), ',', round(np.quantile(arr, 0.90),4), '] = ', round(np.quantile(arr, 0.9),4) - round(np.quantile(arr, 0.1),4))
#     # arr0 = arr
    
#     l = 0.5
#     print(' >> l=',l)
#     dict_score2 = the_dict
#     # dict_score2 = {}
#     # for k,v in the_dict.items():
#     #     dict_score2[k] = (l * v + (1-l)*dict_nbs_tweets[k])*100
    
#     arr = [v for _,v in dict_score2.items() if(v<0.001)]
#     non_act_max = round(np.quantile(arr, 0.1),4)
#     act_min = round(np.quantile(arr, 0.9),4)
#     tot = sum(arr)
#     print('- dict_',l,'(',round(tot,4),') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', round(np.quantile(arr, 0.25), 4), ',',
#           round(np.quantile(arr, 0.50),4),',', round(np.quantile(arr, 0.75),4),',', round(np.quantile(arr, 1),4), ') ++ [',
#           round(np.quantile(arr, 0.2),4), ',', round(np.quantile(arr, 0.90),4), '] = ', round(np.quantile(arr, 0.8),4) - round(np.quantile(arr, 0.1),4))
#     arr1 = arr
    
#     # l = 0.8
#     # print(' >> l=',l)
#     # dict_score3 = {}
#     # for k,v in the_dict.items():
#     #     dict_score3[k] = (l * v + (1-l)*dict_nbs_tweets[k])*100
    
#     # arr = [v for _,v in dict_score3.items() if(v < 0.5)]
#     # tot = sum(arr)
#     # print('- dict_',l,'(',round(tot,4),') --> mean:', round(sum(arr)/len(arr), 2), 'quartile: (', round(np.quantile(arr, 0.25), 4), ',',
#     #       round(np.quantile(arr, 0.50),4),',', round(np.quantile(arr, 0.75),4),',', round(np.quantile(arr, 1),4), ') ++ [',
#     #       round(np.quantile(arr, 0.1),4), ',', round(np.quantile(arr, 0.90),4), '] = ', round(np.quantile(arr, 0.9),4) - round(np.quantile(arr, 0.1),4))
#     # arr2 = arr
    
#     plot_cum_distr([arr1], ['nbs_tweets'])
#     # plot_cum_distr([arr0, arr1, arr2], [0.2, 0.5, 0.8])

#     print('end analysis')

#     # print('>>> step 2: write active tweets from users: Done')
#     # print('   *keep lambda=',l)
#     # print(' + non_active <=', non_act_max)
#     # print(' + active >=', act_min)
    
#     # print('1. load')
#     # df = load_data_csv(from_path)
    
#     # print('2. apply')
#     # df['nbs_tweets'] = df.apply(lambda x: tweets[x['user_id']] if(x['user_id'] in tweets) else -30, axis=1)
#     # df['nbs_degree'] = df.apply(lambda x: deg[x['user_id']] if(x['user_id'] in deg) else -30, axis=1)
#     # df['d_in'] = df.apply(lambda x: in_deg[x['user_id']] if(x['user_id'] in in_deg) else -30, axis=1)
#     # df['d_out'] = df.apply(lambda x: out_deg[x['user_id']] if(x['user_id'] in out_deg) else -30, axis=1)    
#     # df['score'] = df.apply(lambda x: dict_score2[x['user_id']] if(x['user_id'] in dict_score2) else -30, axis=1)

#     # if(is_by_idx == True):
#     #     srt = df[df['score'] != -30].sort_values(['score', 'user_id'], ascending = [True, True])
        
#     #     v_min = srt.head(1)['score'].values[0]
#     #     list_usr = df[df['score'] == v_min]['user_id'].tolist()
#     #     idx_non_active = srt[srt['user_id'].isin(list_usr)].index.tolist()
#     #     # idx_non_active = srt.head(int(0.1*len(srt))).index.tolist()
        
#     #     list_usr = srt.tail(int(0.1*len(srt)))['user_id'].tolist()
#     #     idx_active = srt[srt['user_id'].isin(list_usr)].index.tolist()
#     #     # idx_active = srt.tail(int(0.1*len(srt))).index.tolist()
        
#     #     df['active_LABEL'] = df.apply(lambda x: get_score_by_idx(x, idx_non_active, idx_active), axis=1)
        
#     # else:
#     #     df['active_LABEL'] = df.apply(lambda x: get_score(x, dict_score2, non_act_max, act_min), axis=1)
    
#     # # print('3. write')
#     # create_csv_file(df, to_path)

        
    
#####################################################################################################
    
# #5 column to delete, from 126 to 121
if __name__ == "__main__":
    print('-------- START 5: filter data for thomas, 1 tweet by user (independance) --------')
    cpt=1
    length = 0
    final_df = None
    file = 'INFLUENCERS_LIWC' #'TWEETS_ALL_TOPICS_LABELLED_LIWC' / 'pelosi_LIWC'
    label = ['influencer_label', [-30, -1]] #['controversy_LABEL', []] / ['metis_label', [-10]] \ ['metis_label', [-10]]
    sample_by = 'UNIQUE' #'UNIQUE' 'MEAN'
    
    from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/' + file+'.csv' 
    to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/'+file+'_thomas_'+sample_by+'.csv'
    labels = ut.data_zarate_labels
    
    print('1. load')
    df = load_data_csv(from_path)
    
    print('  >> after 1 [nbs user (total tweets)]:', len(list(set(df['user_id'].tolist()))), '(', len(df), ')')

    print('2. remove not labelled data')
    if(len(label[1]) != 0): #we have to delete data
        print(' lets remove!')
        for l in label[1]:
            df = df[df[label[0]] != l]
    print('  >> after 2 [nbs user (total tweets)]:', len(list(set(df['user_id'].tolist()))), '(', len(df), ')')
    
    if(sample_by == 'MEAN'):
        print('3. kept column')
        features_and_label = ut.LIWC_FEATURES + [label[0]] + ['controversy_LABEL']
        
        features_kept = ['topic', 'user_id'] + features_and_label
        df = df[features_kept]
        
        print('  >> after 3 [nbs user (total tweets)]:', len(list(set(df['user_id'].tolist()))))
    
        print('4B. apply one random tweet by user (my function)')
        temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/temp_MEAN.pickle'
        
        new_df = bmt.get_one_sample_by_user(df, temp_path, label[0], features_and_label, sample_by = sample_by)
        
        print('  >> after 4B [nbs user (total tweets)]:', len(new_df))
        
    elif(sample_by == 'UNIQUE'):    
        print('4B. apply one random tweet by user (my function)')
        temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/temp_UNIQUE.pickle'
        new_df = bmt.get_one_sample_by_user(df, temp_path, label[0], features_and_label = None, sample_by = sample_by)
        print('  >> after 4B [nbs user (total tweets)]:', len(new_df))
        
        print('3. kept column')
        features_and_label = ut.LIWC_FEATURES + [label[0]] + ['controversy_LABEL']
        features_kept = ['topic', 'user_id'] + features_and_label
        new_df = new_df[features_kept]
        print('  >> after 3 [nbs user (total tweets)]:', len(list(set(new_df['user_id'].tolist()))))
      
    
    print('5. write')
    create_csv_file(new_df, to_path)
    
    
#####################################################################################################


# if __name__ == "__main__":
#     print('-------- START 6: get nbs rt by tweet --------')
#     cpt=1
#     final_df = None
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + '/data_ZARATE_translated_v3/' + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/rt_by_tweet.pickle'
        
#         df = load_data_csv(from_path)
#         df = df[df['retweet_status_id'].isnull() == False]
#         df['topic'] = [topic] * len(df)
                
#         if(final_df is None):
#             final_df = df
#         else:
#             final_df = pd.concat([final_df, df])
#         cpt+=1
            
#     count_df = final_df.groupby(['topic', 'retweet_status_id']).size().reset_index(name='counts')


#     pickle_save(to_path, count_df)
    
#     print('<finish>')



# path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
# df_tweets = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')

# count_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/rt_by_tweet.pickle'
# count_df = load_pickle(count_path)


# topk_df = count_df.groupby('topic',group_keys=False)\
#             .apply(lambda grp:grp.nlargest(n=2000,columns='counts').sort_index())
# topk_df = topk_df.rename(columns={"retweet_status_id": "tweet_id"})
# # yyy = topk_df.groupby('topic')['counts'].min()
        
            
# new_df_tweets = df_tweets.set_index(['topic', 'tweet_id'])
# new_topk_df = topk_df.set_index(['topic', 'tweet_id'])

# result = pd.concat([new_df_tweets, new_topk_df], axis=1, join="inner").reset_index()
# result = result.drop('counts', axis=1)



#####################################################################################################

# #     print('-------- START 8: community for influencer --------')
# topic = '1Direction'
# temp_path = ut.CURRENT_DATA_FOLD + '/data_objects/ANALYSIS/influencer_LABEL/statistics/PageRank at 5%/dict_influencer_005.pickle'

# dict_influencer, dict_stats = load_pickle(temp_path)
# topic_inf = dict_influencer['OTDirecto20E']

# from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/' + topic+'_LIWC.csv'
# to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/' + topic+'_inf_LIWC.csv'

# df_tweets = load_data_csv(from_path)

# new_df = df_tweets[df_tweets['user_id'].isin(topic_inf)]

# create_csv_file(new_df, to_path)

#####################################################################################################

# aggr = 'MEAN'
# if __name__ == "__main__":
#     print('-------- START 7: sample by topic a small % --------')

#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/TWEETS_ALL_TOPICS_LABELLED_LIWC_thomas_'+aggr+'.csv' 
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/sampled/TWEETS_ALL_TOPICS_LABELLED_LIWC_thomas_'+aggr+'_10.csv' 
    
#     df = load_data_csv(from_path)
#     nbs_topic = len(list(set(df['topic'].tolist())))

#     grouped = df.groupby('topic')
#     new_df = grouped.apply(lambda x: x.sample(frac=0.1)).reset_index(drop=True)

#     print('5. write')
#     create_csv_file(new_df, to_path)
        


##########################

# if __name__ == "__main__":
#     print('-------- START influencers (proportion pageRank): --------')    
#     topic = 'pelosi'        
#     # fractions = [100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 1, 0]
#     fractions = [100, 90, 70, 50, 30, 20, 10, 5, 1, 0]

#     from_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/influencer_LABEL/statistics/PageRank at 5%/dict_influencer_005.pickle'
#     dict_influencer, dict_stats = load_pickle(from_path)
    
#     pr_all = dict_stats[topic][2]
    
#     sum_all_pr = sum(pr_all)
    
#     prop_by_quartile = []
#     for frac in fractions:
#         q_max = np.quantile(pr_all, 1-(frac/100))
        
#         temp_sum = sum([x for x in pr_all if(x>= q_max)])
        
#         prop_by_quartile.append(round((temp_sum/sum_all_pr)*100, 2))
        
#     df = pd.DataFrame({'fraction_top_user': fractions,
#                         'proportion': prop_by_quartile})
    
#     sns.lineplot(data=df, x="fraction_top_user", y="proportion", 
#                   style="fraction_top_user",
#                   markers=True, dashes=True)

    

    
    
    