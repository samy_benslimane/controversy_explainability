#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 09:54:26 2022

@author: samy
"""
import matplotlib.pyplot as plt

from scipy.stats import mannwhitneyu
from scipy import stats

from transformers import TextClassificationPipeline

from sklearn import metrics
from sklearn.decomposition import TruncatedSVD
from sklearn.tree import DecisionTreeClassifier 
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer


from scipy.stats import chi2_contingency
from shap.plots import waterfall, beeswarm, force


import bert_model_train as bmt
import model_train as mt
import util as ut

import pandas as pd
import numpy as np
import scipy as sp
import pickle5 as pickle
# import pickle1
import random
import torch
import math
import time
import json
import shap
import csv
import os


def get_user_node_label(path, label_method, topic):
    filename = path +topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        dict_labels = json.load(f)
        
    labels = [v for k,v in dict_labels.items()]
    n_lab = len(list(set(labels)))
    print('---------- LABEL_QC ----------')
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('-------------------------------')
    
def check_null(x):
    for i in x:
        if(math.isnan(i) == True):
            return True
    return False
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels


def get_random_samples(x1, x2):
    sample_x1 = []
    sample_x2 = []
    
    if(len(x1)<len(x2)):
        min_lab = len(x1)
        idx = list(random.sample(set(range(len(x2))), min_lab))
        
        sample_x2 = [x2[i] for i in idx]
        sample_x1 = x1   
    elif(len(x2)<len(x1)):
        min_lab = len(x2)
        idx = list(random.sample(set(range(len(x1))), min_lab))
        
        sample_x1 = [x1[i] for i in idx]
        sample_x2 = x2
    else:
        min_lab = len(x1)
        sample_x1 = x1
        sample_x2 = x2
        
    return sample_x1, sample_x2


def balance_test(test_tweets, dict_labels):
    print(' > we balance test!')
    x0 = [t for t in test_tweets if(dict_labels[t]==0)]
    x1 = [t for t in test_tweets if(dict_labels[t]==1)]
    
    if(len(x0)<len(x1)):
        new_set = x0 + random.sample(x1, len(x0))
    elif(len(x1)<len(x0)):
        new_set = x1 + random.sample(x0, len(x1))
    else:
        print('  >> no need wtf')
        return test_tweets
    
    return new_set

def predictor_list(list_x):
    print('hiiiiiiiiiiihi +- > (0000000000)')
    out = []
    val = []
    for x in list_x:
        o, v = predictor_l(x)
        out.append(o)
        val.append(v)
        
    return out, val

def predictor_l(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()} 
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
        
    outputs, out_embedding = p_model(new_inputs)
        
    outputs = outputs.detach().cpu().numpy()

    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return outputs, val



def predictor(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()}
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
    
    outputs, out_embedding = p_model(new_inputs)
    
    outputs = outputs.detach().cpu().numpy()
    
    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return val


def f_batch(x):
    val = np.array([])
    for i in x:
      val = np.append(val, predictor(i))
    return val
    
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
TEST_TOPICS = ['pelosi', 'lula_moro_chats', 'messicumple', 'Thanksgiving']
true_label = ['controversy_LABEL', [], [0,1]]
device = [0]
n_top_k = 2000


#########################################################################################################################################
#################################################### 2. train models #######################################################

if __name__ == "__main__":
    print('------------------------------- TRAIN CLASSIFICATION MODEL v2 [C vs NC] ------------------------------')
    t1 = time.time()
    
    model = 'bert' #'decision_tree', 'random_forest', 'bert'
    features = 'text' #'tf-idf', 'liwc', 'tf-idf_liwc', 'text', 'text_liwc'
    top_k = True
    random_k = False
    
    print(' -----', model, ' |', features, '|', top_k, '|', random_k, '-----')
    ####### LOAD DATA

    root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+true_label[0]+'/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
    
    if(top_k == True):
        temp_path = root + 'controversy_ALL_topk.pickle'
    elif(random_k == True):
        temp_path = root + 'controversy_ALL_randomk.pickle'
    else:
        temp_path = root + 'controversy_ALL.pickle'
        
    if(os.path.isfile(temp_path) == True):
        print(' >> find pickle data file ! ')
        df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets = load_pickle(temp_path)
        print(' > proportion train/test =', round(len(train_tweets)/(len(train_tweets)+len(test_tweets)),3))
    else:
        #1. load data
        print(' 1. load data')
        df_tweets = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')
        if(top_k == True):
            print(' > only keep top-k by topic:', n_top_k)
            count_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/rt_by_tweet.csv'
            df_rt_by_tweet = load_data_csv(count_path)
            topk_df = df_rt_by_tweet.groupby('topic',group_keys=False)\
                                            .apply(lambda grp:grp.nlargest(n=n_top_k,columns='counts').sort_index())
            topk_df = topk_df.rename(columns={"retweet_status_id": "tweet_id"})
            
            df_tweets = df_tweets.set_index(['topic', 'tweet_id'])
            topk_df = topk_df.set_index(['topic', 'tweet_id'])
            
            df_tweets = pd.concat([df_tweets, topk_df], axis=1, join="inner").reset_index()
            df_tweets = df_tweets.drop('counts', axis=1)
        elif(random_k == True):
            print(' > only keep random-k by topic:', n_top_k)
            df_tweets2 = df_tweets.groupby('topic')
            df_tweets2 = df_tweets2.apply(lambda x: x.sample(n_top_k))
            df_tweets2.index = df_tweets2.index.droplevel('topic')
      
        df_tweets = df_tweets[df_tweets['topic'] != 'mothersday']

        #2. clean data
        print(' 2. clean data')
        all_labels = set(df_tweets[true_label[0]].tolist())
        print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
        for l in all_labels:        
            print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
            print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
            print('            > nbs topics:', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['topic'].tolist()))))
            
            
        df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        # df_tweets = df_tweets[df_tweets['topic'].isin(['messicumple', 'pelosi', 'nintendo','bolsonaro27'])] #TODO DELETE

        #3. prepare data
        print(' 3. prepare data (faster)')
        dict_features = {}
        dict_text = {}
        dict_labels = {}
        
        all_train_tweets = []
        test_tweets = []
        records = df_tweets.to_dict('records')
        nbs_nan = 0
        example_nan = 'NO NAN VALUES'
        for row in records:
            t_id = row['tweet_id']
            f = [row[k] for k in ut.LIWC_FEATURES]
            have_nan = check_null(f)
            
            if(have_nan == True):
                nbs_nan += 1
                example_nan = row['clean_text']
            else:
                dict_features[t_id] = f
                dict_text[t_id] = row['clean_text']
                dict_labels[t_id] = true_label[2].index(row[true_label[0]])
                
                if(row['topic'] in TEST_TOPICS):
                    test_tweets.append(t_id)
                else:
                    all_train_tweets.append(t_id)
                
        print(' > number of nan records:', nbs_nan, '---> example: ', example_nan)
        print('  > all_train_tweets:', len(all_train_tweets), '| all_test_tweets:', len(test_tweets))
        all_train_tweets = list(set(all_train_tweets))
        test_tweets = list(set(test_tweets))
        test_tweets = balance_test(test_tweets, dict_labels)
        print('  > all_train_tweets:', len(all_train_tweets), '| all_test_tweets:', len(test_tweets))
            
        #4. split dataset
        print(' 4. split dataset (putting manual tweets on test)')
        train_tweets = bmt.balance_train_set_controversy(dict_labels, all_train_tweets, test_tweets)
        
        pickle_save(temp_path, (df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets))
        print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')
    
    #--------------------------------------------------------
    if(top_k == True):
        root = root + model + '_' + features + '_topk/'
    elif(random_k == True):
        root = root + model + '_' + features + '_randomk/'
    else:
        root = root + model + '_' + features + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    #5. train model
    print(' 5. train model')
    if(model == 'bert'):
        print(' ----', model, '----')
        if(features == 'text'):
            combined_dict_f = None
        elif(features == 'text_liwc'):
            combined_dict_f = {k:v for k,v in dict_features.items()}
        
        p_model, tokenizer, res = bmt.bert_fine_tuning(train_tweets, test_tweets, [], [], dict_labels, dict_text, combined_dict_f, None, features, device, root, true_label[0],
                            is_topk = top_k, is_randomk=random_k, bert_epochs = 30, bert_batches = 32, max_length = 256, lr_rate = 2e-5,
                            truncate = True, discr_lr = True, k_fold = 'NO')
        
        
        X, X_train, X_test, y, y_train, y_test, _ = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_text, combined_dict_f)
        
        
        # print('----------- 6. GET SHAP VALUES TRAIN -----------')
        # print('##### 6.1 on TRAIN #####')
        # print('  >> 6.1.1 sampling data over', len(X_train), 'records')         
        # #sampling
        # temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_train.pickle'
        # if(os.path.isfile(temp_path) == True):
        #     print(' > sample found!')
        #     sample_X, sample_y = load_pickle(temp_path)
        # else:
        #     sample_X, sample_y = bmt.shap_sampling_controversy(X_train, y_train, df_tweets, None, None, 
        #                                                         nbs_sample=45000, test_topics=TEST_TOPICS)
        #     pickle_save(temp_path, (sample_X, sample_y))
            
        # print(' ***** SHAP ALL TRAIN *****')
        # samp_X = sample_X
        # samp_y = sample_y
        # print('          >> size sampling:', len(samp_X))
        
        # samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # samp_X_list = samp_X[0].tolist()
                
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(samp_X_list)
        
        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainerC = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_valuesC = explainerC(samp_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_valuesC, samp_X, samp_y, outputs, val, samp_X_list)
        # pickle_save(root+'/shap_temp_TRAIN_ALL.pickle', data)
        # ###############
        

        # print(' ***** SHAP TRAIN feliznatal (NC) *****')
        # top = 'feliznatal'
        
        # print('  >> 6.1.1 sampling data')         
        # #sampling
        # temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_train_'+top+'.pickle'
        # if(os.path.isfile(temp_path) == True):
        #     print(' > sample found!')
        #     sample_X, sample_y = load_pickle(temp_path)
        # else:
        #     sample_X, sample_y = bmt.shap_sampling_controversy(X_train, y_train, df_tweets, None, None, 
        #                                                         nbs_sample=35000, one_topic=top)
        #     pickle_save(temp_path, (sample_X, sample_y))
        
        # samp_X = sample_X
        # samp_y = sample_y
        # print('          >> size sampling:', len(samp_X))
        
        # samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # samp_X_list = samp_X[0].tolist()
        
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(samp_X_list)
        
        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainerC = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_valuesC = explainerC(samp_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_valuesC, samp_X, samp_y, outputs, val, samp_X_list)
        # pickle_save(root+'/shap_temp_TRAIN_'+top+'.pickle', data)
        # ###############
        
            
        # print(' ***** SHAP TRAIN kavanaugh16 (C) *****')
        # top = 'kavanaugh16'
        
        # print('  >> 6.1.1 sampling data')         
        # #sampling
        # temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_train_'+top+'.pickle'
        # if(os.path.isfile(temp_path) == True):
        #     print(' > sample found!')
        #     sample_X, sample_y = load_pickle(temp_path)
        # else:
        #     sample_X, sample_y = bmt.shap_sampling_controversy(X_train, y_train, df_tweets, None, None, 
        #                                                         nbs_sample=35000, one_topic=top)
        #     pickle_save(temp_path, (sample_X, sample_y))
        
        # samp_X = sample_X
        # samp_y = sample_y
        # print('          >> size sampling:', len(samp_X))
        
        # samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # samp_X_list = samp_X[0].tolist()
                
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(samp_X_list)
        
        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainerC = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_valuesC = explainerC(samp_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_valuesC, samp_X, samp_y, outputs, val, samp_X_list)
        # pickle_save(root+'/shap_temp_TRAIN_'+top+'.pickle', data)
        # ###############
        
        
        print('----------- 8. GET SHAP VALUES TEST -----------')
        # print('##### 6.1 on TEST #####')
        # print('  >> 6.1.1 sampling data over', len(X_test), 'records') 
        # #sampling
        # temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_test.pickle'
        # if(os.path.isfile(temp_path) == True):
        #     print(' > sample found!')
        #     sample_X, sample_y = load_pickle(temp_path)
        # else:
        #     sample_X, sample_y = bmt.shap_sampling_controversy(X_test, y_test, df_tweets, None, None,
        #                                                         nbs_sample=40000)
        #     print(' > len sample_x:', len(sample_X))
        #     pickle_save(temp_path, (sample_X, sample_y))


        # print(' ***** SHAP ALL TEST *****')
        # samp_X = sample_X
        # samp_y = sample_y

        # print('          >> size sampling:', len(samp_X))
        # samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # samp_X_list = samp_X[0].tolist()
                
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(samp_X_list)
        
        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainerC = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_valuesC = explainerC(samp_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_valuesC, samp_X, samp_y, outputs, val, samp_X_list)
        # pickle_save(root+'/shap_temp_TEST_ALL.pickle', data)
        # ###############

        # print(' ***** SHAP TEST pelosi (C) *****')
        # top = 'pelosi'
        
        # print('  >> 6.1.1 sampling data')         
        # #sampling
        # temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_test_'+top+'.pickle'
        # if(os.path.isfile(temp_path) == True):
        #     print(' > sample found!')
        #     sample_X, sample_y = load_pickle(temp_path)
        # else:
        #     sample_X, sample_y = bmt.shap_sampling_controversy(X_test, y_test, df_tweets, None, None, 
        #                                                         nbs_sample=35000, one_topic=top)
        #     pickle_save(temp_path, (sample_X, sample_y))
        
        # samp_X = sample_X
        # samp_y = sample_y
        # print('          >> size sampling:', len(samp_X))
        
        # samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # samp_X_list = samp_X[0].tolist()
                        
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(samp_X_list)

        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainerC = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_valuesC = explainerC(samp_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_valuesC, samp_X, samp_y, outputs, val, samp_X_list)
        # pickle_save(root+'/shap_temp_TEST_'+top+'.pickle', data)
        # ###############       
        
        
        
        print(' ***** SHAP TEST Thanksgiving (NC) *****')
        top = 'Thanksgiving'

        print('  >> 6.1.1 sampling data')         
        #sampling
        temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_test_'+top+'.pickle'
        if(os.path.isfile(temp_path) == True):
            print(' > sample found!')
            sample_X, sample_y = load_pickle(temp_path)
        else:
            sample_X, sample_y = bmt.shap_sampling_controversy(X_test, y_test, df_tweets, None, None, 
                                                                nbs_sample=35000, one_topic=top)
            pickle_save(temp_path, (sample_X, sample_y))
        
        samp_X = sample_X
        samp_y = sample_y
        print('          >> size sampling:', len(samp_X))
        
        samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        samp_X_list = samp_X[0].tolist()
                
        print(' >> 6.1.2 get explainer')
        outputs, val = predictor_list(samp_X_list)

        # build an explainer using a token masker
        print(' >1. explainer')
        time1 = time.time()
        explainerNC = shap.Explainer(f_batch, tokenizer)
        print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        print(' >2. shap values')
        shap_valuesNC = explainerNC(samp_X_list, fixed_context=1)
        print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        ###############
        data = (shap_valuesNC, samp_X, samp_y, outputs, val, samp_X_list)
        pickle_save(root+'/shap_temp_TEST_'+top+'.pickle', data)
        ###############   
        
        
        
        print(' ***** SHAP TEST messicumple (NC) *****')
        top = 'messicumple'

        print('  >> 6.1.1 sampling data')         
        #sampling
        temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_test_'+top+'.pickle'
        if(os.path.isfile(temp_path) == True):
            print(' > sample found!')
            sample_X, sample_y = load_pickle(temp_path)
        else:
            sample_X, sample_y = bmt.shap_sampling_controversy(X_test, y_test, df_tweets, None, None, 
                                                                nbs_sample=35000, one_topic=top)
            pickle_save(temp_path, (sample_X, sample_y))
        
        samp_X = sample_X
        samp_y = sample_y
        print('          >> size sampling:', len(samp_X))
        
        samp_X[0] = samp_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        samp_X_list = samp_X[0].tolist()
                
        print(' >> 6.1.2 get explainer')
        outputs, val = predictor_list(samp_X_list)

        # build an explainer using a token masker
        print(' >1. explainer')
        time1 = time.time()
        explainerNC = shap.Explainer(f_batch, tokenizer)
        print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        print(' >2. shap values')
        shap_valuesNC = explainerNC(samp_X_list, fixed_context=1)
        print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        ###############
        data = (shap_valuesNC, samp_X, samp_y, outputs, val, samp_X_list)
        pickle_save(root+'/shap_temp_TEST_'+top+'.pickle', data)
        ###############       
        
# =========================================================================================================================
  
    
    elif(model == 'decision_tree' or 'random_forest'):
        print(' ----', model, '----')
        print('  5.1 get features')
        
        if(features == 'liwc'):
            feature_names = ut.LIWC_FEATURES
            dict_f = dict_features
        elif(features == 'tf-idf' or features == 'tf-idf_liwc'):
            my_stop_words = []
            # print(' > rm stop words')
            # my_stop_words = text.ENGLISH_STOP_WORDS
            vectorizer = TfidfVectorizer(min_df=5, stop_words=my_stop_words)
            print('clean 1')
            train_tf = [bmt.clean_text_v2(dict_text[k], False) for k in train_tweets]
            vectorizer.fit(train_tf)
            
            print('clean 2')
            dict_f = {}
            all_set = [bmt.clean_text_v2(v, False) for k,v in dict_text.items()]
            print('r1')
            all_set = vectorizer.transform(all_set)
            
            if(top_k == False and random_k == False or True == True):
                print('r2 truncate pca')
                pca_object = TruncatedSVD(1000)
                all_set = pca_object.fit_transform(all_set)
                print('r3')
                cpt=0
                for k in dict_text.keys():
                    if(features == 'tf-idf'):
                        dict_f[k] = all_set[cpt]
                    elif(features == 'tf-idf_liwc'):
                        dict_f[k] = np.array(list(all_set[cpt]) + list(dict_features[k]))
                    cpt+=1
                print('r4')   
                if(features == 'tf-idf'):
                    feature_names = [str(i) for i in range(len(all_set[0]))] #vectorizer.get_feature_names()
                elif(features == 'tf-idf_liwc'):
                    feature_names =[str(i) for i in range(len(all_set[0]))] + ut.LIWC_FEATURES # vectorizer.get_feature_names() + ut.LIWC_FEATURES
                print('r5')   
            else:
                print(' > No pca !')
                cpt=0
                for k in dict_text.keys():
                    if(features == 'tf-idf'):
                        dict_f[k] = all_set[cpt].toarray()[0]
                    elif(features == 'tf-idf_liwc'):
                        dict_f[k] = np.array(list(all_set[cpt].toarray()[0]) + list(dict_features[k]))
                    cpt+=1
                print('r4')
                print(' > get feat names')
                if(features == 'tf-idf'):
                    feature_names = vectorizer.get_feature_names()
                elif(features == 'tf-idf_liwc'):
                    feature_names = vectorizer.get_feature_names() + ut.LIWC_FEATURES  
                print('r5')
        print('    > len features:', len(feature_names)) 
        
        
        print('  5.2 get split dataset')
        temp_path = root + 'controversy_dataset_split_dt.pickle'
        if(os.path.isfile(temp_path) == True):
            X, X_train, X_test, y, y_train, y_test = load_pickle(temp_path)
        else:
            X, X_train, X_test, y, y_train, y_test, (X_train_idx, X_test_idx) = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_f, feature_names=feature_names)
            pickle_save(temp_path, (X, X_train, X_test, y, y_train, y_test))

        print(' > train shape:', X_train.shape)
        print(' > test shape:', X_test.shape)
        
        print('  5.3 training')
        temp_path = root + model + '_' + features + '.pickle'
        if(os.path.isfile(temp_path) == True):
            print('- found model')
            importance, clf = load_pickle(temp_path)
        else:
           clf, importance = mt.train_model(model, X_train, X_test, pd.DataFrame(), y_train, y_test, pd.DataFrame(), features, root, data_to_save='test')
        
        
        if(model == 'random_forest'): #TODO DELETE (voting)
            all_trees_proba = []
            for tree in range(100):
                a = clf.estimators_[tree].predict_proba(X_test) # shape = {nbs_tree, nbs_sample, nbs_class}
                all_trees_proba.append(a)
            pickle_save(root + model + '_' + features +'_PREDICT_PROBA.pickle', (all_trees_proba, y_test))
        elif(model=='decision_tree'):
            all_trees_proba = []
            a = clf.predict_proba(X_test) # shape = {nbs_sample, nbs_class}
            all_trees_proba.append(a)
            pickle_save(root + model + '_' + features +'_PREDICT_PROBA.pickle', (all_trees_proba, y_test))
        

        #------------------------------ SHAP -------------------------------

        print('  5.4 get shap values')
        print('============ TRAIN ===========')
        if((features == 'tf-idf' or features == 'tf-idf_liwc') or model=='decision_tree'): # and (top_k == False and random_k == False and True == True)):
            print(' > no shap, because PCA')
        else:
            print('       5.4.1 sampling data over', len(X_train), 'records')
            
            #sampling
            temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_train.pickle'
            if(os.path.isfile(temp_path) == True):
                sample_X, sample_y = load_pickle(temp_path)
            else:
                sample_X, sample_y = bmt.shap_sampling_controversy(X_train, y_train, df_tweets, None, None, nbs_sample=45000, test_topics=TEST_TOPICS)
                pickle_save(temp_path, (sample_X, sample_y))
            print('          >> NEW size sampling:', len(sample_X))

            clf_sample = clf
 
            print('***** ALL TOPICS *****')
            samp_X = sample_X
            samp_y = sample_y
            
            print('      5.4.3 Explaining over', len(samp_X), 'records')
            time_ex = time.time()
            explainer = shap.TreeExplainer(clf_sample)
            print(' >1.', round(((time.time() - time_ex)/60), 2), 'min') 
            shap_obj = explainer(samp_X, check_additivity=False)
            print(' >2.', round(((time.time() - time_ex)/60), 2), 'min') 
            shap_values = explainer.shap_values(samp_X, check_additivity=False)
            print(' >3.', round(((time.time() - time_ex)/60), 2), 'min') 
            
            ###############
            data = (shap_values, samp_X, samp_y)
            pickle_save(root+'/shap_temp_TRAIN_ALL.pickle', data)
            ###############
            
            
            
            
        print('============ TEST ===========')
        if((features == 'tf-idf' or features == 'tf-idf_liwc') or model=='decision_tree'): # and (top_k == False and random_k == False and True == True)):
            print(' > no shap, because PCA')
        else:
            print('       5.4.1 sampling data over', len(X_test), 'records')
            
            #sampling
            temp_path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/controversy_LABEL/'+ features+'_sample_data_test.pickle'
            if(os.path.isfile(temp_path) == True):
                sample_X, sample_y = load_pickle(temp_path)
            else:
                sample_X, sample_y = bmt.shap_sampling_controversy(X_test, y_test, df_tweets, None, None, nbs_sample=40000)
                pickle_save(temp_path, (sample_X, sample_y))
            print('          >> NEW size sampling:', len(sample_X))

            clf_sample = clf
 
            print('***** ALL TOPICS ****')
            samp_X = sample_X
            samp_y = sample_y
            
            print('      5.4.3 Explaining over', len(samp_X), 'records')
            time_ex = time.time()
            explainer = shap.TreeExplainer(clf_sample)
            print(' >1.', round(((time.time() - time_ex)/60), 2), 'min') 
            shap_obj = explainer(samp_X, check_additivity=False)
            print(' >2.', round(((time.time() - time_ex)/60), 2), 'min') 
            shap_values = explainer.shap_values(samp_X, check_additivity=False)
            print(' >3.', round(((time.time() - time_ex)/60), 2), 'min') 
            
            ###############
            data = (shap_values, samp_X, samp_y)
            pickle_save(root+'/shap_temp_TEST_ALL.pickle', data)
            ###############
                        
    print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---') 
    
    print('-----------------------------------------------------------------------------')