#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:29:42 2022

@author: samy
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 15:09:33 2021

@author: samy
"""

#!/usr/bin/env python
# coding: utf-8

##################### PACKAGES #####################
############################
##
##  - SUPPRIMER FUNCTION WRITE_TEMP()
##  - SUPPRIMER FUNCTION WRITE_TEMP_TOKENS()
##
###########################

import util as ut

from sklearn.metrics import classification_report, accuracy_score
from sklearn.model_selection import train_test_split
from transformers import BertTokenizer, BertModel
from transformers import (AdamW, 
                          get_linear_schedule_with_warmup,
                          set_seed)

from torch.utils.data import WeightedRandomSampler
from torch.utils.data import DataLoader
import torch.nn.functional as F
from torch import nn
import torch

from sklearn import metrics

from os import path
import numpy as np

import random
import time
import html
import re
import os


################### VARIABLE ####################
TO_DELETE = False

################## DATA CLASS ###################
  
"""
bert_fine_tuning with train & test set only
"""
def bert_fine_tuning(train_tweets, test_tweets, X_manual_test, y_manual_test,
                     dict_tweet_label, dict_text,
                     topic, device, root, true_label,
                     bert_epochs = 30,
                     bert_batches = 64,
                     max_length = 256,
                     lr_rate = 2e-5,
                     truncate = True,
                     discr_lr = True,
                     k_fold = 'NO'):
    
    print('<<<<<<<<<<<<<<<<<<<< TRAIN BERT MODEL FT >>>>>>>>>>>>>>>>>>>>>>>')
    
    print('******************************')
    print('       INIT PARAMETERS       ')
    print('+ topic : ', topic)
    print('+ size train/test : ', len(train_tweets), len(test_tweets))
    print('+ nbs tweets : ', len(dict_text))
    print('+ device : ', device)
    print('+ bert_epochs : ', bert_epochs)
    print('+ bert_batches : ', bert_batches)
    print('+ max_length : ', max_length)
    print('+ lr_rate : ', lr_rate)
    print('+ truncate : ', truncate)
    print('+ discr_lr : ', discr_lr)
    print('******************************')  
    
    
    path_fine_tuned_model  = ut.CURRENT_APP_FOLD + ut.bert_model_folder + topic+'_'+true_label+'_BERT.pth'
    print(path_fine_tuned_model)
    
    #delete previous fine-tuned model, because different split train/test
    if(path.exists(path_fine_tuned_model) == True and TO_DELETE == False and k_fold == 'NO'):
        print('- /!\ a model fine-tuned was found & KEPT !')
        return 1
        
    if(path.exists(path_fine_tuned_model) == True):
        os.remove(path_fine_tuned_model)
        print('- /!\ a model fine-tuned was found & DELETED !')
    
    start_time_1 = time.time()
    set_seed(123)
    
    # Loading tokenizer...
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer)    
    #... And load model...
    if(os.path.isdir(ut.BASIC_MODEL)==False):
        pretrained_bert_model = BertModel.from_pretrained(ut.basic_hugging_face)
    else:
        pretrained_bert_model = BertModel.from_pretrained(ut.BASIC_MODEL)        
    model = Tweet_polarization_BERTModel(ut.basic_hugging_face, pretrained_bert_model)    
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
    
    
    ########################### DATA ##############################
    ###### Get data
    X, X_train, X_test, y, y_train, y_test = get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text)
    X_train = clean_text_v2(X_train)
    X_test = clean_text_v2(X_test)
    X_manual_test = clean_text_v2(X_manual_test)
    
    ###### DATALOADERS
    print('#### 1.3.1 TOKENIZATION ####')
      
    # Train DATASET                        
    print('Dealing with Train...')
    train_dataset = TwitterDataset(X_train, y_train,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `train_dataset` with', len(train_dataset), 'examples!')
    if(truncate == True):
        sampler = get_balanced_sampler(y_train)
        train_dataloader = DataLoader(train_dataset,
                                      sampler=sampler,
                                      batch_size=bert_batches)
    else:
        train_dataloader = DataLoader(train_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=True)
    test_train_proportion(train_dataloader)
    
    
    # Manual Test DATASET
    print('\n Dealing with manual Test...', len(X_manual_test))
    manual_test_dataset =  TwitterDataset(X=X_manual_test, y=y_manual_test,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `manual_test_dataset` with', len(manual_test_dataset), 'examples!')
    manual_test_dataloader = DataLoader(manual_test_dataset, batch_size=bert_batches, shuffle=False)
    
    # Test DATASET
    print('Dealing with Test...')
    test_dataset =  TwitterDataset(X=X_test, y=y_test,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset` with', len(test_dataset), 'examples!')
    test_dataloader = DataLoader(test_dataset, batch_size=bert_batches, shuffle=False)
    
    # Test DATASET w/ balanced ds
    print('Dealing with test balanced...')
    test_dataset_balanced =  TwitterDataset(X=X_test, y=y_test,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset_balanced` with', len(test_dataset_balanced), 'examples!')
    if(truncate == True):
        sampler = get_balanced_sampler(y_test)
        test_balanced_dataloader = DataLoader(test_dataset_balanced,
                                      sampler=sampler,
                                      batch_size=bert_batches, shuffle=False)
    else:
        print('--------- no truncate ------------')
        test_balanced_dataloader = DataLoader(test_dataset_balanced,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)        
    
    #----------------------------------------------------------------------------------------------
    
    ######################### TRAINING ############################
    #optimizer
    if(discr_lr == True):
        grouped_parameters = configure_optimizers(model, lr_rate)
        optimizer = AdamW(grouped_parameters, lr = lr_rate, eps = 1e-8)
    else:
        optimizer = AdamW(model.parameters(), lr = lr_rate, eps = 1e-8)
        
    criterion = nn.CrossEntropyLoss()
    
    # Total number of training steps is number of batches * number of epochs.
    total_steps = len(train_dataset) * bert_epochs
    
    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 0, # Default value in run_glue.py
                                                num_training_steps = total_steps)
    
    
    print('#### 1.3.4. START BERT TRAINING (learning rate :', lr_rate,') ####')
    train_loss_list = []
    test_loss_list = []
    test_b_loss_list = []
    
    train_acc_list = []
    test_acc_list = []
    test_b_acc_list = []
    
    for epoch in range(bert_epochs): # Loop through each epoch.
        print("--> EPOCH ", epoch)

        # Perform one full pass over the training set.
        train_labels, train_predict, train_loss = train(model, train_dataloader, criterion, optimizer, scheduler, device)
        train_acc = accuracy_score(train_labels, train_predict)
    
        # Get prediction form model on test data. 
        # print('test on batches...')
        test_labels, test_predict, test_loss = validation(model, test_dataloader, criterion, device)
        test_acc = accuracy_score(test_labels, test_predict)
        print(test_labels, test_predict)
        test_labels_b, test_predict_b, test_loss_b = validation(model, test_balanced_dataloader, criterion, device)
        test_acc_b = accuracy_score(test_labels_b, test_predict_b)
        
    
        # Print loss and accuracy values to see how training evolves.
        print('(train_loss, test_loss, trai_acc, test_acc) = ', train_loss, test_loss, train_acc, test_acc)
        train_loss_list.append(train_loss)
        test_loss_list.append(test_loss)
        test_b_loss_list.append(test_loss_b)
        
        train_acc_list.append(train_acc)
        test_acc_list.append(test_acc)
        test_b_acc_list.append(test_acc_b)
        # print('Train Loss : ', train_loss_list)
        
        
               
        fold_chkpt = ut.CURRENT_APP_FOLD + ut.bert_model_folder + ut.checkpoint_folder
        #delete previous checkpoint
        for old_file in os.listdir(fold_chkpt):
             os.remove(fold_chkpt+old_file)
             
        #save current epochs 
        checkpoint = fold_chkpt + topic + '_' + true_label + '_'+ str(epoch) + '.pth'
        torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'train_loss': train_loss,
            'test_loss': test_loss,
            'train_acc': train_acc,
            'test_acc': test_acc,
            }, checkpoint)
    
    ut.plot_loss_bert(train_loss_list, test_loss_list, test_b_loss_list, root) #ADDED
    ut.plot_acc_bert(train_acc_list, test_acc_list, test_b_acc_list, root) #ADDED
    
    ######################## VALIDATION ###########################
    print('#### 1.3.5. START BERT TEST ####')
    # Get prediction form model on validation data. This is where you should use
    true_labels, predictions_labels, avg_epoch_loss = validation(model, test_dataloader, criterion, device) #ADDED
    m_true_labels, m_predictions_labels, m_avg_epoch_loss = validation(model, manual_test_dataloader, criterion, device) #ADDED
    
    # Show the evaluation report.
    print(">>>> FINAL BERT MODEL RESULT <<<<")
    # Show the evaluation report.
    print(">>>> TRAIN (last epoch) <<<<")
    print(classification_report(train_labels, train_predict))
    
    print(">>>> TEST <<<<")
    # Create the evaluation report.
    evaluation_report = classification_report(true_labels, predictions_labels)
    print(evaluation_report)
    
    print(">>>> MANUAL TEST <<<<")
    # Create the evaluation report.
    evaluation_report = classification_report(m_true_labels, m_predictions_labels)
    print(evaluation_report)
    
    print('--- runtime bert : ', round((time.time() - start_time_1)/60, 2), 'min')
    
    
    ##### Save model
    print('#### 1.3.6. SAVE model ####')
    
    torch.save(model, path_fine_tuned_model)
    print('- model saved! ', path_fine_tuned_model)
        
    return 1

    

"""
input :
    - train_tweets : list of train post
    - test_tweets : list of test post
    - dict_tweet_label : label for each post
    - dict_text : text for each post/comment
    - dict_graph_to_node : node (comment) belongs to which graph (post)
"""
def get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text):
    print('### 1.3.0. get train_test_set')
    print('- dict_text (', len(dict_text.keys()),')')

    X = []
    X_train = []
    X_test = [] 
    y = []
    y_train = []
    y_test = [] 

    pb = 0
    for t in dict_tweet_label.keys():      
        X.append(dict_text[t])
        y.append(dict_tweet_label[t])
        if(t in train_tweets):
            X_train.append(dict_text[t])
            y_train.append(dict_tweet_label[t])
        elif(t in test_tweets):
            X_test.append(dict_text[t])
            y_test.append(dict_tweet_label[t])
        else:
            pb +=1
        
    print('---problem in get train_test set : ', pb)

    return X, X_train, X_test, y, y_train, y_test 


"""
get a sampler for having more balanced dataset for the dataloader
input : 
    - labels : list of label of the dataloader
output :
    - sampler : sampler whoch will sample the data
"""
def get_balanced_sampler(labels):
    lab_unique, counts = np.unique(labels, return_counts=True)
    
    class_w = [sum(counts)/c for c in counts]
    balanced_w = [class_w[e] for e in labels]
    
    sampler = WeightedRandomSampler(balanced_w, len(labels))
    
    return sampler


################## MODEL FUNCTION ###################

"""
function used to fine-tuned our model
input : 
    - model : model to fine-tuned
    - dataloader : train dataloader
    - criterion : criterion to compute loss
    - optimizer_ : optimizer chosed
    - scheduler_ : scheduler 
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during training
"""
def train(model, dataloader, criterion, optimizer, scheduler, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.train() 

    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda(device_[0]) for k,v in batch.items()}     # move batch to device

        model.zero_grad() #init gradient at 0

        outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
        loss = criterion(outputs, batch['labels'])
        
        loss.backward() #compute gradient
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)     #  prevent the "exploding gradients" problem.
        optimizer.step() #Apply gradient descent
        scheduler.step() #Update the learning rate.

        total_loss += loss.item() #sum up all loss       
        
        true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist()  
        predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist() 

    avg_epoch_loss = total_loss / len(dataloader)
  
    return true_labels, predictions_labels, avg_epoch_loss


"""
function used to test our model
input : 
    - model : model to fine-tuned
    - dataloader : test dataloader
    - criterion : criterion to compute loss
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during test
"""
def validation(model, dataloader, criterion, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.eval()
    print('eee:', len(dataloader))
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():       
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
            loss = criterion(outputs, batch['labels'])
            
            total_loss += loss.item()

            true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist() #add original labels
            predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()

    avg_loss = total_loss / len(dataloader)

    return true_labels, predictions_labels, avg_loss



"""
predict from model
"""
def predict_model(dict_text, topic, true_label, device_, bert_batches=64, max_length=256):
    ########################### get tokenizer and model ###########################
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
   
    model_path = ut.CURRENT_APP_FOLD + ut.bert_model_folder + topic+'_'+true_label+'_BERT.pth'
    model = torch.load(model_path)
    model.eval()
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
    
    
    ########################### DATA ##############################
    X = [v for k,v in dict_text.items()]
    X = clean_text_v2(X)
    false_y = [-1]*len(X)
    
    dataset =  TwitterDataset(X=X, y=false_y,
                              tokenizer=tokenizer, 
                              max_sequence_len=max_length)
    print('dataset contains', len(dataset), 'examples!')
    dataloader = DataLoader(dataset, batch_size=bert_batches, shuffle=False)    
    
    ########################### PREDICTION ##############################
    predicted_labels = []
    out_emb = []
    print('eee:', len(dataloader))
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():       
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
            
            predicted_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            
            out_emb += outputs_embedding.detach().cpu().numpy().tolist()
            
    dict_labels = {}
    dict_embedding = {}
    cpt=0
    for k,v in dict_text.items():
        dict_embedding[k] = out_emb[cpt]
        dict_labels[k] = predicted_labels[cpt]

    return dict_embedding, dict_labels


###################################################################################
################################### OBJECT ########################################

"""
class which will transform (tokenized) the input text, and keep its corresponding label 
"""
class TwitterDataset():
    """
    init function
    input : 
        - tokenizer : Transformer type tokenizer used to process raw text into numbers.
        - y : Dictionary to encode any labels names into numbers. Keys map to labels names and Values map to number associated to those labels.
        - max_sequence_len : Value to indicate the maximum desired sequence to truncate or pad text sequences. If no value is passed it will used maximum sequence size
                             supported by the tokenizer and model.
    """
    def __init__(self, X, y, tokenizer, max_sequence_len=None):
        # Check max sequence length.
        max_sequence_len = tokenizer.max_len if max_sequence_len is None else max_sequence_len
        texts = X
        labels = y

        # Number of exmaples.
        self.n_examples = len(labels)
        print('max_sequence_len... ', max_sequence_len)
        
        # Use tokenizer on texts. This can take a while.
        self.inputs = tokenizer(
                        text=texts,  # the sentence to be encoded
                        add_special_tokens=True,  # Add [CLS] and [SEP]
                        truncation=True,
                        max_length = max_sequence_len,  # maximum length of a sentence
                        pad_to_max_length=True,  # Add [PAD]s
                        return_attention_mask = True,  # Generate the attention mask
                        return_tensors = 'pt',  # ask the function to return PyTorch tensors
                    )    

        # Get maximum sequence length.
        self.sequence_len = self.inputs['input_ids'].shape[-1]
        print('Texts padded or truncated to', self.sequence_len, 'length!')

        # Add labels.
        self.inputs.update({'labels':torch.tensor(labels)})
        print('Finished!\n')

    def __len__(self):
        return self.n_examples

    def __getitem__(self, item):
        return {key: self.inputs[key][item] for key in self.inputs.keys()}
    
    
"""
CONTROVERSY
class representing our model composed by a bert model + 1 more dense layer of 64 neurons + 1 softmax layer
Fine-tuning already done, here it is just a forward model
"""
class Tweet_polarization_BERTModel(nn.Module):
    """
    init function
    input : 
        - model_name_or_path : name of the pre-trained model
        - type_model : 'sentiment' or 'controversy'
        - pretrained_bert_model : pre-trained model il already loaded (default=None)
    """
    def __init__(self, model_name_or_path, pretrained_bert_model=None):
        super(Tweet_polarization_BERTModel, self).__init__()
        
        if(pretrained_bert_model is None):
            self.bert_model = BertModel.from_pretrained(model_name_or_path)
        else:
            self.bert_model = pretrained_bert_model
        
        self.embedding_layer = nn.Linear(768, 64)
        self.dropout = nn.Dropout(0.2)

        self.classifier_layer = nn.Linear(64, 2)
        
    """
    forward function
    input :
        - bert_ids : bert id of the sentence (from tokenization)
        - bert_mask : bert mask og the sentence (from tokenization)
    output :
        - out_ : prediction
        - out_embedding : embedding representation   
    """
    def forward(self, x, forward_2 = False):
        if(forward_2 == False):
            bert_ids = x[0]
            bert_mask = x[1]
            
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            out_embedding = self.embedding_layer(sequence_output[:,0,:].view(-1,768)) ## extract the 1st token's embeddings
            out_embedding = F.relu(out_embedding)
            
            out_ = self.dropout(out_embedding)
           
            #Layer 2
            out_ = self.classifier_layer(out_)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_embedding
        else:
            out_ = self.dropout(x)
           
            #Layer 2
            out_ = self.classifier_layer(out_)
            out_ = F.softmax(out_, dim=1)
    
            return out_
   
################################################################################
################################# other ########################################    
   
"""
function used for creating discriminative learning rate
input :
    - model : the current model
    - the learning rate for bert layers
output :
    - grouped_parameters : the group parameters for each layers
"""
def configure_optimizers(model, lr):
    params = list(model.named_parameters())
    def is_backbone(n): return 'bert' in n
    grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n)], 'lr': lr},
        {"params": [p for n, p in params if not is_backbone(n)], 'lr': lr * 100},
    ]
    return grouped_parameters


"""
test training proportion of the dataloader
input : 
    - train_dataloader : dataloader of training set
"""
def test_train_proportion(train_dataloader):
    lab0 = 0
    lab1 = 0
    for step, batch in enumerate(train_dataloader):
        list_lab =  batch['labels']
        for l in list_lab:
            if(l == 0):
                lab0+=1
            else:
                lab1+=1
                
    tot = lab0 + lab1
    print('--- PROPORTION CLASS DATALOADER ---')
    print('lab0 : ', round((lab0/tot)*100, 2), '% (', lab0 ,') / lab1 : ', round((lab1/tot)*100, 2), '% (', lab1 ,') --> TOT : ', tot)
    
    
"""
split the dataset into train/test set
"""
def split_train_test_dataset(dict_label_by_tweet, dict_manual_labels, labels=[0,1], is_balanced=True, keep_manual_test=True, train_ratio=0.8):    
    #if we keep manual test
    if(keep_manual_test == True):
        print('keep manual labelled tweets in test set...')
        old_size = len(dict_label_by_tweet)

        X_test_m = []
        y_test_m = []
        for k,_ in dict_manual_labels.items():
            X_test_m.append(k)
            y_test_m.append(dict_label_by_tweet[k])
            del dict_label_by_tweet[k]
            
        a0 = len([i for i in y_test_m if i==labels[0]])
        a1 = len([i for i in y_test_m if i==labels[1]])
        if(a0 < a1):
            y_to_complete = [labels[0], a1-a0]
        else:
            y_to_complete = [labels[1], a0-a1]
                    
        list_idx = list(dict_label_by_tweet.keys())
        random.shuffle(list_idx)
        for k in list_idx:
            if(y_to_complete[1] < 1):
                break
            v = dict_label_by_tweet[k]
            if(v==y_to_complete[0]):
                X_test_m.append(k)
                y_test_m.append(v)
                del dict_label_by_tweet[k]
                y_to_complete[1] -= 1
                
        print(' >', len(X_test_m), len(y_test_m))
        print(' >', len([i for i in y_test_m if i==labels[0]]), len([i for i in y_test_m if i==labels[1]]))
    
    #start spliting
    if(is_balanced == True):       
        print(' ... balancing ...')
        x0 = []
        x1 = []
        y0 = []
        y1 = []
        
        for k,v in dict_label_by_tweet.items():
            if(v==labels[0]):
                x0.append(k)
                y0.append(v)
            elif(v==labels[1]):
                x1.append(k)
                y1.append(v)
            
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
        
        if(len(x0) > len(x1)):       
            while(len(x0) != len(x1)):
                idx = random.randint(0,len(x0)-1)
                x0.pop(idx)
                y0.pop(idx)
        elif(len(x1) > len(x0)):
            while(len(x1) != len(x0)):
                idx = random.randint(0,len(x1)-1)
                x1.pop(idx)
                y1.pop(idx)
        print('...')
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
    
        X = x0 + x1
        y = y0 + y1
        
    else:
        print(' ... no balanced, we will do it at each epoch ...')
        X = []
        y = []
        for k,v in dict_label_by_tweet.items():
            if(v==labels[0] or v==labels[1]):
                X.append(k)
                y.append(v)
            else:
                print(' /!\ wtf problem, other label still present ! ')
           
            
    if(keep_manual_test == True):
        removed = len(X_test_m)
        old_size = len(X) + removed
        train_ratio = round((train_ratio * old_size)/(old_size-removed), 5)
        print(' > new ratio::', train_ratio, '::')
        
    print(' ... split', train_ratio, '...')
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    stratify=y, 
                                                    train_size=train_ratio)
    if(keep_manual_test == True):
        X_test = X_test + X_test_m
        y_test = y_test + y_test_m
    
    print('------- info split before normalizing -------')
    print(' > train set: ', round(len(X_train)/(len(X_train)+len(X_test)), 2), '(ratio) -->  y0: ', 
          round(len([y for y in y_train if y==labels[0]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_train if y==labels[1]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[1]]),')')
    
    print(' > test set: ', round(len(X_test)/(len(X_train)+len(X_test)),2), ' -->  y0: ',
          round(len([y for y in y_test if y==labels[0]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_test if y==labels[1]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[1]]),')')
    print('--------------------------')
    
    return X_train, X_test


def get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_features, limit=50):
    print(' > no balanced manual test set')
    tweets_c1 = []
    tweets_c2 = []
    
    dict_usr_labels = {}
    dict_tweet_labels = {}
    cpt0=0
    cpt1=0
    for k,v in dict_manual_label_by_usr.items():
        if(v['label']==0):
            dict_usr_labels[k] = v['label']
            cpt0+=1
        if(v['label']==1):
            dict_usr_labels[k] = v['label']
            cpt1+=1
            
    for idx, row in df_tweets.iterrows():
        if(row['user_id'] in dict_usr_labels.keys()):
            if(dict_usr_labels[row['user_id']] == 0):
                tweets_c1.append(row['tweet_id'])
            else:
                tweets_c2.append(row['tweet_id'])
            dict_tweet_labels[row['tweet_id']] = dict_usr_labels[row['user_id']]
                    
    X_manual_test = []
    y_manual_test = []
    for k,v in dict_tweet_labels.items():
        X_manual_test.append(dict_features[k])
        y_manual_test.append(v)
    
    prop0 = len([k for k,v in dict_tweet_labels.items() if(v==0)])
    prop1 = len([k for k,v in dict_tweet_labels.items() if(v==1)])
    print('  > nbs user: 0 (',cpt0, ')  --  1 (',cpt1,')')
    print('  > proportion manual test tweets set:')
    print('       > 0:', prop0, round(prop0/len(dict_tweet_labels), 3))
    print('       > 1:', prop1, round(prop1/len(dict_tweet_labels), 3))
    
    return X_manual_test, y_manual_test, dict_tweet_labels


"""
get_unique tweets by user
"""
def get_one_sample_by_user(df):
    list_user = list(set(df['user_id'].tolist()))
    print('  **', len(list_user))
    
    
    print('      -- a. run loop keeping one tweet')
    tweets_kept = []
    cpt=0
    for u in list_user:
        t1 = time.time()
        tweets = df[df['user_id'] == u]['tweet_id'].tolist()
        s = random.sample(tweets, 1)
        tweets_kept.append(s[0])
        
        
        cpt +=1
        if(cpt%int(len(list_user)/200) == 0 or cpt==1):
            print('   >>> done:', round((cpt/len(list_user))*100, 2) , '%  ->  time left:', round(((len(list_user)-cpt) * (time.time() - t1))/60, 2), 'min <<<')
        
    print('  **', len(tweets_kept))
    
    print('      -- b. get new dataframe')
    new_df = df[df['tweet_id'].isin(tweets_kept)]
    print('  **', len(new_df))
    print('  **', len(list(set(new_df['user_id'].tolist()))))
    
    return new_df
    


"""
clean text
"""
def clean_text_v2(inputs):
    new_inputs = []
    try:
        print(' > cleaning text!')
        for tweet in inputs:
            #remove retweet
            tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
            
            #remove URL
            tweet = re.sub(r'http\S+', r'URL', tweet)
            
            # remove @
            # tweet = re.sub(r'@[^\s]+',r'', tweet)
            
            #remove non-ascii words and characters
            # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
            # tweet = ''.join(tweet)
            # tweet = re.sub(r'_[\S]?',r'', tweet)
            # tweet = re.sub(r'\n',r' ', tweet)
            
            #remove #
            # tweet = re.sub(r'#([a-zA-Z0-9_]{1,50})',r'', tweet)
            #tweet = re.sub(r'#', '', tweet)
            
            # #remove &, < and >
            # tweet = tweet.replace(r'&amp;?',r'and')
            # tweet = tweet.replace(r'&lt;',r'<')
            # tweet = tweet.replace(r'&gt;',r'>')
            
            # # remove extra space
            # tweet = re.sub(r'\s\s+', r' ', tweet)
            
            # # insert space between punctuation marks
            # tweet = re.sub(r'([\w\d]+)([^\w\d ]+)', r'\1 \2', tweet)
            # tweet = re.sub(r'([^\w\d ]+)([\w\d]+)', r'\1 \2', tweet)
            
            # lower case and strip white spaces at both ends
            # tweet = tweet.lower()
            # tweet = tweet.strip()
            
            tweet = html.unescape(tweet)
            
            tweet = re.sub(r'\n', r'', tweet)
            
            new_inputs.append(tweet)
    except TypeError:
        print(tweet)
        print('XXXXXXXXXXXXXX')
        
    return new_inputs


def get_accuracy_from_dict(dict_labels, dict_manual_labels):
    y0 = []
    y1 = []
    for k,v in dict_manual_labels.items():
        y0.append(dict_labels[k])
        y1.append(v)    
    
    return metrics.accuracy_score(y0, y1)