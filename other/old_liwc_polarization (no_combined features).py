#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:32:09 2022

@author: samy
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 09:54:26 2022

@author: samy
"""
import matplotlib.pyplot as plt

from scipy.stats import mannwhitneyu
from scipy import stats

from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier 
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import chi2_contingency
from sklearn.feature_extraction.text import TfidfVectorizer

import bert_model_train as bmt
import model_train as mt
import util as ut

from boruta import BorutaPy
import pandas as pd
import numpy as np
import pickle
import random
import torch
import time
import json
import csv
import os




def get_user_node_label(path, label_method, topic):
    filename = path +topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        dict_labels = json.load(f)
        
    labels = [v for k,v in dict_labels.items()]
    n_lab = len(list(set(labels)))
    print('---------- LABEL_QC ----------')
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('-------------------------------')
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels


def get_random_samples(x1, x2):
    sample_x1 = []
    sample_x2 = []
    
    if(len(x1)<len(x2)):
        min_lab = len(x1)
        idx = list(random.sample(set(range(len(x2))), min_lab))
        
        sample_x2 = [x2[i] for i in idx]
        sample_x1 = x1   
    elif(len(x2)<len(x1)):
        min_lab = len(x2)
        idx = list(random.sample(set(range(len(x1))), min_lab))
        
        sample_x1 = [x1[i] for i in idx]
        sample_x2 = x2
    else:
        min_lab = len(x1)
        sample_x1 = x1
        sample_x2 = x2
        
    return sample_x1, sample_x2



    
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


topic = 'pelosi'
code_label = 'LC_8A'
path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
path_label_qc = ut.CURRENT_DATA_FOLD + ut.object_folder + 'LABELS/' +code_label+'/'+topic+'/RETWEET/'


true_label = ['metis_label', [-10], [0,1]] #['metis_label', [-10], [0,1]], ['LABEL_QC_label', [-20,0], [1,2]]
device = [0]
    
#########################################################################################################################################
#################################################### 2. train models #######################################################
##BEST FEATURES selection
    # decision tree
    # test acceptance (test + univaraite & multi-variate analaysis)
    # ++ shap value

if __name__ == "__main__":
    print('------------------------------- TRAIN CLASSIFICATION MODEL ------------------------------')
    t1 = time.time()
    
    model = 'decision_tree' #'decision_tree', 'random_forest', 'bert'
    features = 'tf-idf_liwc' #'tf-idf', 'liwc', 'tf-idf_liwc', 'text', 'text_liwc'
    print(' -----', model, ' |', features, '-----')
    ####### LOAD DATA
    manual_path = ut.CURRENT_DATA_FOLD + 'community_labels/' + topic + '__random.json'
    dict_manual_label_by_usr = load_json(manual_path)    

    root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+topic+'/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
    root+= true_label[0] + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    temp_path = root + topic+'.pickle'
    if(os.path.isfile(temp_path) == True):
        print(' >> find pickle data file ! ')
        dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels = load_pickle(temp_path)
        print(' > proportion train/test =', round(len(train_tweets)/(len(train_tweets)+len(test_tweets)),3))
    else:
        #1. load data
        print(' 1. load data')
        dict_node_label_sample = get_user_node_label(path_label_qc, code_label, topic)
        df_tweets = load_data_csv(path_liwc+topic+'_LIWC.csv')
        df_tweets = df_tweets[df_tweets['topic'] == topic]
        
        #2. clean data
        print(' 2. clean data')
        all_labels = set(df_tweets[true_label[0]].tolist())
        print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
        for l in all_labels:        
            print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
            print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
            
        for rm_lab in true_label[1]:
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
        df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        
        #3. prepare data
        print(' 3. prepare data')
        dict_features = {}
        dict_text = {}
        dict_labels = {}
        for idx, row in df_tweets.iterrows():
            t_id = row['tweet_id']
            
            dict_features[t_id] = row[ut.LIWC_FEATURES]
            dict_text[t_id] = row['clean_text']
            dict_labels[t_id] = true_label[2].index(row[true_label[0]]) # dict_labels[t_id] = row[true_label[0]]
            
       #4. get manual dataset
        _, _, dict_manual_labels = bmt.get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_features)
            
        #4. split dataset
        print(' 4. split dataset (putting manual tweets on test)')
        train_tweets, test_tweets = bmt.split_train_test_dataset(dict_labels, dict_manual_labels)
        
        pickle_save(temp_path, (dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels))
        print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')
    

    
    #5. train model
    print(' 5. train model')
    if(model == 'bert'):
        print(' ----', model, '----')
        X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_text)
        
        bmt.bert_fine_tuning(train_tweets, test_tweets, X_manual_test, y_manual_test, dict_labels, dict_text, topic, device, root, true_label[0],
                          bert_epochs = 30, bert_batches = 32, max_length = 256, lr_rate = 2e-5,
                          truncate = True, discr_lr = True, k_fold = 'NO')
    
    elif(model == 'decision_tree' or 'random_forest'):
        print(' ----', model, '----')
        print('  5.1 get features')
        
        if(features == 'liwc'):
            dict_f = dict_features
        elif(features == 'tf-idf' or features == 'tf-idf_liwc'):
            vectorizer = TfidfVectorizer()
            print('clean 1')
            train_tf = [bmt.clean_text_v2(v, False) for k,v in dict_text.items() if(k in train_tweets)]
            vectorizer.fit(train_tf)
            
            print('clean 2')
            dict_f = {}
            all_set = [bmt.clean_text_v2(v, False) for k,v in dict_text.items()]
            all_set = vectorizer.transform(all_set)
            cpt=0
            for k,v in dict_text.items():
                if(features == 'tf-idf'):
                    dict_f[k] = all_set[cpt].toarray()[0]
                elif(features == 'tf-idf_liwc'):
                    dict_f[k] = np.array(list(all_set[cpt].toarray()[0]) + list(dict_features[k]))
                cpt+=1
                

        print('  5.2 get split dataset')
        X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_f)
        X, X_train, X_test, y, y_train, y_test = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_f)
        print(' > train shape:', len(X_train), len(X_train[0]))
        
        print('  5.3 training')
        clf, importance = mt.train_model(model, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, features, root)
    
    print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---') 
    
    
    print('-----------------------------------------------------------------------------')



#########################################################################################################################################
################################################### 3. SVM or decision_tree models ######################################################



# if __name__ == "__main__":
#     model_to_train = 'boruta' #decision_tree, random_forest, boruta, linear, SGD_classifier
#     features_to_keep = 'all'
    
   
    
    
   
#     print('------------------------------- TRAIN model for feature selection ------------------------------')
#     t1 = time.time()

#     root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+topic+'/'
#     if(os.path.isdir(root) == False):
#         os.mkdir(root)
#     root+= true_label[0] + '/'
#     if(os.path.isdir(root) == False):
#         os.mkdir(root)
    
#     temp_path = root + topic+'.pickle'
#     if(os.path.isfile(temp_path) == True):
#         print(' >> find pickle data file ! ')
#         dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, X_manual_test, y_manual_test, dict_manual_labels  = load_pickle(temp_path)
#         print(' > proportion train/test =', round(len(train_tweets)/(len(train_tweets)+len(test_tweets)),3))
#     else:
#         #1. load data
#         print(' 1. load data')
#         dict_node_label_sample = get_user_node_label(path_label_qc, code_label, topic)
#         df_tweets = load_data_csv(path_liwc+topic+'_LIWC.csv')
#         df_tweets = df_tweets[df_tweets['topic'] == topic]
        
#         #2. clean data
#         print(' 2. clean data')
#         all_labels = set(df_tweets[true_label[0]].tolist())
#         print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
#         for l in all_labels:        
#             print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
#             print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
        
#         for rm_lab in true_label[1]:
#             df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
#         df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        
#         #3. prepare data
#         print(' 3. prepare data')
#         dict_features = {}
#         dict_text = {}
#         dict_labels = {}
#         for idx, row in df_tweets.iterrows():
#             t_id = row['tweet_id']
#             if(features_to_keep == 'all'):
#                 dict_features[t_id] = list(row[ut.LIWC_FEATURES])
#             else:
#                 dict_features[t_id] = list(row[features_to_keep])  
#             dict_text[t_id] = row['clean_text']
#             dict_labels[t_id] = true_label[2].index(row[true_label[0]])
            
#         #4. get manual dataset
#         manual_path = ut.CURRENT_DATA_FOLD + 'community_labels/' + topic + '__random.json'
#         dict_manual_label_by_usr = load_json(manual_path)
#         X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_features)
            
#         #4. split dataset
#         print(' 4. split dataset (puttin manual tweets on test)')
#         train_tweets, test_tweets = bmt.split_train_test_dataset(dict_labels, dict_manual_labels)
        
#         pickle_save(temp_path, (dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, X_manual_test, y_manual_test, dict_manual_labels))
#         print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')

#     # ************************************************************************************************************************************* #
#     print(' > get accuracy WEAK LABEL:', true_label[0])
#     acc = bmt.get_accuracy_from_dict(dict_labels, dict_manual_labels)
#     print('     > accuracy:', acc, '(', len(dict_manual_labels) ,')')    
    
#     #5. train model
#     print('5. train model:', model_to_train)
    
#     print('  5.1 get splitted dataset')
#     feature_cols = list(df_tweets.columns[ut.LIWC_FEATURES])
#     X, X_train, X_test, y, y_train, y_test = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_features)
    
#     if(model_to_train in ['decision_tree', 'random_forest']):
#         importance = mt.train_model(model_to_train, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, feature_cols, root, features_to_keep)
        
#     elif(model_to_train == 'boruta'):
#         print('  5.1 get ACCEPTED features from boruta from', len(X_train), 'examples w/',len(feature_cols) , 'features!')
#         model = RandomForestClassifier(n_estimators=100, max_depth=5, random_state=42)
#         feat_selector = BorutaPy(
#             verbose=2,
#             estimator=model,
#             n_estimators='auto',
#             random_state=42,
#             max_iter=100)
#         feat_selector.fit(np.array(X_train), np.array(y_train))
        
#         print("\n------Support and Ranking for each feature------")
#         accept = np.array(feature_cols)[feat_selector.support_]
#         irresolution = np.array(feature_cols)[feat_selector.support_weak_]
#         print('Accepted features:')
#         print('----------------------------')
#         print(list(accept))
#         print('----------------------------')
#         print(list(irresolution))
#         write_json(root + model_to_train + '_' + features_to_keep + '_SELECTION.json', {k:1 for k in accept})     
        
#         print('  5.2 train model on SELECTED features')
#         print('     5.2.1 re-prepare data')
#         dict_features = {}
#         for idx, row in df_tweets.iterrows():
#             t_id = row['tweet_id']
#             dict_features[t_id] = list(row[accept])
#         print('     5.2.3 train new model')
#         importance = mt.train_model(model_to_train, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, feature_cols, root, features_to_keep)
        
#     elif(model_to_train == 'linear' or model_to_train == 'SGD_classifier'):
#         mt.train_linear_model(model_to_train, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, feature_cols, root, features_to_keep)



#########################################################################################################################################
################################################### 1. test significatif + boxplot ######################################################

# if __name__ == "__main__":
#     print('------------------------------ TEST SIGNIFICATIF + boxplot ------------------------------')
#     limit_p = 0.05
#     type_test = 'ttest' #mann_whitney, ttest
#     features_from = 'all' #'boruta' 'all'
    
#     t1 = time.time()
    
#     print(' ----------- I- LOAD DATA')
#     root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+topic+'/'
#     if(os.path.isdir(root) == False):
#         os.mkdir(root)
#     root+= true_label[0] + '/'
#     if(os.path.isdir(root) == False):
#         os.mkdir(root)
#     temp_path = root + topic+'.pickle'
#     if(os.path.isfile(temp_path) == True):
#         print(' >> find pickle data file ! ')
#         dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, X_manual_test, y_manual_test, dict_manual_labels  = load_pickle(temp_path)
#     else:
#         #1. load data
#         print(' 1. load data')
#         dict_node_label_sample = get_user_node_label(path_label_qc, code_label, topic)
#         df_tweets = load_data_csv(path_liwc+topic+'_LIWC.csv')
#         df_tweets = df_tweets[df_tweets['topic'] == topic]
    
        
#         #2. clean data
#         print(' 2. clean data')
#         all_labels = set(df_tweets[true_label[0]].tolist())
#         print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
#         for l in all_labels:        
#             print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
#             print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
            
#         for rm_lab in true_label[1]:
#             df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
#         df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        
#         #3. prepare data
#         print(' 3. prepare data')
#         dict_features = {}
#         dict_text = {}
#         dict_labels = {}
#         for idx, row in df_tweets.iterrows():
#             t_id = row['tweet_id']
            
#             dict_features[t_id] = row[ut.LIWC_FEATURES]
#             dict_text[t_id] = row['clean_text']
#             dict_labels[t_id] = true_label[2].index(row[true_label[0]])
            
#         #4. get manual dataset
#         manual_path = ut.CURRENT_DATA_FOLD + 'community_labels/' + topic + '__random.json'
#         dict_manual_label_by_usr = load_json(manual_path)
#         X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set(df_tweets, dict_manual_label_by_usr, dict_features)

#         #4. split dataset
#         print(' 4. split dataset')
#         train_tweets, test_tweets = bmt.split_train_test_dataset(dict_labels, dict_manual_labels)
        
#         pickle_save(temp_path, (dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, X_manual_test, y_manual_test, dict_manual_labels ))
#         print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')
        
        
#     print(' ----------- II- TEST + BOXPLOT', type_test)
#     root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+ topic + '/' + true_label[0] + '/'
    
#     if(os.path.isdir(root+'boxplot/') == False):
#         os.mkdir(root+'boxplot/')
#     if(os.path.isdir(root+'test/') == False):
#         os.mkdir(root+'test/')
        
#     if(features_from == 'boruta'):
#         features_to_keep = load_json(root+'boruta_all_SELECTION.json')
#         features_to_keep = list(features_to_keep.keys())
#     elif(features_from == 'all'):
#         features_to_keep = list(df_tweets.columns[ut.LIWC_FEATURES])
    
#     cpt=1
#     dict_p_value = {}
#     for f in features_to_keep:
#         data = {k:v[f] for k,v in dict_features.items()}
#         x1 = []
#         x2 = []
#         for k,v in data.items():
#             if(dict_labels[k]==0):
#                 x1.append(v)
#             elif(dict_labels[k]==1):
#                 x2.append(v)
                
#         if(cpt == 1):
#             print(' > number of population:', len(x1), '+', len(x2), '=', len(x1)+len(x2))

#         #significativ test
#         sample_x1, sample_x2 = get_random_samples(x1, x2)
#         if(type_test == 'mann_whitney'):
#             U1, p_value = mannwhitneyu(sample_x1, sample_x2, alternative = 'two-sided')
#             p_value = round(p_value, 4)
#         elif(type_test == 'chi2'):
#             tbl = np.stack([[sample_x1, sample_x2]])
#             stat, p_value, df, expected = chi2_contingency(tbl)
#         elif(type_test == 'ttest'):
#             _, p_value = stats.ttest_ind(sample_x1,sample_x2)
            
#         dict_p_value[f] = p_value
#         print('   >', cpt, '/', len(features_to_keep), '\'', f, '\': ', p_value, '       -->', p_value<limit_p)
        
#         #boxplot
#         boxplot_path = root+'boxplot/'+f+'.png'
#         boxplot_title = f+' - '+str(p_value) + '('+ type_test +')' + '-->' + str(p_value<limit_p)
#         mt.plot_boxplots(sample_x1, sample_x2, boxplot_path, boxplot_title)
               
#         cpt+=1
        
#     print(' > Total of', len([k for k,v in dict_p_value.items() if v<limit_p]) , '/', len(features_to_keep), 'significativ features')
#     #write json test result
#     test_path = root+'test/'+type_test+'.json'
#     if(os.path.isfile(test_path) == False):
#         write_json(test_path, dict_p_value)
        