#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 11:12:52 2022

@author: samy
"""

from six import StringIO

from IPython.display import Image  
from sklearn.tree import export_graphviz
# import pydotplus


import matplotlib.pyplot as plt

from sklearn import svm
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier 
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

from scipy.special import softmax
import numpy as np
import pickle
import json
import os




"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

def save_results(path, txt):
    with open(path, 'w') as log_file:
        log_file.write(txt)
    return 1


"""
plot final decision_tree
"""
def plot_decision_tree(clf, feature_cols, final_path):
    dot_data = StringIO()
    export_graphviz(clf, out_file=dot_data,  
                    filled=True, rounded=True,
                    special_characters=True, feature_names = feature_cols,class_names=['0','1'])
    # graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
    # graph.write_png(final_path)
    # Image(graph.create_png())
    
    
def grid_search(X_train, y_train, root, features):
    # X_train_temp = X_train.sample(n=1000, random_state=1)
    # y_train_temp = y_train.loc[X_train_temp.index]
    X_train_temp = X_train
    y_train_temp = y_train
    print('     > shape:', X_train.shape, 'vs sample', X_train_temp.shape)
    
    temp_root = '/'.join(root.split('/')[:-1][:-1]) + '/'
    path_grid = temp_root + 'grid_search_'+ features + '.pickle'
    
    print(' > start gridsearch fast...')
    if(os.path.isfile(path_grid) == True):
        print('      > found!')
        best_parameters = load_pickle(path_grid)
    else:
        grid_param={"max_depth": [20, 30, 40, 50],
             "min_samples_leaf":[20, 30, 35, 40],
             "min_samples_split": [4, 6, 10]
            }
        
        clf = DecisionTreeClassifier()
        grid_search=GridSearchCV(estimator=clf,param_grid=grid_param,cv=5,n_jobs=25)
        grid_search.fit(X_train_temp, y_train_temp)
        best_parameters = grid_search.best_params_
        
        pickle_save(path_grid, best_parameters)
    
    return best_parameters

    
def train_model(model_to_train, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, features, root, data_to_save='manual_test', sampled='', is_grid=True):
    temp_path = root + model_to_train + '_' + features + '.pickle'
    if(os.path.isfile(temp_path) == False):    
        if(is_grid == True):
            best_parameters = grid_search(X_train, y_train, root, features)
            print(' ** best param kept for rf and df:', best_parameters)
         
            print('  * train model (yessss grid)')
            if(model_to_train == 'decision_tree'):
                clf = DecisionTreeClassifier(max_depth= best_parameters['max_depth'],
                                             min_samples_leaf= best_parameters['min_samples_leaf'],
                                             min_samples_split= best_parameters['min_samples_split'])
            elif(model_to_train == 'random_forest'):
                clf = RandomForestClassifier(n_estimators=100,
                                             max_depth= best_parameters['max_depth'],
                                             min_samples_leaf= best_parameters['min_samples_leaf'],
                                             min_samples_split= best_parameters['min_samples_split'])
        else:
            print('  * train model (NO grid)')
            if(model_to_train == 'decision_tree'):
                clf = DecisionTreeClassifier()
            elif(model_to_train == 'random_forest'):
                clf = RandomForestClassifier(n_estimators=100) 
            elif(model_to_train == 'svm'):
                print('svm :)')
                clf = svm.SVC(probability=True)
        
        clf = clf.fit(X_train,y_train)
    else:
        print(' /!\/!\/!\/!\/!\ found model /!\ /!\ /!\ /!\ /!\ ')
        importance, clf = load_pickle(temp_path)
        
    feature_cols = list(X_train.columns)
    print(' X shape:', X_train.shape, X_test.shape, X_manual_test.shape)
    print(' y shape:', y_train.shape, y_test.shape, y_manual_test.shape)
        
    y_test_pred = clf.predict(X_test)
    y_train_pred = clf.predict(X_train)
    if(len(X_manual_test) > 0):
        y_manual_test_pred = clf.predict(X_manual_test)
        
    print(' > data_to_save:', data_to_save)
    if(data_to_save == 'manual_test'):
        data = (X_manual_test, y_manual_test)
    else:
        data = (X_test, y_test)
    if(model_to_train == 'random_forest'):
        all_trees_proba = [] # shape = {nbs_tree, nbs_sample, nbs_class}
        for tree in range(100):
            a = clf.estimators_[tree].predict_proba(data[0])
            all_trees_proba.append(a)   
    elif(model_to_train in ['decision_tree', 'svm']):
        all_trees_proba = []
        a = clf.predict_proba(data[0]) # shape = {nbs_sample, nbs_class}
        all_trees_proba.append(a)
    else:
        all_trees_proba = None
        data = [None, None]
    pickle_save(root + model_to_train + '_' + features + sampled +'_PREDICT_PROBA.pickle', (all_trees_proba, data[1]))

    print('  ** evaluate model')
    print("  >>>>>>> TRAIN Accuracy <<<<<<<<", metrics.accuracy_score(y_train, y_train_pred))
    print(metrics.classification_report(y_train, y_train_pred))
    print("  >>>>>>> TEST Accuracy <<<<<<<<", metrics.accuracy_score(y_test, y_test_pred))
    print(metrics.classification_report(y_test, y_test_pred))
    if(len(X_manual_test) > 0):
        print("  >>>>>>> MANUAL TEST Accuracy <<<<<<<<", metrics.accuracy_score(y_manual_test, y_manual_test_pred))
        print(metrics.classification_report(y_manual_test, y_manual_test_pred))
    
    results = ""
    results += ">>>>>>> TRAIN Accuracy <<<<<<<"
    results += str(metrics.classification_report(y_train, y_train_pred))
    results += '\n'
    results += ">>>>>>> TEST Accuracy <<<<<<<"
    results += str(metrics.classification_report(y_test, y_test_pred))
    results += '\n'
    if(len(X_manual_test) > 0):
        results += ">>>>>>> MANUAL TEST Accuracy <<<<<<<"
        results += str(metrics.classification_report(y_manual_test, y_manual_test_pred))
        results += '\n'
    
    
    print('  *** save best features tree')
    # if(model_to_train == 'decision_tree'):
        # image_path = root + model_to_train + '_' + features_to_keep + '.png'
        # plot_decision_tree(clf, feature_cols, image_path)
    
    importance = {}
    for x,y in zip(feature_cols, clf.feature_importances_):
        importance[x] = y
    pickle_save(root + model_to_train + '_' + features + sampled +'.pickle', (importance, clf))
    
    save_results(root + 'result.model.txt', results)
    
    return clf, importance


def plot_boxplots(x1, x2, path, title):
    my_dict = {'cA': x1, 'cB': x2}
    
    fig, ax = plt.subplots()
    ax.boxplot(my_dict.values())
    ax.set_xticklabels(my_dict.keys())
    fig.suptitle(title)
    fig.savefig(path)
    
    return 1


def train_linear_model(model_to_train, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, feature_cols, root, features_to_keep):
    scaler = StandardScaler().fit(X_train) 
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    
    
    if(model_to_train == 'linear'):
        clf = LogisticRegression(C=1, penalty='l1', solver='liblinear').fit(X_train, y_train)
    elif(model_to_train == 'SGD_classifier'):
        clf = SGDClassifier(max_iter=1000, tol=1e-3, penalty ='l1').fit(X_test, y_test)
        
    # proba_test = clf.predict_proba(X_test)
    
    y_train_pred = clf.predict(X_train)
    y_test_pred = clf.predict(X_test)
    y_manual_test_pred = clf.predict(X_manual_test)
    
    
    print('  ** evaluate model')
    print("  >>>>>>> TRAIN Accuracy <<<<<<<<", metrics.accuracy_score(y_train, y_train_pred))
    print(metrics.classification_report(y_train, y_train_pred))
    print("  >>>>>>> TEST Accuracy <<<<<<<<", metrics.accuracy_score(y_test, y_test_pred))
    print(metrics.classification_report(y_test, y_test_pred))
    print("  >>>>>>> MANUAL TEST Accuracy <<<<<<<<", metrics.accuracy_score(y_manual_test, y_manual_test_pred))
    print(metrics.classification_report(y_manual_test, y_manual_test_pred))
    
    print('  *** save best features tree')
    coeffs = clf.coef_ 
    print('coeff:', coeffs)
    
    return 1
        
    
        
    
    
    
    
"""
Prints the feature importances based on SHAP values in an ordered way
shap_values -> The SHAP values calculated from a shap.Explainer object
features -> The name of the features, on the order presented to the explainer
"""
def print_feature_importances_shap_values(shap_values, features):
    # Calculates the feature importance (mean absolute shap value) for each feature
    importances = []
    shap_values = np.stack(shap_values)
    for i in range(shap_values.shape[1]):
        importances.append(np.mean(np.abs(shap_values[:, i])))
    # Calculates the normalized version
    importances_norm = softmax(importances)
    # Organize the importances and columns in a dictionary
    feature_importances = {fea: imp for imp, fea in zip(importances, features)}
    feature_importances_norm = {fea: imp for imp, fea in zip(importances_norm, features)}
    # Sorts the dictionary
    feature_importances = {k: v for k, v in sorted(feature_importances.items(), key=lambda item: item[1], reverse = True)}
    feature_importances_norm= {k: v for k, v in sorted(feature_importances_norm.items(), key=lambda item: item[1], reverse = True)}
    # Prints the feature importances
    for k, v in feature_importances.items():
        print(f"{k} -> {v:.4f} (softmax = {feature_importances_norm[k]:.4f})")
        
    return feature_importances
    
    
    
    
    
    
    
    
    
    
    
    
    

