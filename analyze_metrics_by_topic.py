#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 16:57:41 2023

@author: samy
"""
import util as ut

from sklearn.metrics import roc_auc_score
import pandas as pd
import json
import os
import pickle
import pickle5

dict_labels = ut.data_zarate_labels

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json data
"""
def load_json(path):
    if(os.path.isfile(path) == True):
        with open(path) as f:
            labels = json.load(f)
    else:
        print(' > no manual data !!!!')
        labels = {}
      
    return labels

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

def load_pickle5(path_file):
    with open(path_file, 'rb') as file:
        data = pickle5.load(file)
    return data


def get_roc_auc_score(dict_accuracy, dict_labels):
    preds = []
    labs = []
    for k,v in dict_accuracy.items():
        preds.append(v)
        labs.append(dict_labels[k])
    
    score = roc_auc_score(labs, preds)
    
    return score

#---------------------------------------------------------------------------------------------

# path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
# topics = ut.topic_qc_zarate

# if __name__ == "__main__":
#     print('------------------ get roc_auc (ratio proportion) --------------')
#     ALL_TWEETS = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')
#     ALL_TWEETS = ALL_TWEETS.drop(['controversy_LABEL'], axis=1)
#     ALL_TWEETS = ALL_TWEETS[ALL_TWEETS['topic'] != 'mothersday']
    
#     dict_prop = {}
#     cpt=0
#     for topic in topics:           
#         print(cpt, '/', len(topics))

#         df_tweets = ALL_TWEETS[ALL_TWEETS['topic'] == topic]

#         c0 = len(df_tweets[df_tweets['metis_label'] == 0])
#         c1 = len(df_tweets[df_tweets['metis_label'] == 1])
        
#         if(c0<c1):
#             ratio = c0/c1
#         else:
#             ratio = c1/c0
            
#         dict_prop[topic] = ratio
#         dict_prop = {k: round(v,3) for k, v in sorted(dict_prop.items(), key=lambda item: item[1])}
        
#         cpt+=1
    
#     score = get_roc_auc_score(dict_prop, dict_labels)
#     print(' >>> roc_auc score : ', score)
    
  
#---------------------------------------------------------------------------------------------

# # topic > "test" > "accuracy"

from_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/TOPICS/dict_results_by_topic_v2.json'

if __name__ == "__main__":
    print('------------------ roc_auc (accuracy) --------------')
    
    dict_results = load_json(from_path)
    
    print(' > nbs_topics:', len(dict_results))
    
    dict_accuracy = {k:v['test']['accuracy'] for k,v in dict_results.items()}
    dict_accuracy = {k: round(v,3) for k, v in sorted(dict_accuracy.items(), key=lambda item: item[1])}
    
    res_score = get_roc_auc_score(dict_accuracy, dict_labels)
    print(' >>> roc_auc score : ', res_score)

        
    
    
score = {}
for k, v in dict_accuracy.items():
    if(dict_prop[k] > 0.3):
        score[k] = v #* dict_prop[k]
score = {k: round(v,3) for k, v in sorted(score.items(), key=lambda item: item[1])}        
print(' >>> roc_auc score : ', get_roc_auc_score(score, dict_labels))   
for k, v in score.items():
    print(k, v, dict_labels[k])
