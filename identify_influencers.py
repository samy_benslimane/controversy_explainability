#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 15:43:59 2022

@author: samy
"""

import util as ut

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import collections
import random
import pickle
import json
import time
import html
import copy
import csv
import os
import re
import networkx as nx


code_xp_LABEL_QC = 'LC_8A'


"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels

def plot_cum_distr(x, l, path):
    n_bins = 100
    fig, ax = plt.subplots(figsize=(8, 4))
    
    # plot the cumulative histogram
    for xx, ll in zip(x,l):
        n, bins, patches = ax.hist(xx, n_bins, density=True, histtype='step',
                                    cumulative=True, label='label='+ str(ll))        
    # tidy up the figure
    ax.grid(True)
    ax.legend(loc='right')
    ax.set_title('Cumulative step histograms')
    ax.set_xlabel('Active user score')
    ax.set_ylabel('Likelihood of occurrence')
    
    plt.savefig(path)
    plt.show()
    
    return 1


#-------------------------------------------------------------------------------------------
    
if __name__ == "__main__":
    fraction = 0.05
    title = '5'
    
    print('-------- START influencers --------')
    from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/kavanaugh_LIWC.csv' #'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'
    # to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/INFLUENCERS_LIWC_'+title+'.csv'
    plot_path = ut.CURRENT_DATA_FOLD + 'data_objects/ANALYSIS/influencer_LABEL/plot_distribution/'
    temp_path = ut.CURRENT_DATA_FOLD + 'data_objects/ANALYSIS/influencer_LABEL/'+title+'%/'
    
    df = load_data_csv(from_path)
    cpt=1

    if(os.path.isfile(temp_path+'dict_influencer_00'+title+'_NEW_KAV.pickle') == True):
        dict_influencer, dict_stats = load_pickle(temp_path+'dict_influencer_00'+title+'_NEW_KAV.pickle')
    else:
        dict_influencer = {}
        dict_stats = {}
        
    # list_topic = ut.topic_qc_zarate
    # dict_labels = ut.data_zarate_labels
    list_topic = ['kavanaugh']
    dict_labels = {'kavanaugh': 1}
    
    for topic in list_topic:
        print(' =============== TOPIC:', cpt, '/', len(list_topic), ':', topic, '====================')
        if(topic in dict_influencer.keys()):
            print('   > already done !')
        else:
            topic_label = dict_labels[topic]
            
            print('0. ')
            # object_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+topic+'/RETWEET/data.pickle'
            # dict_tweets, dict_nodes, dict_edges, _, _, _, _, _, _, _, _, _ = load_pickle(object_path)
            object_path = ut.CURRENT_DATA_FOLD + 'data_objects/S_1A/'+'ALL_KAVANAUGH'+'/RETWEET/data_step2.pickle'
            dict_tweets, dict_nodes, dict_edges, _ = load_pickle(object_path)
            
            # dict_nodes = {k:list(set(v)) if(v is not None) else None for k,v in dict_nodes.items()} #DELETE
            x = [len(v) if(v is not None) else 0 for _,v in dict_nodes.items()]
            
            
            print(topic, '   -->   nbs_tweets:', len(dict_tweets) , '(in_graph=', sum(x) , ')    /   nbs_users:', len(dict_nodes), '(with_tweets:', 
                  round((len([k for k,v  in dict_nodes.items() if(v is not None)])/len(dict_nodes))*100, 2), ')   /   nbs_RT:', len(dict_edges))

            #1. get user w/ tweets
            print('1.')
            df_temp = df[df['topic']==topic]
            df_temp = df_temp.drop(df_temp[df_temp['metis_label'] == -10].index)
            list_user_with_tweets = list(set(df_temp['user_id'].tolist()))
        
            #2. create graph
            print('2.')
            list_edges = [v for _,v in dict_edges.items()]
            G = nx.DiGraph(list_edges)    
            print('    >> G nx : nodes->', len(G.nodes()))
        
            #3. get user ranking
            print('3. get top-Q=', 1-fraction)
            user_ranking = nx.pagerank(G, alpha=0.85)
            user_ranking_with_tweets = {k:user_ranking[k] for k in list_user_with_tweets}
            values = [v for _,v in user_ranking_with_tweets.items()]
            q_max = np.quantile(values, 1-fraction)
            
            print('    >> first value:', min(values), 'represent', round((len([v for v in values if(v==min(values))])/len(values))*100, 2), '%')
            print('    >> min, max | q',str(1-fraction), '=', min(values), ',', max(values), '|', q_max)
            
            #4. PLOT DISTRIB
            print('4.')
            print(' > plot')
            to_plot = np.array(values)
            plot_cum_distr([to_plot], ['alpha:'+str(0.85)], path=plot_path+str(topic_label)+'_'+topic)
        
            #5. get influencer
            print('5.')
            # user_ranking = {k: v for k, v in sorted(user_ranking_with_tweets.items(), key=lambda item: item[1], reverse=True)} #sort
            all_user = list(user_ranking_with_tweets.keys())
            influencer = [k for k,v in user_ranking_with_tweets.items() if(v>=q_max)]
            normal = [k for k,v in user_ranking_with_tweets.items() if(v<q_max)]
            
            total_pr_infl = sum([user_ranking_with_tweets[k] for k in influencer])
            total_pr_normal = sum([user_ranking_with_tweets[k] for k in normal])
            total_pr = sum([user_ranking_with_tweets[k] for k in all_user])
            
            print(' > normal=', round((total_pr_normal/total_pr)*100, 2), '% of total_pageRank (', len(normal), 'users)')
            print(' > influencer=', round((total_pr_infl/total_pr)*100, 2), '% of total_pageRank (', len(influencer), 'users =,',
                  round((len(influencer)/len(all_user))*100, 2),'%)')
            
            dict_influencer[topic] = influencer
            dict_stats[topic] = [[user_ranking_with_tweets[k] for k in influencer],
                                  [user_ranking_with_tweets[k] for k in normal],
                                  [user_ranking_with_tweets[k] for k in all_user]]
        
        print('6.')
        pickle_save(temp_path+'dict_influencer_00'+title+'_NEW_KAV.pickle', (dict_influencer, dict_stats))
        cpt+=1

    #7.
    dict_influencer['mothersday'] = []
    print('7. write file')
    df['influencer_label'] = df.apply(lambda x: 1 if(x['user_id'] in dict_influencer[x['topic']]) else 0, axis=1)
    df[df['topic']=='mothersday']['influencer_label'] = -1
    
    print('3. write') #only keep influencer tweets
    # df = df[df['influencer_label'] == 1]
    # create_csv_file(df, to_path)
    
    for topic in list_topic:
        lab = df[df['topic']==topic]
        lab = lab.drop(lab[lab['metis_label'] == -10].index)
        
        infl = list(set(lab[lab['influencer_label']==1]['user_id'].tolist()))
        all_u = list(set(lab['user_id'].tolist()))
        
        print(topic, ' ->  Tweets from influencers=', len(lab[lab['influencer_label']==1]),
              ' /  Total tweets=', len(lab), ' == ', round((len(lab[lab['influencer_label']==1])/len(lab))*100, 2), '%')
    
    print('----------- end ------------')