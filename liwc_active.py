#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 09:54:26 2022

@author: samy
"""
import matplotlib.pyplot as plt

from scipy.stats import mannwhitneyu
from scipy import stats
import scipy as sp



from transformers import TextClassificationPipeline
from torch.utils.data import DataLoader


from sklearn import metrics
from sklearn.feature_extraction import text
from sklearn.tree import DecisionTreeClassifier 
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer

from scipy.stats import chi2_contingency

import bert_model_train as bmt
import model_train as mt
import util as ut

from shap.plots import waterfall, beeswarm, force

from boruta import BorutaPy
import pandas as pd
import numpy as np
import pickle
import pickle5
import random
import torch
import time
import json
import shap
import csv
import os




def get_user_node_label(path, label_method, topic):
    filename = path +topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        dict_labels = json.load(f)
        
    labels = [v for k,v in dict_labels.items()]
    n_lab = len(list(set(labels)))
    print('---------- LABEL_QC ----------')
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('-------------------------------')
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1
def pickle_save5(path, data):
    with open(path, 'wb') as file:
        pickle5.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels


def change_label(df_tweets, dict_manual_labels, label_name='true_label'):
    new_dict_label = {}
    
    for k in dict_manual_labels.keys():
        new_dict_label[k] = df_tweets[df_tweets['tweet_id']==k][label_name].tolist()[0]
    print(' <active label>:')
    for l in [0,1]:
        print(l,':', len([k for k,v in new_dict_label.items() if(v==l)]))
    
    return new_dict_label


def get_random_samples(x1, x2):
    sample_x1 = []
    sample_x2 = []
    
    if(len(x1)<len(x2)):
        min_lab = len(x1)
        idx = list(random.sample(set(range(len(x2))), min_lab))
        
        sample_x2 = [x2[i] for i in idx]
        sample_x1 = x1   
    elif(len(x2)<len(x1)):
        min_lab = len(x2)
        idx = list(random.sample(set(range(len(x1))), min_lab))
        
        sample_x1 = [x1[i] for i in idx]
        sample_x2 = x2
    else:
        min_lab = len(x1)
        sample_x1 = x1
        sample_x2 = x2
        
    return sample_x1, sample_x2


def predictor_list(list_x):
    print('hiiiiiiiiiiihi +- > (0000000000)')
    out = []
    val = []
    for x in list_x:
        o, v = predictor_l(x)
        out.append(o)
        val.append(v)
        
    return out, val

def predictor_l(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()} 
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
        
    outputs, out_embedding = p_model(new_inputs)
        
    outputs = outputs.detach().cpu().numpy()

    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return outputs, val



def predictor(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()}
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
    
    outputs, out_embedding = p_model(new_inputs)
    
    outputs = outputs.detach().cpu().numpy()
    
    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return val


def f_batch(x):
    val = np.array([])
    for i in x:
      val = np.append(val, predictor(i))
    return val


def get_labeled_active_tweets(df_tweets):
    old_s = len(df_tweets)
    if(type_score == 'custom_score'):
        for rm_lab in true_label[1] + [-1]:
            print(' > TO DELEEEEEEETE:', len(df_tweets[df_tweets[true_label[0]] == rm_lab]))
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
            
    elif(type_score in ['score', 'nbs_degree']):
        for rm_lab in true_label[1]:
            print(' > TO DELEEEEEEETE:', len(df_tweets[df_tweets[true_label[0]] == rm_lab]))
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
        df_tweets = df_tweets.drop(true_label[0], axis=1)   
        old_s = len(df_tweets)
            
        print('1b. sort data & get fraction *', quartile_percent)
        srt = df_tweets.sort_values(type_score, ascending = True)
        df_non_active = srt.head(int(len(df_tweets)*quartile_percent))
        df_non_active[true_label[0]] = [0] * len(df_non_active)
        df_active = srt.tail(int(len(df_tweets)*quartile_percent))
        df_active[true_label[0]] = [1] * len(df_active)
        df_tweets = pd.concat([df_active, df_non_active])
        
    elif(type_score == 'd_in_out_nbstweets'):
        for rm_lab in true_label[1]:
            print(' > TO DELEEEEEEETE:', len(df_tweets[df_tweets[true_label[0]] == rm_lab]))
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
        df_tweets = df_tweets.drop(true_label[0], axis=1)
        
        print('1b. normalize scores')
        tot_d_in = sum(df_tweets['d_in'].tolist())
        tot_d_out = sum(df_tweets['d_out'].tolist())
        tot_nbs_tweets = sum(df_tweets['nbs_tweets'].tolist())
        print(' >> TOTAL -> d_in:', tot_d_in, '| d_out:', tot_d_out, '| nbs_tweets:', tot_nbs_tweets)
        df_tweets['d_in'] = df_tweets.apply(lambda x: (x['d_in']/tot_d_in) * 1000, axis=1)
        df_tweets['d_out'] = df_tweets.apply(lambda x: (x['d_out']/tot_d_out) * 1000, axis=1)
        df_tweets['nbs_tweets'] = df_tweets.apply(lambda x: (x['nbs_tweets']/tot_nbs_tweets) * 1000, axis=1)
        df_tweets['temp_new_score'] = df_tweets.apply(lambda x: x['d_in'] + x['d_out'] + x['nbs_tweets'], axis=1)
        old_s = len(df_tweets)
        
        print('1c. sort data & get fraction *', quartile_percent)
        srt = df_tweets.sort_values('temp_new_score', ascending = True)
        df_non_active = srt.head(int(len(df_tweets)*quartile_percent))
        df_non_active[true_label[0]] = [0] * len(df_non_active)
        df_active = srt.tail(int(len(df_tweets)*quartile_percent))
        df_active[true_label[0]] = [1] * len(df_active)
        df_tweets = pd.concat([df_active, df_non_active])
        
    elif(type_score == 'd_out_nbs_tweets'):
        for rm_lab in true_label[1]:
            print(' > TO DELEEEEEEETE:', len(df_tweets[df_tweets[true_label[0]] == rm_lab]))
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
        df_tweets = df_tweets.drop(true_label[0], axis=1)
        
        print('1b. normalize scores')
        tot_d_out = sum(df_tweets['d_out'].tolist())
        tot_nbs_tweets = sum(df_tweets['nbs_tweets'].tolist())
        print(' >> TOTAL -> d_out:', tot_d_out, '| nbs_tweets:', tot_nbs_tweets)
        df_tweets['d_out'] = df_tweets.apply(lambda x: (x['d_out']/tot_d_out) * 1000, axis=1)
        df_tweets['nbs_tweets'] = df_tweets.apply(lambda x: (x['nbs_tweets']/tot_nbs_tweets) * 1000, axis=1)
        df_tweets['temp_new_score'] = df_tweets.apply(lambda x: x['d_out'] + x['nbs_tweets'], axis=1)
        old_s = len(df_tweets)
        
        print('1c. sort data & get fraction *', quartile_percent)
        srt = df_tweets.sort_values('temp_new_score', ascending = True)
        df_non_active = srt.head(int(len(df_tweets)*quartile_percent))
        df_non_active[true_label[0]] = [0] * len(df_non_active)
        df_active = srt.tail(int(len(df_tweets)*quartile_percent))
        df_active[true_label[0]] = [1] * len(df_active)
        df_tweets = pd.concat([df_active, df_non_active])
        
    print(' >>> WE REMOVED FROM A/NA',old_s - len(df_tweets),'TWEETS')
    return df_tweets
    
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


topic = 'pelosi'
code_label = 'LC_8A'
path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
path_label_qc = ut.CURRENT_DATA_FOLD + ut.object_folder + 'LABELS/' +code_label+'/'+topic+'/RETWEET/'
quartile_percent = 0.2

true_label = ['active_label', [-30], [0,1]] #['metis_label', [-10], [0,1]], ['LABEL_QC_label', [-20,0], [1,2]]
community = 'inter' #inter intra
device = [0]

#########################################################################################################################################
#################################################### 2. train models #######################################################

if __name__ == "__main__":
    print('------------------------------- TRAIN CLASSIFICATION MODEL (c1 v c2) ------------------------------')
    t1 = time.time()
    
    type_score = 'custom_score' #'custom_score', 'score', 'nbs_degree', 'd_in_out_nbstweets' 'd_out_nbs_tweets'
    model = 'decision_tree' #'decision_tree', 'random_forest', 'bert'
    features = 'liwc' #'tf-idf', 'liwc', 'tf-idf_liwc', 'text', 'text_liwc'
    
    print(' -----', model, ' |', features, '|', community, '|', type_score, '-----')
    ####### LOAD DATA
    manual_path = ut.CURRENT_DATA_FOLD + 'community_labels/' + topic + '__random.json'
    dict_manual_label_by_usr = load_json(manual_path)    

    root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+topic+'/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
    root+= true_label[0] + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
    root+= community + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    temp_path = root + topic+'.pickle'
    if(os.path.isfile(temp_path) == True):
        print(' >> find pickle data file ! ')
        dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels = load_pickle(temp_path)
        print(' > proportion train/test =', round(len(train_tweets)/(len(train_tweets)+len(test_tweets)),3))
    else:
        #1. load data
        print(' 1. load data')
        dict_node_label_sample = get_user_node_label(path_label_qc, code_label, topic)
        df_tweets = load_data_csv(path_liwc+topic+'_ACTIVE_LIWC.csv')
        df_tweets = df_tweets[df_tweets['topic'] == topic]
        
        #get community concerned
        df_tweets = df_tweets[df_tweets['metis_label'] != -10]
        if(community == 'intra_0'):
           df_tweets = df_tweets[df_tweets['metis_label'] == 0]
        elif(community == 'intra_1'):
           df_tweets = df_tweets[df_tweets['metis_label'] == 1]
        
        #get df_tweets with good labels        
        df_tweets = get_labeled_active_tweets(df_tweets)
        
        #2. clean data
        print(' 2. clean data')
        var = 'score'
        all_labels = set(df_tweets[true_label[0]].tolist())
        print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
        for l in all_labels:        
            print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
            print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
            if(type_score in ['score', 'nbs_degree', 'custom_score']):
                if(type_score == 'custom_score'):
                    var = 'score'
                else:
                    var = type_score
            else:
                var = 'temp_new_score'
                
            temp = df_tweets[df_tweets[true_label[0]] == l][var].tolist()
            print('            >  (min, max, mean) degree -> (', min(temp) ,',', max(temp) ,',', round(sum(temp)/len(temp), 2) ,')')
            
        df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        
        #3. prepare data
        print(' 3. prepare data')
        dict_features = {}
        dict_text = {}
        dict_labels = {}
        for idx, row in df_tweets.iterrows():
            t_id = row['tweet_id']
            
            dict_features[t_id] = row[ut.LIWC_FEATURES]
            dict_text[t_id] = row['clean_text']
            dict_labels[t_id] = true_label[2].index(row[true_label[0]]) # dict_labels[t_id] = row[true_label[0]]
            
       #4. get manual dataset
        _, _, dict_manual_labels = bmt.get_manual_test_set_active_user(df_tweets, dict_manual_label_by_usr, dict_features, true_label[0])

        #4. split dataset
        print(' 4. split dataset (putting manual tweets on test)')
        train_tweets, test_tweets = bmt.split_train_test_dataset(dict_labels, dict_manual_labels)
        
        pickle_save(temp_path, (dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels))
        print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')

    #--------------------------------------------------------------------------------------------------------------------------

    root = root + model + '_' + features + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    ########################################## 5. train BERT model ##########################################
    print(' 5. train model')
    if(model == 'bert'):
        print(' ----', model, '----')
        if(features == 'text'):
            combined_dict_f = None
        elif(features == 'text_liwc'):
            combined_dict_f = {k:v.tolist() for k,v in dict_features.items()}

        X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set_active_user(df_tweets, dict_manual_label_by_usr, dict_text, true_label[0], combined_dict_f)

        p_model, tokenizer = bmt.bert_fine_tuning(train_tweets, test_tweets, X_manual_test, y_manual_test, dict_labels, dict_text, combined_dict_f, topic, features, device, root, true_label[0],
                            bert_epochs = 30, bert_batches = 32, max_length = 256, lr_rate = 2e-5,
                            truncate = True, discr_lr = True, k_fold = 'NO')
        p_model.eval()
        
            
        print('  5.4 get shap values')
        if(features == 'text_liwc'):
            print(' > no shap !!')
        else:
            
            print('       5.4.0 get test data')   
            if(features == 'text'):
                other_f = None
            elif(features == 'text_liwc'):
                other_f = [dict_features[k] for k in test_tweets]               

            # ------------------------------- # -------------------------------
            print('----------- 6. GET SHAP VALUES !!!! -----------')
            print(' >1. explainer')
            explainer = shap.Explainer(f_batch, tokenizer)

            X, X_train, X_test, y, y_train, y_test, _ = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_text, combined_dict_f)
           
            print('###### TEST')
            print(' > shap over', len(X_test), 'records')
            sample_X_list = bmt.clean_text_v2(X_test[0].tolist())
            sample_X = X_test
            sample_y = y_test
            
            outputs, val = predictor_list(sample_X_list)
            print(' >2. shap values')
            shap_values_TEST = explainer(sample_X_list, fixed_context=1)
            
            data = (shap_values_TEST, sample_X, sample_y, outputs, val, sample_X_list)
            pickle_save(root+'/shap_TEST.pickle', data)
            
            
            print('###### TRAIN')
            print(' > shap over', len(X_train), 'records')
            sample_X_list = bmt.clean_text_v2(X_train[0].tolist())
            sample_X = X_train
            sample_y = y_train
            
            outputs, val = predictor_list(sample_X_list)
            print(' >2. shap values')
            shap_values_TRAIN = explainer(sample_X_list, fixed_context=1)
            
            data = (shap_values_TRAIN, sample_X, sample_y, outputs, val, sample_X_list)
            pickle_save(root+'/shap_TRAIN.pickle', data)
    
    
    ########################################## 5. train OTHER model ##########################################
    elif(model == 'decision_tree' or 'random_forest'):
        print(' ----', model, '----')
        print('  5.1 get features')
        
        if(features == 'liwc'):
            feature_names = ut.LIWC_FEATURES
            dict_f = dict_features
        elif(features == 'tf-idf' or features == 'tf-idf_liwc'):
            my_stop_words = []
            print(' > rm stop words')
            my_stop_words = text.ENGLISH_STOP_WORDS
            vectorizer = TfidfVectorizer(min_df=3, stop_words=my_stop_words)
            print('clean 1')
            train_tf = [bmt.clean_text_v2(dict_text[k], False) for k in train_tweets]
            vectorizer.fit(train_tf)
            
            print('clean 2')
            dict_f = {}
            all_set = [bmt.clean_text_v2(v, False) for k,v in dict_text.items()]
            all_set = vectorizer.transform(all_set)
            cpt=0
            for k,v in dict_text.items():
                if(features == 'tf-idf'):
                    dict_f[k] = all_set[cpt].toarray()[0]
                elif(features == 'tf-idf_liwc'):
                    dict_f[k] = np.array(list(all_set[cpt].toarray()[0]) + list(dict_features[k]))
                cpt+=1
                
            if(features == 'tf-idf'):
                feature_names = vectorizer.get_feature_names() #[str(i) for i in range(len(all_set[0].toarray()[0]))] 
            elif(features == 'tf-idf_liwc'):
                feature_names = vectorizer.get_feature_names() + ut.LIWC_FEATURES #[str(i) for i in range(len(all_set[0].toarray()[0]))]
        print('    > len features:', len(feature_names))

        print('  5.2 get split dataset')
        X_manual_test, y_manual_test, dict_manual_labels = bmt.get_manual_test_set_active_user(df_tweets, dict_manual_label_by_usr, dict_f, true_label[0])
        X_manual_test = pd.DataFrame(X_manual_test, columns=feature_names, index=list(dict_manual_labels.keys()))
        y_manual_test = pd.DataFrame(y_manual_test, columns=['label'], index=list(dict_manual_labels.keys()))
        X, X_train, X_test, y, y_train, y_test, (X_train_idx, X_test_idx) = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_f, feature_names=feature_names)
        print(' > train shape:', X_train.shape)
        print(' > test shape:', X_test.shape)


        print('  5.3 training')
        clf, importance = mt.train_model(model, X_train, X_test, X_manual_test, y_train, y_test, y_manual_test, features, root, 'test')
        
        #------------------------------ SHAP -------------------------------

        print('  5.4 get shap values')
        print('       5.4.1 sampling data over', len(X_manual_test), 'records --NO SAMPLING--')
        idx_to_use = X_test_idx
        sample_X = X_manual_test
        sample_y = y_manual_test
        clf_sample = clf
        
        print('      5.4.3 [TEST] Explaining over', len(sample_X), 'records')
        time_ex = time.time()
        explainer = shap.TreeExplainer(clf_sample, sample_X)
        print(' >1.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_obj = explainer(sample_X, check_additivity=False)
        print(' >2.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_values = explainer.shap_values(sample_X, check_additivity=False)
        print(' >3.', round(((time.time() - time_ex)/60), 2), 'min')
        
        temp_path = root + '/shap_test_v1.pickle'
        pickle_save(temp_path, (shap_obj, shap_values, sample_X))
        temp_path = root + '/shap_test_tf_v1_pickle5.pickle'
        pickle_save5(temp_path, (shap_obj, shap_values, sample_X))
        
        
    print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---') 
    
    print('-----------------------------------------------------------------------------')