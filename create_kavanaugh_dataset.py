#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 22 15:55:14 2022

@author: samy
"""

import bert_model_train as bmt
import util as ut

from deep_translator import GoogleTranslator
import matplotlib.pyplot as plt
from collections import Counter
import seaborn as sns
import pandas as pd
import numpy as np
import collections
import random
import pickle
import metis
import json
import time
import html
import copy
import csv
import os
import re
import networkx as nx



def clean_text_v2(tweet):
    #remove retweet
    tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
    
    #remove URL
    tweet = re.sub(r'http\S+', r'URL', tweet)
    
    # # remove @
    tweet = re.sub(r'@[^\s]+',r'HASHTAG', tweet)
    
    #remove non-ascii words and characters
    # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
    # tweet = ''.join(tweet)
    # tweet = re.sub(r'_[\S]?',r'', tweet)
    # tweet = re.sub(r'\n',r' ', tweet)
    
    # lower case and strip white spaces at both ends
    # tweet = tweet.lower()
    # tweet = tweet.strip()
    
    tweet = html.unescape(tweet)
        
    return tweet

"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df
    
"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels

def plot_cum_distr(x, l):
    n_bins = 100
    fig, ax = plt.subplots(figsize=(8, 4))
    
    # plot the cumulative histogram
    for xx, ll in zip(x,l):
        n, bins, patches = ax.hist(xx, n_bins, density=True, histtype='step',
                                    cumulative=True, label='label='+ str(ll))        
    # tidy up the figure
    ax.grid(True)
    ax.legend(loc='right')
    ax.set_title('Cumulative step histograms')
    ax.set_xlabel('Active user score')
    ax.set_ylabel('Likelihood of occurrence')
    
    plt.show()
    return 1


def transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights=None, to_connex = True):
    list_edges = [v for k,v in dict_edges.items()]
    G = nx.Graph()
    G.add_edges_from(list_edges)
    print('    > number of components:', len([len(x) for x in nx.connected_components(G)]))
    
    if(to_connex == False):
        print('/!\ WE EVEN NON-CONNEX COMPONENT /!\ ')
        return dict_nodes, dict_edges, dict_edge_weights
    
    print(' /!\ WE KEEP ONLY THE LARGEST CONNEX COMPONENT cc /!\ ')

    is_connected = nx.is_connected(G)   
    if(is_connected == True):
        print(' > the graph is ALREADY connected')
        return dict_nodes, dict_edges, dict_edge_weights
                
    else:
        print(' > the graph is NOT connected')
        largest_cc = max(nx.connected_components(G), key=len)

        print('    > number of components:', len([len(x) for x in nx.connected_components(G)]),
              ' --- biggest component:', len(largest_cc), '(DELETE ', len(dict_nodes)-len(largest_cc), 'nodes)')
    
        dict_edges_cc =  {}
        for ts, e in dict_edges.items():
            if(e[0] in largest_cc and e[1] in largest_cc):
                dict_edges_cc[ts] = e
                
        #keep connected edges and nodes info
        dict_nodes_cc = {}
        for n in largest_cc:
            dict_nodes_cc[n] = dict_nodes[n]
        dict_edge_weights_cc = {}
        if(dict_edge_weights is not None):
            for ts, e in dict_edges_cc.items():
                dict_edge_weights_cc[e] = dict_edge_weights[e]
            
        #verify the new connectivity
        test_G = nx.Graph()
        test_G.add_edges_from([v for k,v in dict_edges_cc.items()])
        print('    > new graph is connected:', nx.is_connected(test_G), 'with', len(dict_edges_cc), 'edges')
        print('>> number of nodes deleted: ', len(dict_nodes)-len(largest_cc), ', edges deleted: ', len(dict_edges)-len(dict_edges_cc))
            
        
        return dict_nodes_cc, dict_edges_cc, dict_edge_weights_cc


def graph_partitionning(list_edges, method='metis', p=2):
    if(method == 'metis'):
        G=nx.Graph()
        G.add_edges_from(list_edges)
            
        (edgecuts, parts) = metis.part_graph(G, p)

        part0 = 0
        part1 = 0
        for p in parts:
            if(p==0):
                part0+=1
            else:
                part1+=1

        print("//stats partitionning//")
        print("com1 : (", part0, ")+ com2 (", part1 , ") = ", part0+part1)
        print("///////////////////////")

    return G.nodes, parts

def create_communities_with_metis(dict_edges):   
    list_edges = [dict_edges[ts] for ts in dict_edges.keys()]
    nodes, parts = graph_partitionning(list_edges, method='metis', p=2)
    
    dict_node_label = {}
    for node, com in zip(nodes,parts):
       dict_node_label[node] = com
        
    return dict_node_label

def save_giphi_files(to_path, dict_edges, dict_node_label):
    if(os.path.isfile(to_path+'edges_giphi.csv') == True):
        print("-- *Giphi files* already created --")
    else:
        node_filename = to_path + 'nodes_giphi.csv'
        with open(node_filename, mode='w') as nf:
            writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_node.writerow(['Id','Label','modularity_class'])
        
            if(dict_node_label is not None):
                for user in dict_node_label.keys():
                    writer_node.writerow([str(user), '', str(dict_node_label[user])])
                    
        
        edge_filename = to_path + 'edges_giphi.csv'
        with open(edge_filename, mode='w') as ef:
            writer_edge = csv.writer(ef, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_edge.writerow(['Source','Target','Type', 'Id', 'Label']) 
            
            for ts in dict_edges.keys():
                edge = dict_edges[ts]
                writer_edge.writerow([str(edge[0]),str(edge[1]),'Undirected','',''])
    print(' > saved !')
    return 1

def change_label(label):
    if(label == 0):
        new_label = 1
    elif(label == 1):
        new_label = 0
    else:
        new_label = label
        
    return new_label
#==========================================================================================================
#==========================================================================================================
#==========================================================================================================

to_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/ALL_KAVANAUGH/RETWEET/'

all_topics = ['kavanaugh06-08',
              'kavanaugh16',
              'kavanaugh02-05']
# all_topics = ['OTDirecto20E']
task = 'kavanaugh' #kavanaugh 1Direction


print(' ------- I- merge 3 graph into a 1 --------')

dict_tweets = {}
dict_nodes = {} 
dict_edges = {}

file = 'data_step1.pickle'
if(os.path.isfile(to_path+file) == False):
    for topic in all_topics:
        print(' >>>', topic)
        from_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+topic+'/RETWEET/data.pickle'
        d_tweets, d_nodes, d_edges, _, _, _, _, _, _, _, _, _ = load_pickle(from_path)
        print('  - info:', len(d_nodes), len(d_edges), len(d_tweets))
        
        for k,v in d_nodes.items():
            if(k not in dict_nodes.keys()):
                dict_nodes[k] = v
            else:
                temp = dict_nodes[k]
                if(temp is None):
                    temp = v
                else:
                    if(v is not None):
                        temp += v
        print('  *nodes done!')
        
        for k,v in d_tweets.items():
            dict_tweets[k] = v
        print('  *tweets done!') 
        
        cpt = len(dict_edges)
        current = 0
        for _,v in d_edges.items():
            dict_edges[cpt+current] = v
            current+=1
        print('  *edges done!')

    dict_nodes = {k:list(set(v)) if(v is not None) else None for k,v in dict_nodes.items()}  
    
    # dict_nodes, dict_edges, _ = transform_graph_into_connex_graph(dict_nodes, dict_edges)
    pickle_save(to_path+file, (dict_tweets, dict_nodes, dict_edges))
else:
    print('  > $already done$')
    dict_tweets, dict_nodes, dict_edges = load_pickle(to_path+file)
    
print('  > FINAL info:', len(dict_nodes), len(dict_edges), len(dict_tweets))

print(' ------- II- partition in 2 communities --------')
file = 'data_step2.pickle'
if(os.path.isfile(to_path+file) == False):
    dict_node_label = create_communities_with_metis(dict_edges)
    pickle_save(to_path+file, (dict_tweets, dict_nodes, dict_edges, dict_node_label))
else:
    print('  > $already done$')
    dict_tweets, dict_nodes, dict_edges, dict_node_label = load_pickle(to_path+file)
    
    
x = [len(v) if(v is not None) else 0 for _,v in dict_nodes.items()]
print(task, '   -->   nbs_tweets:', len(dict_tweets) , '(in_graph=', sum(x) , ')    /   nbs_users:', len(dict_nodes), '(with_tweets:', 
                  round((len([k for k,v  in dict_nodes.items() if(v is not None)])/len(dict_nodes))*100, 2), ')   /   nbs_RT:', len(dict_edges))


print(' ------- III- create gephi files --------')
save_giphi_files(to_path, dict_edges, dict_node_label)

print(' ------- IV- Get tweets w/ LIWC + label --------')
root = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_ZARATE/LIWC/'
from_path = root+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'
to_path = root+task+'_LIWC.csv'

df_tweets = load_data_csv(from_path)

new_df = df_tweets[df_tweets['topic'].isin(all_topics)]
print('1->', len(new_df))
new_df = new_df.assign(topic=task)
new_df = new_df.drop(['metis_label', 'controversy_LABEL'], axis=1)
new_df = new_df.drop_duplicates(subset=['tweet_id'])
print('2->', len(new_df))
new_df['metis_label'] = new_df.apply(lambda x: dict_node_label[x['user_id']] if(x['user_id'] in dict_node_label.keys()) else -10, axis=1)
new_df['metis_label'] = new_df.apply(lambda x: change_label(x['metis_label']), axis=1)
# create_csv_file(new_df, to_path)
print(' > saved !')