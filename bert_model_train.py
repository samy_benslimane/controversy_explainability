#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 15:09:33 2021

@author: samy
"""

#!/usr/bin/env python
# coding: utf-8

##################### PACKAGES #####################
############################
##
##  - SUPPRIMER FUNCTION WRITE_TEMP()
##  - SUPPRIMER FUNCTION WRITE_TEMP_TOKENS()
##
###########################

import util as ut

from sklearn.metrics import classification_report, accuracy_score
from sklearn.model_selection import train_test_split
from transformers import BertTokenizer, BertModel
from transformers import (AdamW, 
                          get_linear_schedule_with_warmup,
                          set_seed)

from torch.utils.data import WeightedRandomSampler
from torch.utils.data import DataLoader
import torch.nn.functional as F
from torch import nn
import torch

from sklearn import metrics

from os import path

import pandas as pd
import numpy as np

import random
import pickle5
import pickle
import copy
import time
import html
import re
import os


################### VARIABLE ####################
TO_DELETE = False
bert_m_folder = ut.bert_model_folder # + 'TOPICS_v2/'
################## DATA CLASS ###################
  
"""
bert_fine_tuning with train & test set only
"""
def bert_fine_tuning(train_tweets, test_tweets, X_manual_test, y_manual_test,
                     dict_tweet_label, dict_text, combined_dict_f,
                     topic, features, device, root, true_label,
                     is_topk = False,
                     is_randomk = False,
                     bert_epochs = 30,
                     bert_batches = 64,
                     max_length = 256,
                     lr_rate = 2e-5,
                     truncate = True,
                     discr_lr = True,
                     k_fold = 'NO',
                     task = 'controversial'):
    
    # bert_epochs = 10
    print('<<<<<<<<<<<<<<<<<<<< TRAIN BERT MODEL FT >>>>>>>>>>>>>>>>>>>>>>>')
    print('******************************')
    print('       INIT PARAMETERS       ')
    print('+ topic : ', topic, 'is_topk=', is_topk, 'is_randomk=', is_randomk)
    print('+ features : ', features)
    print('+ size train/test : ', len(train_tweets), len(test_tweets))
    print('+ task : ', task)
    print('+ nbs tweets : ', len(dict_text))
    print('+ device : ', device)
    print('+ bert_epochs : ', bert_epochs)
    print('+ bert_batches : ', bert_batches)
    print('+ max_length : ', max_length)
    print('+ lr_rate : ', lr_rate)
    print('+ truncate : ', truncate)
    print('+ discr_lr : ', discr_lr)
    print('+ *early stoping*')
    print('******************************')  
    
    
    
    if(topic is not None):
        path_fine_tuned_model  = ut.CURRENT_APP_FOLD + bert_m_folder + topic+'_'+task+'_'+features+'_BERT.pth'
    else:
        if(is_topk == True):
            path_fine_tuned_model  = ut.CURRENT_APP_FOLD + bert_m_folder + task+'_'+features+'_topk_BERT.pth'
        elif(is_randomk == True):
            path_fine_tuned_model  = ut.CURRENT_APP_FOLD + bert_m_folder + task+'_'+features+'_randomk_BERT.pth'
        else:
            path_fine_tuned_model  = ut.CURRENT_APP_FOLD + bert_m_folder + task+'_'+features+'_BERT.pth'
    print(path_fine_tuned_model)
    
    start_time_1 = time.time()
    set_seed(123)
    
    # Loading tokenizer...
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer)
    
    #... And load model...
    if(os.path.isdir(ut.BASIC_MODEL)==False):
        pretrained_bert_model = BertModel.from_pretrained(ut.basic_hugging_face)
    else:
        pretrained_bert_model = BertModel.from_pretrained(ut.BASIC_MODEL)        
        
    #Add new token
    # print('   > add new token')
    # new_token = tokenizer.add_tokens(["pelosi"])
    # print('       >> add new token "pelosi":', new_token)
    
    # weights = pretrained_bert_model.embeddings.word_embeddings.weight.data
    # print('     >> old shape model token:', pretrained_bert_model.embeddings.word_embeddings.weight.data.shape)
    # new_weights_tok = torch.cat((weights, weights[101:102]), 0)
    # new_emb_tok = nn.Embedding.from_pretrained(new_weights_tok, padding_idx=0, freeze=False) 
    # pretrained_bert_model.embeddings.word_embeddings = new_emb_tok
    # print('     >> new shape model token:', pretrained_bert_model.embeddings.word_embeddings.weight.data.shape)
    
    
        
    #delete previous fine-tuned model, because different split train/test
    if(path.exists(path_fine_tuned_model) == True and TO_DELETE == False and k_fold == 'NO'):
        print('- /!\ a model fine-tuned was found & KEPT !')
        model = torch.load(path_fine_tuned_model)
        return model, tokenizer, None
        
    # if(path.exists(path_fine_tuned_model) == True):
    #     os.remove(path_fine_tuned_model)
    #     print('- /!\ a model fine-tuned was found & DELETED !')
    
        
    
    # Get features
    dim_other_feat = 0
    if(combined_dict_f is not None):
        k = list(combined_dict_f.keys())[0]
        dim_other_feat = len(combined_dict_f[k])
    print(' > dim_other_feat : ', dim_other_feat)

    model = Tweet_polarization_BERTModel(ut.basic_hugging_face, pretrained_bert_model, dim_other_feat=dim_other_feat)
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
    
    
    ########################### DATA ##############################
    ###### Get data
    X, X_train, X_test, y, y_train, y_test, (_, _) = get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text, combined_dict_f)
    
    #df to list
    y_train = y_train['label'].tolist()
    y_test = y_test['label'].tolist()
    
    if(combined_dict_f is None):
        X_train = X_train[0].tolist()
        X_test = X_test[0].tolist()
        
        X_train = clean_text_v2(X_train)
        X_test = clean_text_v2(X_test)
        X_manual_test = clean_text_v2(X_manual_test)
        train_other_feat = None
        test_other_feat = None
        manual_test_other_feat = None
    else:
        temp_train = X_train
        temp_test = X_test
        temp_manual_test = X_manual_test
        X_train = clean_text_v2(temp_train[0].tolist())
        X_test = clean_text_v2(temp_test[0].tolist())
        X_manual_test = []
        if(len(temp_manual_test) != 0):
            X_manual_test = clean_text_v2([i[0] for i in temp_manual_test])    
            
        train_other_feat = temp_train[1].tolist()
        test_other_feat = temp_test[1].tolist()        
        manual_test_other_feat = []
        if(len(temp_manual_test) != 0):
            manual_test_other_feat = [i[1] for i in temp_manual_test]
        
        print(X_train[0])
        print(train_other_feat[0])
        
    ###### DATALOADERS
    print('#### 1.3.1 TOKENIZATION ####')
    
    # Train DATASET                        
    print('Dealing with Train...')
    train_dataset = TwitterDataset(X_train, y_train, other_features=train_other_feat,
                                   tokenizer=tokenizer,
                                   max_sequence_len=max_length)
    print('Created `train_dataset` with', len(train_dataset), 'examples!')
    if(truncate == True):
        sampler = get_balanced_sampler(y_train)
        train_dataloader = DataLoader(train_dataset,
                                      sampler=sampler,
                                      batch_size=bert_batches)
    else:
        train_dataloader = DataLoader(train_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=True)
    test_train_proportion(train_dataloader)
    
    
    # Manual Test DATASET
    print('\n Dealing with manual Test...', len(X_manual_test))
    manual_test_dataset =  TwitterDataset(X=X_manual_test, y=y_manual_test, other_features=manual_test_other_feat,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `manual_test_dataset` with', len(manual_test_dataset), 'examples!')
    manual_test_dataloader = DataLoader(manual_test_dataset, batch_size=bert_batches, shuffle=False)
    
    # Test DATASET
    print('Dealing with Test...')
    test_dataset =  TwitterDataset(X=X_test, y=y_test, other_features=test_other_feat,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset` with', len(test_dataset), 'examples!')
    test_dataloader = DataLoader(test_dataset, batch_size=bert_batches, shuffle=False)
    
    # Test DATASET w/ balanced ds
    print('Dealing with test balanced...')
    test_dataset_balanced =  TwitterDataset(X=X_test, y=y_test, other_features=test_other_feat,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset_balanced` with', len(test_dataset_balanced), 'examples!')
    if(truncate == True):
        sampler = get_balanced_sampler(y_test)
        test_balanced_dataloader = DataLoader(test_dataset_balanced,
                                      sampler=sampler,
                                      batch_size=bert_batches, shuffle=False)
    else:
        print('--------- no truncate ------------')
        test_balanced_dataloader = DataLoader(test_dataset_balanced,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)        
    
    #----------------------------------------------------------------------------------------------
    
    ######################### TRAINING ############################
    #optimizer
    print(' > add weight decay')
    if(discr_lr == True):
        grouped_parameters = configure_optimizers(model, lr_rate)
        optimizer = AdamW(grouped_parameters, lr = lr_rate, eps = 1e-8, weight_decay=0.05)
    else:
        optimizer = AdamW(model.parameters(), lr = lr_rate, eps = 1e-8, weight_decay=0.05)
        
    criterion = nn.CrossEntropyLoss()
    
    # Total number of training steps is number of batches * number of epochs.
    total_steps = len(train_dataset) * bert_epochs
    
    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 0, # Default value in run_glue.py
                                                num_training_steps = total_steps)
    
    
    print('#### 1.3.4. START BERT TRAINING (learning rate :', lr_rate,') ####')
    train_loss_list = []
    test_loss_list = []
    test_b_loss_list = []
    
    train_acc_list = []
    test_acc_list = []
    test_b_acc_list = []
    
    start_epoch = 0
    
    fold_chkpt = ut.CURRENT_APP_FOLD + ut.bert_model_folder + ut.checkpoint_folder
    if(len(os.listdir(fold_chkpt)) >0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > find checkpoint!')
            chk = torch.load(checkpoint_path)
            
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            
            train_loss_list = chk['train_loss']
            test_loss_list = chk['test_loss']
            train_acc_list = chk['train_acc']
            test_acc_list = chk['test_acc']
        
    loss_last_epoch = 1000
    final_model = None
    for epoch in range(start_epoch, bert_epochs): # Loop through each epoch.
        print("--> EPOCH ", epoch, '/', bert_epochs-1)

        # Perform one full pass over the training set.
        train_labels, train_predict, train_loss = train(model, train_dataloader, dim_other_feat, criterion, optimizer, scheduler, device)
        train_acc = accuracy_score(train_labels, train_predict)
    
        # Get prediction form model on test data. 
        # print('test on batches...')
        test_labels, test_predict, test_loss = validation(model, test_dataloader, dim_other_feat, criterion, device)
        test_acc = accuracy_score(test_labels, test_predict)
        test_labels_b, test_predict_b, test_loss_b = validation(model, test_balanced_dataloader, dim_other_feat, criterion, device)
        test_acc_b = accuracy_score(test_labels_b, test_predict_b)
        
        if(loss_last_epoch < train_loss):
            print(' > hop we stop, new_train_loss=', train_loss)
            break
        else:
            loss_last_epoch = train_loss
            final_model = copy.deepcopy(model)
        
        
    
        # Print loss and accuracy values to see how training evolves.
        print('(train_loss, test_loss, trai_acc, test_acc) = ', train_loss, test_loss, train_acc, test_acc)
        train_loss_list.append(train_loss)
        test_loss_list.append(test_loss)
        test_b_loss_list.append(test_loss_b)
        
        train_acc_list.append(train_acc)
        test_acc_list.append(test_acc)
        test_b_acc_list.append(test_acc_b)
        # print('Train Loss : ', train_loss_list)
        
        
        
        #delete previous checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # # save current epochs 
        # if(topic is not None):
        #     checkpoint = fold_chkpt + topic + '_' + true_label + '_'+ str(epoch) + '.pth'
        # else:
        #     checkpoint = fold_chkpt + 'controversy_' + true_label + '_'+ str(epoch) + '.pth'
        # torch.save({
        #     'epoch': epoch,
        #     'model_state_dict': model.state_dict(),
        #     'optimizer_state_dict': optimizer.state_dict(),
        #     'train_loss': train_loss_list,
        #     'test_loss': test_loss_list,
        #     'test_b_loss': test_b_loss_list,
        #     'train_acc': train_acc_list,
        #     'test_acc': test_acc_list,
        #     'test_b_acc': test_b_acc_list,
        #     }, checkpoint)
    
    ut.plot_loss_bert(train_loss_list, test_loss_list, test_b_loss_list, root)
    ut.plot_acc_bert(train_acc_list, test_acc_list, test_b_acc_list, root)
    
    ######################## VALIDATION ###########################
    print('#### 1.3.5. START BERT TEST ####')
    # Get prediction form model on validation data. This is where you should use
    true_labels, predictions_labels, avg_epoch_loss = validation(final_model, test_dataloader, dim_other_feat, criterion, device)
    
    if(len(manual_test_dataset) > 0):
        m_true_labels, m_predictions_labels, m_avg_epoch_loss = validation(final_model, manual_test_dataloader, dim_other_feat, criterion, device)
    
    # Show the evaluation report.
    print(">>>> FINAL BERT MODEL RESULT <<<<")
    # Show the evaluation report.
    print(">>>> TRAIN (last epoch) <<<<")
    print(classification_report(train_labels, train_predict))
    
    print(">>>> TEST <<<<")
    # Create the evaluation report.
    evaluation_report = classification_report(true_labels, predictions_labels)
    print(evaluation_report)
    
    print(">>>> MANUAL TEST <<<<")
    # Create the evaluation report.
    if(len(manual_test_dataset) > 0):
        evaluation_report = classification_report(m_true_labels, m_predictions_labels)
        print(evaluation_report)
    else:
        print('> no evaluation!')
    
    print('--- runtime bert : ', round((time.time() - start_time_1)/60, 2), 'min')
    
    
    ##### Save model
    print('#### 1.3.6. SAVE model ####')
    
    torch.save(final_model, path_fine_tuned_model)
    print('- model saved! ', path_fine_tuned_model)
    
    #delete previous checkpoint
    for old_file in os.listdir(fold_chkpt):
         os.remove(fold_chkpt+old_file)
         
    
    train_report = classification_report(train_labels, train_predict, output_dict=True)
    test_report = classification_report(true_labels, predictions_labels,  output_dict=True)
    
    results = {'train': train_report, 'test': test_report}
        
    return final_model, tokenizer, results

    
"""
output 
"""
def send_to_train(x, dict_label_by_tweet, dict_text, combined_dict_f):
    lab = dict_label_by_tweet[x['tweet_id']]
    if(combined_dict_f is None):
        feat = dict_text[x['tweet_id']]
    else:
        feat = (dict_text[x['tweet_id']], combined_dict_f[x['tweet_id']])
    return [feat, feat, lab, lab]

"""
input :
    - train_tweets : list of train post
    - test_tweets : list of test post
    - dict_tweet_label : label for each post
    - dict_text : text for each post/comment
    - dict_graph_to_node : node (comment) belongs to which graph (post)
"""
def get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text, combined_dict_f=None, feature_names=None):
    
    print(' -get train_test_set-')
    print('- dict_text (', len(dict_text.keys()),')')
    if(combined_dict_f is None):
        print(' ** indeed features NOT combined!!!')
    else:
        print(' ** indeed features combined (bert + liwc I suppose)!!!')

    # print(' **sol samy**) **DELETE
    # X = []
    # X_train = []
    # X_test = [] 
    # y = []
    # y_train = []
    # y_test = [] 

    # pb = 0
    # if(combined_dict_f is None):
    #     for t,v in dict_tweet_label.items():      
    #         X.append(dict_text[t])
    #         y.append(v)
    #         if(t in train_tweets):
    #             X_train.append(dict_text[t])
    #             y_train.append(v)
    #         elif(t in test_tweets):
    #             X_test.append(dict_text[t])
    #             y_test.append(v)
    #         else:
    #             pb +=1
    #*********************************
                       
    print(' **sol fati2**')
    print('   *send train')
    df = pd.DataFrame({'tweet_id': train_tweets})
    df['temp_train'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text, combined_dict_f), axis=1)
    df[['X', 'X_train', 'y', 'y_train']] = pd.DataFrame(df['temp_train'].tolist(), index= df.index)
    X = df[df["X"].isnull() == False]['X'].tolist()
    X_train = df[df["X_train"].isnull() == False]['X_train'].tolist()
    y = df[df["y"].isnull() == False]['y'].tolist()
    y_train = df[df["y_train"].isnull() == False]['y_train'].tolist()
    
    X_train_idx = df[df["X_train"].isnull() == False]['tweet_id'].tolist()
    
    print('   *send test')
    df = pd.DataFrame({'tweet_id': test_tweets})
    df['temp_test'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text, combined_dict_f), axis=1)
    df[['X', 'X_test', 'y', 'y_test']] = pd.DataFrame(df['temp_test'].tolist(), index= df.index)
    X += df[df["X"].isnull() == False]['X'].tolist()
    X_test = df[df["X_test"].isnull() == False]['X_test'].tolist()
    y += df[df["y"].isnull() == False]['y'].tolist()
    y_test = df[df["y_test"].isnull() == False]['y_test'].tolist()
    
    X_test_idx = df[df["X_test"].isnull() == False]['tweet_id'].tolist()
    
    pb=0    
    print('---problem in get train_test set (NO MEANING) : ', pb)
    
    if(feature_names is not None):
        print('  -- transform to DF')
        X = pd.DataFrame(X, columns=feature_names)
        X_train = pd.DataFrame(X_train, columns=feature_names, index=X_train_idx)
        X_test = pd.DataFrame(X_test, columns=feature_names, index=X_test_idx)
        y = pd.DataFrame(y, columns=['label'])
        y_train = pd.DataFrame(y_train, columns=['label'], index=X_train_idx)
        y_test = pd.DataFrame(y_test, columns=['label'], index=X_test_idx)
    else:
        print('  -- transform to DF no FN')
        X = pd.DataFrame(X)
        X_train = pd.DataFrame(X_train, index=X_train_idx)
        X_test = pd.DataFrame(X_test, index=X_test_idx)
        y = pd.DataFrame(y, columns=['label'])
        y_train = pd.DataFrame(y_train, columns=['label'], index=X_train_idx)
        y_test = pd.DataFrame(y_test, columns=['label'], index=X_test_idx)
    
    print(len(X), len(X_train), len(X_test), len(y), len(y_train), len(y_test))
    print(X_train.head(3))
    print(' <finish>')

    return X, X_train, X_test, y, y_train, y_test, (X_train_idx, X_test_idx)


"""
get a sampler for having more balanced dataset for the dataloader
input : 
    - labels : list of label of the dataloader
output :
    - sampler : sampler which will sample the data
"""
def get_balanced_sampler(labels):
    lab_unique, counts = np.unique(labels, return_counts=True)
    
    class_w = [sum(counts)/c for c in counts]
    balanced_w = [class_w[e] for e in labels]
    
    sampler = WeightedRandomSampler(balanced_w, len(labels))
    
    return sampler


################## MODEL FUNCTION ###################

"""
function used to fine-tuned our model
input : 
    - model : model to fine-tuned
    - dataloader : train dataloader
    - criterion : criterion to compute loss
    - optimizer_ : optimizer chosed
    - scheduler_ : scheduler 
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during training
"""
def train(model, dataloader, dim_other_feat, criterion, optimizer, scheduler, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.train() 
    for step, batch in enumerate(dataloader):
            
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda(device_[0]) for k,v in batch.items()}     # move batch to device

        model.zero_grad() #init gradient at 0

        if(dim_other_feat == 0):
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
        else:
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask'], batch['other_features']])
        loss = criterion(outputs, batch['labels'])
        
        loss.backward() #compute gradient
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)     #  prevent the "exploding gradients" problem.
        optimizer.step() #Apply gradient descent
        scheduler.step() #Update the learning rate.

        total_loss += loss.item() #sum up all loss       
        
        true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist()  
        predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist() 

    avg_epoch_loss = total_loss / len(dataloader)
  
    return true_labels, predictions_labels, avg_epoch_loss


"""
function used to test our model
input : 
    - model : model to fine-tuned
    - dataloader : test dataloader
    - criterion : criterion to compute loss
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during test
"""
def validation(model, dataloader, dim_other_feat, criterion, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.eval()
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():
            if(dim_other_feat == 0):
                outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
            else:
                outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask'], batch['other_features']])
                
            loss = criterion(outputs, batch['labels'])
            
            total_loss += loss.item()

            true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist() #add original labels
            predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            # embeddings += outputs_embedding.detach().cpu().numpy().tolist()

    avg_loss = total_loss / len(dataloader)

    return true_labels, predictions_labels, avg_loss



"""
predict from model
"""
def predict_model(dict_text, combined_dict_f, topic, features, true_label, device_, task, bert_batches=64, max_length=256):
    
    ########################### get tokenizer and model ###########################
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
      
    if(topic is not None):
        model_path  = ut.CURRENT_APP_FOLD + bert_m_folder + topic+'_'+true_label+'_'+features+'_BERT.pth'
    else:
        model_path  = ut.CURRENT_APP_FOLD + bert_m_folder + task+'_'+features+'_BERT.pth'
    
    model = torch.load(model_path)
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device_[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
    
    
    ########################### DATA ##############################
    if(combined_dict_f is not None):
        X = []
        other_f = []
        for k,v in dict_text.items():
            X.append(v)
            other_f.append(combined_dict_f[k])
    else:        
        X = [v for k,v in dict_text.items()]
        other_f = None

    X = clean_text_v2(X)
    false_y = [-1]*len(X)
    
    dataset =  TwitterDataset(X=X, y=false_y, other_features=other_f,
                              tokenizer=tokenizer, 
                              max_sequence_len=max_length)
    print('dataset contains', len(dataset), 'examples!')
    dataloader = DataLoader(dataset, batch_size=bert_batches, shuffle=False)    
    
    ########################### PREDICTION ##############################
    predicted_labels = []
    out_emb = []
    print(' > size dataloader (nbs batch):', len(dataloader))
    model.eval()
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():       
            if(combined_dict_f is None):
                outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
            else:
                outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask'], batch['other_features']])            
            
            predicted_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            
            out_emb += outputs_embedding.detach().cpu().numpy().tolist()
            
    dict_labels = {}
    dict_embedding = {}
    cpt=0
    for k,v in dict_text.items():
        dict_embedding[k] = out_emb[cpt]
        dict_labels[k] = predicted_labels[cpt]

    return dict_embedding, dict_labels





def predict_model_TEMP(X_manual_test, model, device_, bert_batches=64, max_length=256):
    print(len(X_manual_test))
    print(X_manual_test[0])
    print('*****************************')
    ########################### get tokenizer and model ###########################
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
        
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device_[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device_) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device_) #--for the forward part--
    
    ########################### DATA ##############################
    X = X_manual_test
    other_f = None
    
    X = clean_text_v2(X)
    false_y = [-1]*len(X)
    
    dataset =  TwitterDataset(X=X, y=false_y, other_features=other_f,
                              tokenizer=tokenizer, 
                              max_sequence_len=max_length)
    print('dataset contains', len(dataset), 'examples!')
    dataloader = DataLoader(dataset, batch_size=bert_batches, shuffle=False)    
    
    ########################### PREDICTION ##############################
    predicted_labels = []
    out_emb = []
    a  = []
    print(' > size dataloader (nbs batch):', len(dataloader))
    model.eval()
    for step, batch in enumerate(dataloader):
            
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device
            
        with torch.no_grad():                                     
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])     
            
            predicted_labels.append(outputs.detach().cpu().numpy().argmax(axis=-1).flatten())   
            
            out_emb.append(outputs_embedding.detach().cpu().numpy())       
            
            a.append(outputs.detach().cpu().numpy())
            
    return a, out_emb, predicted_labels




###################################################################################
################################### OBJECT ########################################

"""
class which will transform (tokenized) the input text, and keep its corresponding label 
"""
class TwitterDataset():
    """
    init function
    input : 
        - tokenizer : Transformer type tokenizer used to process raw text into numbers.
        - y : Dictionary to encode any labels names into numbers. Keys map to labels names and Values map to number associated to those labels.
        - max_sequence_len : Value to indicate the maximum desired sequence to truncate or pad text sequences. If no value is passed it will used maximum sequence size
                             supported by the tokenizer and model.
    """
    def __init__(self, X, y, tokenizer, other_features = None, max_sequence_len=None):
        # Check max sequence length.
        max_sequence_len = tokenizer.max_len if max_sequence_len is None else max_sequence_len
        texts = X
        labels = y

        # Number of exmaples.
        self.n_examples = len(labels)
        print('max_sequence_len... ', max_sequence_len)
        
        # Use tokenizer on texts. This can take a while.
        if(len(texts) > 0):
            self.inputs = tokenizer(
                            text=texts,  # the sentence to be encoded
                            add_special_tokens=True,  # Add [CLS] and [SEP]
                            truncation=True,
                            max_length = max_sequence_len,  # maximum length of a sentence
                            pad_to_max_length=True,  # Add [PAD]s
                            return_attention_mask = True,  # Generate the attention mask
                            return_tensors = 'pt',  # ask the function to return PyTorch tensors
                        )    
    
            # Get maximum sequence length.
            self.sequence_len = self.inputs['input_ids'].shape[-1]
            print('Texts padded or truncated to', self.sequence_len, 'length!')
    
            # Add labels.
            self.inputs.update({'labels':torch.tensor(labels)})
            
            # Add combined features.
            if(other_features is not None):
                self.inputs['other_features'] = torch.tensor(other_features)
    
            
            print('Finished!\n')
        else:
            print(' > no sample inside!')
            self.inputs = {}

    def __len__(self):
        return self.n_examples

    def __getitem__(self, item):
        return {key: self.inputs[key][item] for key in self.inputs.keys()}
    
    
"""
CONTROVERSY
class representing our model composed by a bert model + 1 more dense layer of 64 neurons + 1 softmax layer
Fine-tuning already done, here it is just a forward model
"""
class Tweet_polarization_BERTModel(nn.Module):
    """
    init function
    input : 
        - model_name_or_path : name of the pre-trained model
        - type_model : 'sentiment' or 'controversy'
        - pretrained_bert_model : pre-trained model il already loaded (default=None)
    """
    def __init__(self, model_name_or_path, pretrained_bert_model=None, dim_other_feat = 0):
        super(Tweet_polarization_BERTModel, self).__init__()
        
        if(pretrained_bert_model is None):
            self.bert_model = BertModel.from_pretrained(model_name_or_path)
        else:
            self.bert_model = pretrained_bert_model
            
        if(dim_other_feat == 0):
            self.embedding_layer = nn.Linear(768, 64)
        else:
            print(' ** we got combined features!!!')
            self.embedding_layer = nn.Linear(768+dim_other_feat, 64)
            
        self.dropout = nn.Dropout(0.2)

        self.classifier_layer = nn.Linear(64, 2)
        
    """
    forward function
    input :
        - bert_ids : bert id of the sentence (from tokenization)
        - bert_mask : bert mask og the sentence (from tokenization)
    output :
        - out_ : prediction
        - out_embedding : embedding representation   
    """
    def forward(self, x, forward_2 = False):
        if(forward_2 == False):
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            if(len(x) > 2):
                seq = torch.cat((seq,x[2]), 1)

            out_embedding = self.embedding_layer(seq) ## extract the 1st token's embeddings
            out_ = F.relu(out_embedding)
            
            out_ = self.dropout(out_)
           
            #Layer 2
            out_ = self.classifier_layer(out_)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_embedding
        else:
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            if(len(x) > 2):
                seq = torch.cat((seq,x[2]), 1)
            
            out_embedding = self.embedding_layer(seq) ## extract the 1st token's embeddings
            out_ = F.relu(out_embedding)
            
            out_ = self.dropout(out_)
           
            #Layer 2
            out_no_soft = self.classifier_layer(out_)
            out_ = F.softmax(out_no_soft, dim=1)
            
            return out_, out_no_soft, out_embedding
       
################################################################################
################################# other ########################################    
   
"""
function used for creating discriminative learning rate
input :
    - model : the current model
    - the learning rate for bert layers
output :
    - grouped_parameters : the group parameters for each layers
"""
def configure_optimizers(model, lr):
    params = list(model.named_parameters())
    def is_backbone(n): return 'bert' in n
    grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n)], 'lr': lr},
        {"params": [p for n, p in params if not is_backbone(n)], 'lr': lr * 100},
    ]
    return grouped_parameters


"""
test training proportion of the dataloader
input : 
    - train_dataloader : dataloader of training set
"""
def test_train_proportion(train_dataloader):
    lab0 = 0
    lab1 = 0
    for step, batch in enumerate(train_dataloader):
        list_lab =  batch['labels']
        for l in list_lab:
            if(l == 0):
                lab0+=1
            else:
                lab1+=1
                
    tot = lab0 + lab1
    print('--- PROPORTION CLASS DATALOADER ---')
    print('lab0 : ', round((lab0/tot)*100, 2), '% (', lab0 ,') / lab1 : ', round((lab1/tot)*100, 2), '% (', lab1 ,') --> TOT : ', tot)
    
    
"""
split the dataset into train/test set
"""
def split_train_test_dataset(dict_label, dict_manual_labels, labels=[0,1], is_balanced=True, keep_manual_test=True, train_ratio=0.8):    
    
    dict_label_by_tweet = copy.deepcopy(dict_label)
    #if we keep manual test in test set
    if(keep_manual_test == True):
        print('keep manual labelled tweets in test set...')
        old_size = len(dict_label_by_tweet)

        X_test_m = []
        y_test_m = []
        for k,_ in dict_manual_labels.items():
            X_test_m.append(k)
            y_test_m.append(dict_label_by_tweet[k])
            del dict_label_by_tweet[k]
            
        a0 = len([i for i in y_test_m if i==labels[0]])
        a1 = len([i for i in y_test_m if i==labels[1]])
        if(a0 < a1):
            y_to_complete = [labels[0], a1-a0]
        else:
            y_to_complete = [labels[1], a0-a1]
                    
        list_idx = list(dict_label_by_tweet.keys())
        random.shuffle(list_idx)
        for k in list_idx:
            if(y_to_complete[1] < 1):
                break
            v = dict_label_by_tweet[k]
            if(v==y_to_complete[0]):
                X_test_m.append(k)
                y_test_m.append(v)
                del dict_label_by_tweet[k]
                y_to_complete[1] -= 1
                
        print(' >', len(X_test_m), len(y_test_m))
        print(' >', len([i for i in y_test_m if i==labels[0]]), len([i for i in y_test_m if i==labels[1]]))
    
    #start spliting
    if(is_balanced == True):       
        print(' ... balancing ...')
        x0 = []
        x1 = []
        y0 = []
        y1 = []
        
        for k,v in dict_label_by_tweet.items():
            if(v==labels[0]):
                x0.append(k)
                y0.append(v)
            elif(v==labels[1]):
                x1.append(k)
                y1.append(v)
            
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
        
        if(len(x0) > len(x1)):       
            while(len(x0) != len(x1)):
                idx = random.randint(0,len(x0)-1)
                x0.pop(idx)
                y0.pop(idx)
        elif(len(x1) > len(x0)):
            while(len(x1) != len(x0)):
                idx = random.randint(0,len(x1)-1)
                x1.pop(idx)
                y1.pop(idx)
        print('...')
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
    
        X = x0 + x1
        y = y0 + y1
        
    else:
        print(' ... no balanced, we will do it at each epoch ...')
        X = []
        y = []
        for k,v in dict_label_by_tweet.items():
            if(v==labels[0] or v==labels[1]):
                X.append(k)
                y.append(v)
            else:
                print(' /!\ wtf problem, other label still present ! ')
           
            
    if(keep_manual_test == True):
        removed = len(X_test_m)
        old_size = len(X) + removed
        train_ratio = round((train_ratio * old_size)/(old_size-removed), 5)
        print(' > new ratio::', train_ratio, '::')
        
    print(' ... split', train_ratio, '...')
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    stratify=y, 
                                                    train_size=train_ratio)
    if(keep_manual_test == True):
        X_test = X_test + X_test_m
        y_test = y_test + y_test_m
    
    # no_balanced_test = False
    # if(no_balanced_test == True):
    #     for k,v in dict_label.items():
    #         if(v==labels[0] or v==labels[1]):
    #             if(k not in X_train and k not in X_test):
    #                 X_test.append(k)
    #                 y_test.append(v)
    
    print('------- info split before normalizing -------')
    print(' > train set: ', round(len(X_train)/(len(X_train)+len(X_test)), 2), '(ratio) -->  y0: ', 
          round(len([y for y in y_train if y==labels[0]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_train if y==labels[1]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[1]]),')')
    
    print(' > test set: ', round(len(X_test)/(len(X_train)+len(X_test)),2), ' -->  y0: ',
          round(len([y for y in y_test if y==labels[0]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_test if y==labels[1]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[1]]),')')
    print('--------------------------')

    return X_train, X_test



"""
split by users
"""
def split_train_test_dataset_v2(dict_tweet_by_usr, dict_user_label, dict_label, dict_manual_labels, labels=[0,1], is_balanced=True, train_ratio=0.8):    
    
    #1. split user
    print('    A/ split users')
    X_usr = []
    y_usr = []
    for k,v in dict_user_label.items():
        X_usr.append(k)
        y_usr.append(v)
    
    X_usr_train, X_usr_test, y_usr_train, y_usr_test = train_test_split(X_usr, y_usr,
                                                    stratify=y_usr, 
                                                    train_size=train_ratio)
    
    print(' > train USER: ', round(len(X_usr_train)/(len(X_usr_train)+len(X_usr_test)), 2), '(ratio) -->  y0: ', 
      round(len([y for y in y_usr_train if y==labels[0]])/len(y_usr_train), 2), '(', len([y for y in y_usr_train if y==labels[0]]),')',
      ' /  y1: ', round(len([y for y in y_usr_train if y==labels[1]])/len(y_usr_train), 2), '(', len([y for y in y_usr_train if y==labels[1]]),')')

    print(' > test USER: ', round(len(X_usr_test)/(len(X_usr_train)+len(X_usr_test)), 2), '(ratio) -->  y0: ', 
      round(len([y for y in y_usr_test if y==labels[0]])/len(y_usr_test), 2), '(', len([y for y in y_usr_test if y==labels[0]]),')',
      ' /  y1: ', round(len([y for y in y_usr_test if y==labels[1]])/len(y_usr_test), 2), '(', len([y for y in y_usr_test if y==labels[1]]),')')
   
    #--------------------------------------------
    
    print('   B/ split tweets from users')
    X_train_t = []
    y_train_t = []
    dict_train = {}
    for u in X_usr_train:
        tweets = dict_tweet_by_usr[u]
        l = dict_user_label[u]
        for t in tweets:
            X_train_t.append(t)
            y_train_t.append(l)
            dict_train[t] = l
            
    X_test_t = []
    y_test_t = []
    dict_test = {}
    for u in X_usr_test:
        tweets = dict_tweet_by_usr[u]
        l = dict_user_label[u]
        for t in tweets:
            X_test_t.append(t)
            y_test_t.append(l) 
            dict_test[t] = l
            
    print(' > all train tweets: ', round(len(X_train_t)/(len(X_train_t)+len(X_test_t)), 2), '(ratio) -->  y0: ', 
      round(len([y for y in y_train_t if y==labels[0]])/len(y_train_t), 2), '(', len([y for y in y_train_t if y==labels[0]]),')',
      ' /  y1: ', round(len([y for y in y_train_t if y==labels[1]])/len(y_train_t), 2), '(', len([y for y in y_train_t if y==labels[1]]),')')

    print(' > all test tweets: ', round(len(X_test_t)/(len(X_train_t)+len(X_test_t)), 2), '(ratio) -->  y0: ', 
      round(len([y for y in y_test_t if y==labels[0]])/len(y_test_t), 2), '(', len([y for y in y_test_t if y==labels[0]]),')',
      ' /  y1: ', round(len([y for y in y_test_t if y==labels[1]])/len(y_test_t), 2), '(', len([y for y in y_test_t if y==labels[1]]),')')
   

    #--------------------------------------------
    
    print('   C/ balance each set')   
    the_set = {'train': dict_train, 'test': dict_test}
    #start spliting
    for curr_set, curr_dict in the_set.items():
    
        print(' ... balancing', curr_set, '...')
        x0 = []
        x1 = []
        y0 = []
        y1 = []
        
        for k,v in curr_dict.items():
            if(v==labels[0]):
                x0.append(k)
                y0.append(v)
            elif(v==labels[1]):
                x1.append(k)
                y1.append(v)
            
        print('       > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
        
        if(len(x0) > len(x1)):       
            while(len(x0) != len(x1)):
                idx = random.randint(0,len(x0)-1)
                x0.pop(idx)
                y0.pop(idx)
        elif(len(x1) > len(x0)):
            while(len(x1) != len(x0)):
                idx = random.randint(0,len(x1)-1)
                x1.pop(idx)
                y1.pop(idx)
        print('               ...')
        print('       > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
    
        if(curr_set == 'train'):
            X_train = x0 + x1
            y_train = y0 + y1
        else:
            X_test = x0 + x1
            y_test = y0 + y1 

    
    print('------- info split before normalizing -------')
    print(' > train set: ', round(len(X_train)/(len(X_train)+len(X_test)), 2), '(ratio) -->  y0: ', 
          round(len([y for y in y_train if y==labels[0]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_train if y==labels[1]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[1]]),')')
    
    print(' > test set: ', round(len(X_test)/(len(X_train)+len(X_test)),2), ' -->  y0: ',
          round(len([y for y in y_test if y==labels[0]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_test if y==labels[1]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[1]]),')')
    print('--------------------------')

    return X_train, X_test




















"""
topic == 'pelosi'
"""
def get_manual_test_set(topic, df_tweets, dict_manual_label_by_usr, dict_features, combined_dict_f = None, limit=50):
    print(' > no balanced manual test set (v2):', topic)
    
    print('     >> len(df_tweets) BEFORE:', len(df_tweets))
    df_tweets = df_tweets[df_tweets['topic'] == topic]
    print('     >> len(df_tweets) AFTER:', len(df_tweets))
    
    if(combined_dict_f is None):
        print(' ** features NOT combined!!!')
    else:
        print(' ** features combined!!!')
    
    tweets_c1 = []
    tweets_c2 = []
    
    dict_usr_labels = {}
    dict_tweet_labels = {}
    cpt0=0
    cpt1=0
    for k,v in dict_manual_label_by_usr.items():
        if(v['label']==0):
            dict_usr_labels[k] = v['label']
            cpt0+=1
        if(v['label']==1):
            dict_usr_labels[k] = v['label']
            cpt1+=1
            
    for idx, row in df_tweets.iterrows():
        if(row['user_id'] in dict_usr_labels.keys()):
            if(dict_usr_labels[row['user_id']] == 0):
                tweets_c1.append(row['tweet_id'])
            else:
                tweets_c2.append(row['tweet_id'])
            dict_tweet_labels[row['tweet_id']] = dict_usr_labels[row['user_id']]
                    
    X_manual_test = []
    y_manual_test = []
    for k,v in dict_tweet_labels.items():
        if(combined_dict_f is None): #no combinaison of features
            X_manual_test.append(dict_features[k])
        else:
            X_manual_test.append((dict_features[k], combined_dict_f[k]))
        y_manual_test.append(v)
    
    prop0 = len([k for k,v in dict_tweet_labels.items() if(v==0)])
    prop1 = len([k for k,v in dict_tweet_labels.items() if(v==1)])
    print('  > nbs user: 0 (',cpt0, ')  --  1 (',cpt1,')')
    if(len(dict_tweet_labels) != 0):
        print('  > proportion manual test tweets set:')
        print('       > 0:', prop0, round(prop0/len(dict_tweet_labels), 3))
        print('       > 1:', prop1, round(prop1/len(dict_tweet_labels), 3))
    
    return X_manual_test, y_manual_test, dict_tweet_labels


def get_manual_test_set_active_user(df_tweets, dict_manual_label_by_usr, dict_features, label_name, combined_dict_f = None, limit=50):
    print(' > no balanced manual test set')
    
    df_tweets = df_tweets[df_tweets['topic'] == 'pelosi']
    
    if(combined_dict_f is None):
        print(' ** features NOT combined!!!')
    else:
        print(' ** features combined!!!')
    
    tweets_c1 = []
    tweets_c2 = []
    
    dict_usr_labels = {k:v for k,v in dict_manual_label_by_usr.items() if(v['label']==0 or v['label']==1)}
    dict_tweet_labels = {}

    for idx, row in df_tweets.iterrows():
        if(row['user_id'] in dict_usr_labels.keys()):
            lab_temp = row[label_name]
            if(lab_temp == 0):
                tweets_c1.append(row['tweet_id'])
            else:
                tweets_c2.append(row['tweet_id'])
            dict_tweet_labels[row['tweet_id']] = lab_temp

    X_manual_test = []
    y_manual_test = []
    for k,v in dict_tweet_labels.items():
        if(combined_dict_f is None): #no combinaison of features
            X_manual_test.append(dict_features[k])
        else:
            X_manual_test.append((dict_features[k], combined_dict_f[k]))
        y_manual_test.append(v)
    
    prop0 = len([k for k,v in dict_tweet_labels.items() if(v==0)])
    prop1 = len([k for k,v in dict_tweet_labels.items() if(v==1)])
    print('  > proportion manual test tweets set:')
    print('       > 0:', prop0, round(prop0/len(dict_tweet_labels), 3))
    print('       > 1:', prop1, round(prop1/len(dict_tweet_labels), 3))
    
    return X_manual_test, y_manual_test, dict_tweet_labels



"""
get_unique tweets by user
"""
def get_one_sample_by_user(df, temp_path, label, features_and_label, sample_by = 'UNIQUE'):
    list_user = list(set(df['user_id'].tolist()))
    print('  **', len(list_user))
    
    print('    --------- sample by', sample_by, '------------')
    
    if(sample_by == 'UNIQUE'):
        print('      -- a. run loop keeping one tweet')
        tweets_kept = []
        cpt=0
        for u in list_user:
            t1 = time.time()
            tweets = df[df['user_id'] == u]['tweet_id'].tolist()
            s = random.sample(tweets, 1)
            tweets_kept.append(s[0])
      
            cpt +=1
            if(cpt%int(len(list_user)/200) == 0 or cpt==1):
                print('   >>> done:', round((cpt/len(list_user))*100, 2) , '%  ->  time left:', round(((len(list_user)-cpt) * (time.time() - t1))/60, 2), 'min <<<')
            
        print('  *', len(tweets_kept))
        
        print('      -- b. get new dataframe')
        new_df = df[df['tweet_id'].isin(tweets_kept)]
        pickle_save5(temp_path, new_df)
        
        print('  **', len(new_df))
        print('  ***', len(new_df['tweet_id'].tolist()), len(list(set(new_df['tweet_id'].tolist()))))
        
        new_df = new_df.drop_duplicates(subset=['tweet_id'], keep=False)
        pickle_save5(temp_path, new_df)
        
        print('  ****', len(new_df))
        print('  *****', len(list(set(new_df['user_id'].tolist()))))
        # new_df = df.groupby('user_id').apply(lambda x: x.sample(1)).reset_index(drop=True)
        
    else:
        new_df = df.groupby(['topic', 'user_id'])[features_and_label].mean().reset_index()
        print('  ****', len(new_df))
        new_df = new_df.drop_duplicates(subset=['user_id'], keep=False)
        print('  *****', len(new_df))
        print('  *****', len(list(set(new_df['user_id'].tolist()))))
        pickle_save5(temp_path, new_df)

    print(' > type label:', new_df[label].dtypes)

    return new_df
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1
def pickle_save5(path, data):
    with open(path, 'wb') as file:
        pickle5.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
clean text
"""
def clean_text_v2(inputs, is_list=True):
    if(is_list == True):
        new_inputs = []
        print(' > cleaning text!')
        for tweet in inputs:
            #remove retweet
            tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
            
            #remove URL
            tweet = re.sub(r'http\S+', r'URL', tweet)
            
            # remove @
            # tweet = re.sub(r'@[^\s]+',r'', tweet)
            
            #remove non-ascii words and characters
            # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
            # tweet = ''.join(tweet)
            # tweet = re.sub(r'_[\S]?',r'', tweet)
            # tweet = re.sub(r'\n',r' ', tweet)
            
            #remove #
            # tweet = re.sub(r'#([a-zA-Z0-9_]{1,50})',r'', tweet)
            #tweet = re.sub(r'#', '', tweet)
            
            # #remove &, < and >
            # tweet = tweet.replace(r'&amp;?',r'and')
            # tweet = tweet.replace(r'&lt;',r'<')
            # tweet = tweet.replace(r'&gt;',r'>')
            
            # # remove extra space
            # tweet = re.sub(r'\s\s+', r' ', tweet)
            
            # # insert space between punctuation marks
            # tweet = re.sub(r'([\w\d]+)([^\w\d ]+)', r'\1 \2', tweet)
            # tweet = re.sub(r'([^\w\d ]+)([\w\d]+)', r'\1 \2', tweet)
            
            # lower case and strip white spaces at both ends
            # tweet = tweet.lower()
            # tweet = tweet.strip()
            
            tweet = html.unescape(tweet)     
            tweet = re.sub(r'\n', r'', tweet)
            new_inputs.append(tweet)
            
        return new_inputs
    
    else:
        #remove retweet
        tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', inputs)
        
        #remove URL
        tweet = re.sub(r'http\S+', r'URL', tweet)
        
        tweet = html.unescape(tweet)
        tweet = re.sub(r'\n', r'', tweet)
        
        return tweet
            
    return 1     


def get_accuracy_from_dict(dict_labels, dict_manual_labels):
    y0 = []
    y1 = []
    for k,v in dict_manual_labels.items():
        y0.append(dict_labels[k])
        y1.append(v)    
    
    return metrics.accuracy_score(y0, y1)


def to_df(X_manual_test, feature_names):
     df = pd.DataFrame(data=X_manual_test, columns=feature_names)
     
     return df

#---------------------------------------------------------------------------------

def is_train(x, dict_label_by_tweet):
    if(dict_label_by_tweet[x['all_train_tweets']] == 0):
        return [x['all_train_tweets'], None, 0, None]
    elif(dict_label_by_tweet[x['all_train_tweets']] == 1):
        return [None, x['all_train_tweets'], None, 1]
    
    print('WEIRD', dict_label_by_tweet[x['all_train_tweets']])
    
    return None


"""
split the dataset into train/test set
"""
def balance_train_set_controversy(dict_label_by_tweet, all_train_tweets, test_tweets, labels=[0,1], is_balanced=True):
    # print('**sol samy**') **DELETE**
    # x0 = []
    # y0 = []
    # x1 = []
    # y1 = []
    # X_test = []
    # y_test = []

    
    # cpt=0
    # idx=0
    
    # print(' **create df')
    # df = pd.DataFrame(list(dict_label_by_tweet.items()),columns = ['col1','col2'])
    # size = len(df)
    # records = df.to_dict('records')
    # print(' **df created', size)
    
    
    
    # for row in records:
    #     t1 = time.time()
    #     k = row['col1']
    #     v = int(row['col2'])
    #     if(k in all_train_tweets):
    #         if(v==labels[0]):
    #             x0.append(k)
    #             y0.append(v)
    #         elif(v==labels[1]):
    #             x1.append(k)
    #             y1.append(v)
    #     elif(k in test_tweets):
    #         X_test.append(k)
    #         y_test.append(v)
    #     else:
    #         cpt+=1
            
    #     if(idx%int(size/1000) == 0 or idx==1):
    #         print('   >>> done:', round((idx/size)*100, 2) , '%  ->  time left:', round(((size-idx) * (time.time() - t1))/60, 2), 'min <<<')
    #     idx+=1
    # ***********************************
    
    # solution fati
    print(' **sol fati**')
    df = pd.DataFrame({'all_train_tweets': all_train_tweets}) #
    df['temp'] = df.apply(lambda x: is_train(x, dict_label_by_tweet), axis=1)
    df[['x0', 'x1', 'y0', 'y1']] = pd.DataFrame(df['temp'].tolist(), index= df.index)

    x0 = df[df["x0"].isnull() == False]['x0'].tolist()
    x1 = df[df["x1"].isnull() == False]['x1'].tolist()
    y0 = df[df["y0"].isnull() == False]['y0'].tolist()
    y1 = df[df["y1"].isnull() == False]['y1'].tolist()
    
    X_test = test_tweets
    y_test = [dict_label_by_tweet[k] for k in test_tweets]
    cpt=0
    
    print(len(x0), len(x1), len(y0), len(y1))
        
        
    print('  >> NBS PROBLEM :', cpt)
    print('  >> test comparaison TEST set :', len(test_tweets), len(X_test), '|| ', len(dict_label_by_tweet), '==', len(all_train_tweets) + len(test_tweets))
    #start spliting
    if(is_balanced == True):       
        print(' ... balancing ...')            
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
        
        if(len(x0) > len(x1)):       
            while(len(x0) != len(x1)):
                idx = random.randint(0,len(x0)-1)
                x0.pop(idx)
                y0.pop(idx)
        elif(len(x1) > len(x0)):
            while(len(x1) != len(x0)):
                idx = random.randint(0,len(x1)-1)
                x1.pop(idx)
                y1.pop(idx)
        print('...')
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
    
        X_train = x0 + x1
        y_train = y0 + y1
        
    else:
        print(' ... no balanced, we will do it at each epoch ...')
        X_train = []
        y_train = []
        
        for k,v in dict_label_by_tweet.items():
            if(v==labels[0] or v==labels[1]):
                X_train.append(k)
                y_train.append(v)
            else:
                print(' /!\ wtf problem, other label still present ! ')
    
    print('------- info split before normalizing -------')
    print(' > train set: ', round(len(X_train)/(len(X_train)+len(X_test)), 2), '(ratio) -->  y0: ', 
          round(len([y for y in y_train if y==labels[0]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_train if y==labels[1]])/len(y_train), 2), '(', len([y for y in y_train if y==labels[1]]),')')
    
    print(' > test set: ', round(len(X_test)/(len(X_train)+len(X_test)),2), ' -->  y0: ',
          round(len([y for y in y_test if y==labels[0]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[0]]),')',
          ' /  y1: ', round(len([y for y in y_test if y==labels[1]])/len(y_test), 2), '(', len([y for y in y_test if y==labels[1]]),')')
    print('--------------------------')
    
    return X_train


"""
sample data for shap value
"""
def shap_sampling(X_train, y_train, labels = [0,1], nbs_sample= 9000):
    print(' > sample size:', nbs_sample)
    if(len(X_train) < nbs_sample):
        print(' > no need to sample (<)')
        c0_idx = y_train[y_train['label'] == labels[2][0]].index[0]
        c1_idx = y_train[y_train['label'] == labels[2][1]].index[0]
        
        return X_train, y_train, (c0_idx, c0_idx), (c1_idx, c1_idx)
    
    print(' > start sample shape:', X_train.shape)
        
    X_train['y'] = y_train['label'].tolist()
    X_train['idx'] = X_train.index
    
    df_x0 = X_train[X_train['y'] == labels[0]]
    df_x1 = X_train[X_train['y'] == labels[1]]
    
    print(' > temp sample shape:', X_train.shape, '(', df_x0.shape, df_x1.shape, ')')
    
    df_x0 = df_x0.sample(n=int(nbs_sample/2), random_state=1)
    df_x1 = df_x1.sample(n=int(nbs_sample/2), random_state=1)
    
    c0_sample_idx = 0
    c0_idx = df_x0['idx'].tolist()[c0_sample_idx]
    c1_sample_idx = 0
    c1_idx = df_x1['idx'].tolist()[c1_sample_idx]
    
    df_X_sample = pd.concat([df_x0, df_x1])
    
    df_y_sample = pd.DataFrame({'label': df_X_sample['y'].tolist()})
    
    df_X_sample = df_X_sample.drop('y', axis=1)
    df_X_sample = df_X_sample.drop('idx', axis=1)
    
    print(' > final sample shape:', df_X_sample.shape, '(', df_x0.shape, df_x1.shape, ')')
    
    return df_X_sample, df_y_sample, (c0_idx, c0_sample_idx), (c1_idx, c1_sample_idx)



"""
We have 4 topics in test
take same amount for each topic
"""
def shap_sampling_controversy(X, y, df_tweets, c0_id_tweet, c1_id_tweet, labels = [0,1], nbs_sample= 35000, test_topics=None, one_topic = None):
    print(' > sample size:', nbs_sample)
    df_t = df_tweets[['topic', 'tweet_id', 'controversy_LABEL']]
    df_t = df_t.rename(columns = {'topic':'TOPIC_NAME'})
    df_t.groupby(df_t.index).first()
    df_t = df_t.set_index(['tweet_id'])
    df_t = df_t.groupby(df_t.index).first()

    X = X.groupby(X.index).first()
    
    df_to_sample = pd.concat([X, df_t], axis=1, join="inner")
    if(test_topics is not None):
        df_to_sample = df_to_sample.drop(df_to_sample[df_to_sample['TOPIC_NAME'].isin(test_topics)].index)

    

    if(one_topic is None):
        print(' >> multiple topic sampled!')
        topics_present = list(set(df_to_sample['TOPIC_NAME'].tolist()))
        print(topics_present)
        print(df_to_sample.groupby('TOPIC_NAME').size())
        
        nbs_sample_by_topic = int(nbs_sample/len(topics_present))
        print(' > nbs_sample_by_topic:', nbs_sample_by_topic, '(', len(topics_present), 'topics)')
        
        X_sample = None
        for topic in topics_present:
            temp = df_to_sample[df_to_sample['TOPIC_NAME'] == topic]
            temp = temp.sample(n=min(nbs_sample_by_topic, len(temp)), random_state=1)
            
            if(len(temp) != nbs_sample_by_topic):
                print(' > /!\ topic', topic , 'is small, no !', len(temp))
            
            if(X_sample is None):
                X_sample = temp
            else:
                X_sample = pd.concat([X_sample, temp])
                
        if(c0_id_tweet is not None and c0_id_tweet not in X_sample):
            print(' > c0 added!')
            X_sample.loc[c0_id_tweet] = df_to_sample.loc[c0_id_tweet]
        if(c1_id_tweet is not None and c1_id_tweet not in X_sample):
            print(' > c1 added!')
            X_sample.loc[c1_id_tweet] = df_to_sample.loc[c1_id_tweet]
    else:
        print(' >> one topic sampled!', one_topic)
        nbs_sample_by_topic = nbs_sample
        temp = df_to_sample[df_to_sample['TOPIC_NAME'] == one_topic]
        temp = temp.sample(n=min(nbs_sample_by_topic, len(temp)), random_state=1)
        
        if(len(temp) != nbs_sample_by_topic):
            print(' > /!\ topic', one_topic , 'is too small!', len(temp))
        X_sample = temp

    print(' > final split:', len(X_sample[X_sample['controversy_LABEL'] == labels[0]]), 'vs', len(X_sample[X_sample['controversy_LABEL'] == labels[1]]))
    
    y_sample = X_sample[['TOPIC_NAME', 'controversy_LABEL']]
    if(c0_id_tweet is not None):
        print(y_sample.loc[c0_id_tweet])
        print(df_tweets[df_tweets['tweet_id'] == c0_id_tweet]['clean_text'].tolist()[0])
    if(c1_id_tweet is not None):
        print(y_sample.loc[c1_id_tweet])
        print(df_tweets[df_tweets['tweet_id'] == c1_id_tweet]['clean_text'].tolist()[0])
    
    y_sample = y_sample.drop('TOPIC_NAME', axis=1)
    
    X_sample = X_sample.drop('TOPIC_NAME', axis=1)
    X_sample = X_sample.drop('controversy_LABEL', axis=1)

    return X_sample, y_sample




def shap_sampling_pol(X, y, df_tweets, c0_id_tweet, c1_id_tweet, labels = [0,1], nbs_sample= 35000, test_topics=None, one_topic = None):
    print(' > sample size:', nbs_sample)
    df_t = df_tweets[['topic', 'tweet_id', 'metis_label']]
    df_t = df_t.rename(columns = {'topic':'TOPIC_NAME'})
    df_t.groupby(df_t.index).first()
    df_t = df_t.set_index(['tweet_id'])
    df_t = df_t.groupby(df_t.index).first()

    X = X.groupby(X.index).first()
    
    df_to_sample = pd.concat([X, df_t], axis=1, join="inner")
    if(test_topics is not None):
        df_to_sample = df_to_sample.drop(df_to_sample[df_to_sample['TOPIC_NAME'].isin(test_topics)].index)

    

    if(one_topic is None):
        print(' >> multiple topic sampled!')
        topics_present = list(set(df_to_sample['TOPIC_NAME'].tolist()))
        print(topics_present)
        print(df_to_sample.groupby('TOPIC_NAME').size())
        
        nbs_sample_by_topic = int(nbs_sample/len(topics_present))
        print(' > nbs_sample_by_topic:', nbs_sample_by_topic, '(', len(topics_present), 'topics)')
        
        X_sample = None
        for topic in topics_present:
            temp = df_to_sample[df_to_sample['TOPIC_NAME'] == topic]
            temp = temp.sample(n=min(nbs_sample_by_topic, len(temp)), random_state=1)
            
            if(len(temp) != nbs_sample_by_topic):
                print(' > /!\ topic', topic , 'is not too small!', len(temp))
            
            if(X_sample is None):
                X_sample = temp
            else:
                X_sample = pd.concat([X_sample, temp])
                
        if(c0_id_tweet is not None and c0_id_tweet not in X_sample):
            print(' > c0 added!')
            X_sample.loc[c0_id_tweet] = df_to_sample.loc[c0_id_tweet]
        if(c1_id_tweet is not None and c1_id_tweet not in X_sample):
            print(' > c1 added!')
            X_sample.loc[c1_id_tweet] = df_to_sample.loc[c1_id_tweet]
    else:
        print(' >> one topic sampled!', one_topic)
        nbs_sample_by_topic = nbs_sample
        temp = df_to_sample[df_to_sample['TOPIC_NAME'] == one_topic]
        temp = temp.sample(n=min(nbs_sample_by_topic, len(temp)), random_state=1)
        
        if(len(temp) != nbs_sample_by_topic):
            print(' > /!\ topic', one_topic , 'is not too small!', len(temp))
        X_sample = temp

    print(' > final split:', len(X_sample[X_sample['metis_label'] == labels[0]]), 'vs', len(X_sample[X_sample['metis_label'] == labels[1]]))
    
    y_sample = X_sample[['TOPIC_NAME', 'metis_label']]
    if(c0_id_tweet is not None):
        print(y_sample.loc[c0_id_tweet])
        print(df_tweets[df_tweets['tweet_id'] == c0_id_tweet]['clean_text'].tolist()[0])
    if(c1_id_tweet is not None):
        print(y_sample.loc[c1_id_tweet])
        print(df_tweets[df_tweets['tweet_id'] == c1_id_tweet]['clean_text'].tolist()[0])
    
    y_sample = y_sample.drop('TOPIC_NAME', axis=1)
    
    X_sample = X_sample.drop('TOPIC_NAME', axis=1)
    X_sample = X_sample.drop('metis_label', axis=1)

    return X_sample, y_sample
