#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  1 15:08:17 2022

@author: samy
"""

import os
import csv
import json
import pickle
import pickle5
import pandas as pd
import seaborn as sns 
import matplotlib.pyplot as plt



from sklearn.metrics import classification_report, accuracy_score, roc_auc_score
from sklearn.model_selection import train_test_split
from transformers import BertTokenizer, BertModel
from transformers import (AdamW, 
                          get_linear_schedule_with_warmup,
                          set_seed)

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from umap import UMAP

import shap
from shap.plots import text

import util as ut


"""
read json
"""
def read_json(path):    
    results = {}
    if(os.path.isfile(path) == True):
        with open(path, 'r') as f:
            results = json.load(f)
            
    return results

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1


def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1
def pickle_save5(path, data):
    with open(path, 'wb') as file:
        pickle5.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

def save_giphi_files(path_file, dict_node_label, dict_edges):
    #1. write node information
    node_filename = path_file + 'nodes_connected_v2.csv'
    
    if(os.path.isfile(node_filename) == True):
        print("-- *Giphi files* already created --")
    else:
        with open(node_filename, mode='w') as nf:
            writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_node.writerow(['Id','Label','modularity_class'])
        
            if(dict_node_label is not None):
                for user in dict_node_label.keys():
                    writer_node.writerow([str(user), '', str(dict_node_label[user])])
                    
        
        edge_filename = path_file + 'edges_connected_v2.csv'
        with open(edge_filename, mode='w') as ef:
            writer_edge = csv.writer(ef, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_edge.writerow(['Source','Target','Type', 'Id', 'Label']) 
            
            for ts in dict_edges.keys():
                edge = dict_edges[ts]
                writer_edge.writerow([str(edge[0]),str(edge[1]),'Undirected','',''])
            
    return 1

def get_roc_auc_score(dict_accuracy, dict_labels):
    preds = []
    labs = []
    for k,v in dict_accuracy.items():
        preds.append(v)
        labs.append(dict_labels[k])
    
    score = roc_auc_score(labs, preds)
    
    return score

# aaa = df_tweets[df_tweets['topic'] == 'pelosi'][['tweet_id', 'clean_text', 'topic', 'controversy_LABEL']]




#-------------------------------------------


# p1 = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/controversy_LABEL/bert_text (on TEST) (no complete shap)/all_test_topics_together/shap_temp_TEST_v1.pickle'

# pp = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/metis_label/bert_text/predict_model_TEMP.pickle'


# shap_values, sample_X, sample_y, c0_id_tweet, c0_sample_idx, tweet_c0, c1_id_tweet, c1_sample_idx, tweet_c1, outputs, val, sample_X_list = load_pickle(p1)
# a, out_emb, predicted_labels = load_pickle(pp)

# # aa_val = None
# for v in val:
#     if(aa_val is None):
#         aa_val = v
#     else:
#         aa_val = aa_val.concat(v)
        
# aa_outputs = None
# for v in outputs:
#     if(aa_outputs is None):
#         aa_outputs = v
#     else:
#         aa_outputs = aa_outputs.concat(v)

# p_model, tokenizer = load_pickle(p_m)


#----------------------------------------------------

# df = pd.DataFrame({'a': [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3], 'b': [1,2,3,4,5,6,7,8,9,10,11,12]})


# df2 = df.groupby('a')
# df2 = df2.apply(lambda x: x.sample(2))
# df2.index = df2.index.droplevel('a')

# df2 = df2.append(df[(df['a']==1) & (df['b'] == 3)])






        # print('          >> size sampling:', len(sample_X))
        # sample_X[0] = sample_X.apply(lambda x: bmt.clean_text_v2(x[0], False), axis=1)
        # sample_X_list = sample_X[0].tolist()
        
        # c0_idx = sample_y.index.get_loc(c0_id_tweet)
        # c0_sample_idx = c0_idx
        # tweet_c0 = df_tweets[df_tweets['tweet_id'] == c0_id_tweet]['clean_text'].tolist()[0]
        
        # c1_idx = sample_y.index.get_loc(c1_id_tweet)
        # c1_sample_idx = c1_idx
        # tweet_c1 = df_tweets[df_tweets['tweet_id'] == c1_id_tweet]['clean_text'].tolist()[0]
                
        # print(' >> 6.1.2 get explainer')
        # outputs, val = predictor_list(sample_X_list)

        # # build an explainer using a token masker
        # print(' >1. explainer')
        # time1 = time.time()
        # explainer = shap.Explainer(f_batch, tokenizer)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min')           
        # print(' >2. shap values')
        # shap_values = explainer(sample_X_list, fixed_context=1)
        # print('   >.', round(((time.time() - time1)/60), 2), 'min') 
                    
        # ###############
        # data = (shap_values, sample_X, sample_y, c0_id_tweet, c0_sample_idx, tweet_c0, c1_id_tweet, c1_sample_idx, tweet_c1, outputs, val, sample_X_list)
        # pickle_save(root+'/shap_temp_TEST_v1.pickle', data)
        # ###############
        
        # print('   ---- print plot text shap (example', c0_sample_idx,') ----')
        # print('ex0:', sample_X.iloc[c0_sample_idx][0])
        # f = plt.figure()
        # shap.plots.text(shap_values[c0_sample_idx])
        # f.savefig(root+"/SHAP_text_0_plot.png", bbox_inches='tight', dpi=600)
        
        # print('   ---- print plot text shap (example', c1_sample_idx,') ----')
        # print('ex1:', sample_X.iloc[c1_sample_idx][0])
        # f = plt.figure()
        # shap.plots.text(shap_values[c1_sample_idx])
        # f.savefig(root+"/SHAP_text_1_plot.png", bbox_inches='tight', dpi=600)
        
        
        # print('   ---- print plot summary shap (TEST) sum ----') #most common & most impact
        # f = plt.figure()
        # shap.plots.bar(shap_values.abs.sum(0))
        # f.savefig(root+"/SHAP_summary_test_SUM.png", bbox_inches='tight', dpi=600)
        
        # print('   ---- print plot summary shap (TEST) max ----') #most impact
        # f = plt.figure()
        # shap.plots.bar(shap_values.abs.max(0))
        # f.savefig(root+"/SHAP_summary_test_MAX.png", bbox_inches='tight', dpi=600)
        
        # print('   ---- print plot summary shap (TEST) qum on BOTH SENSE ----') #most common & most impact in which direction
        # f = plt.figure()
        # shap.plots.bar(shap_values.sum(0))
        # f.savefig(root+"/SHAP_summary_test_SUM_BOTH_SENSE.png", bbox_inches='tight', dpi=600)
        
        #----------------
        
        
##################################################################################

# """
# Reduce node embedding dimension for vizualisation
# """
# def reduce_dimension(method, data, dim=2):
#     if(method == 'tsne'):
#         X_embedded = TSNE(n_components=dim, init='random').fit_transform(data)
#     if(method == 'umap'):
#         umap_2d = UMAP(n_components=2, init='random', random_state=0)
#         X_embedded = umap_2d.fit_transform(data)
#     if(method == 'pca'):
#         pca = PCA(n_components=2)
#         X_embedded = pca.fit_transform(data)
        
#     return X_embedded

# print('- load')
# p = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/metis_label/bert_text/embedding_manual_test.pickle'
# X_manual_test, y_manual_test, dict_tweet_label, dict_embeddings = load_pickle(p)

# p_df = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/metis_label/pelosi.pickle'
# _, df_tweets, _, dict_text, _, _, _, _ = load_pickle(p_df)


# #3.1 plot w/ label c1 v c2
# print('- reduce')
# tweet_labels = [dict_tweet_label[k] for k in dict_embeddings.keys()]


# # reduce using T-SNE or UMAP
# embeddings = [v for _,v in dict_embeddings.items()]
# reduced = reduce_dimension('pca', embeddings)

# print('- prepare')
# x = []
# y = []
# for a in reduced:
#     x.append(a[0])
#     y.append(a[1])
# data = pd.DataFrame({'d1': x, 'd2': y, 'label':tweet_labels})
# sns.scatterplot(data=data, x="d1", y="d2", hue="label")


#3.2 plot w/ user labels, 2 for c1, 2 for c2 (w/ multiple tweets)
# dict_usr_name = {'954190967422242817': 'A0', '938994922547421189': 'B0', '429554368': 'C1', '27493883': 'D1'}

# # get label
# id_ = dict_embeddings.keys()
# df_test = df_tweets[df_tweets['tweet_id'].isin(id_)]

# lab = []
# new_emb = []
# for k,v in dict_embeddings.items():
#     uid = df_test[df_test['tweet_id'] == k]['user_id'].tolist()[0]
    
#     if(uid in dict_usr_name.keys()):
#         lab.append(dict_usr_name[uid])
#         new_emb.append(v)
        
# # reduce using T-SNE or UMAP
# reduced = reduce_dimension('pca', new_emb)

# print('- prepare')
# x = []
# y = []
# for a in reduced:
#     x.append(a[0])
#     y.append(a[1])
    
# data = pd.DataFrame({'d1': x, 'd2': y, 'label':lab})
# sns.scatterplot(data=data, x="d1", y="d2", hue="label")



############# get stat on each dataset

# from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'
# df = load_data_csv(from_path)


# topic = 'Thanksgiving'
# object_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+topic+'/RETWEET/data.pickle'
# dict_tweets, dict_nodes, dict_edges, _, _, _, _, _, dict_node_label_metis, _, _, _ = load_pickle(object_path)

# df_temp = df[df['topic']==topic]
# df_temp = df_temp[df_temp['metis_label'] != -10]
# df0 = df_temp[df_temp['metis_label'] == 0]
# df1 = df_temp[df_temp['metis_label'] == 1]

# #tweets by community
# print(' > tweets by community')
# print('     c0:', len(df0), '| c1:', len(df1), '  --> ', len(df_temp), len(df0)+len(df1))

# #users by comunity
# print(' > users by community')
# all_ = dict_node_label_metis
# u0 = [k for k,v in dict_node_label_metis.items() if(v==0)]
# u1 = [k for k,v in dict_node_label_metis.items() if(v==1)]

# print('     c0:', len(u0), '| c1:', len(u1), '  --> ', len(all_), len(u0)+len(u1))


# #users who tweets by community
# print(' > users who T by community')
# usr = list(set(df_temp['user_id'].tolist()))
# usr_c0 = list(set(df0['user_id'].tolist()))
# usr_C1 = list(set(df1['user_id'].tolist()))
# print('     c0:', len(usr_c0), '| c1:', len(usr_C1), '  --> ', len(usr), len(usr_c0)+len(usr_C1))

################################################################################## VOTING DT & RF

# p = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/active_LABEL/'
# mod = 'decision_tree'

# print(' > load')
# if(mod == 'random_forest'):
#     print('--random_forest--')
#     liwc_prediction, y_liwc = load_pickle(p+'random_forest_liwc_PREDICT_PROBA.pickle') # shape = {nbs_tree, nbs_sample, nbs_class}
#     tfidf_prediction, y_tfidf = load_pickle(p+'random_forest_tf-idf_PREDICT_PROBA.pickle') # shape = {nbs_tree, nbs_sample, nbs_class}
# else:
#     print('--decision_tree--')
#     liwc_prediction, y_liwc = load_pickle(p+'decision_tree_liwc_PREDICT_PROBA.pickle') # shape = {nbs_sample, nbs_class}
#     tfidf_prediction, y_tfidf = load_pickle(p+'decision_tree_tf-idf_PREDICT_PROBA.pickle') # shape = {nbs_sample, nbs_class}
    
    
# # print(' > compute combination')
# # all_proba = {}
# # for liwc_tree, tf_tree in zip(liwc_prediction, tfidf_prediction):
# #     id_sample=0
# #     for x_liwc, x_tf in zip(liwc_tree, tf_tree):
# #         if(id_sample not in all_proba.keys()):
# #             all_proba[id_sample] = [x_liwc[1], x_tf[1]]
# #         else:
# #             all_proba[id_sample].append(x_liwc[1])
# #             all_proba[id_sample].append(x_tf[1])
# #         id_sample+=1
    
# # mean_proba = []
# # for k,v in all_proba.items():
# #     mean_proba.append(sum(v)/len(v))
        
# # preds = []
# # for m in mean_proba:
# #     if(m>0.5):
# #         preds.append(1)
# #     else:
# #         preds.append(0)
        
# # print(classification_report(y_liwc, preds))


# feat = 'liwc'
# print(' > compute alone', feat, mod)
# all_proba = {}
# if(feat == 'liwc'):
#     tree = liwc_prediction
#     y = y_liwc
# else:
#     tree = tfidf_prediction
#     y = y_tfidf
    
# for t in tree:
#     id_sample=0
#     for x in t:
#         if(id_sample not in all_proba.keys()):
#             all_proba[id_sample] = [x[1]]
#         else:
#             all_proba[id_sample].append(x[1])
#         id_sample+=1
    
# mean_proba = []
# for k,v in all_proba.items():
#     mean_proba.append(sum(v)/len(v))
        
# preds = []
# for m in mean_proba:
#     if(m>0.5):
#         preds.append(1)
#     else:
#         preds.append(0)
        
# print(classification_report(y_liwc, preds))

#----------------------------------------------------------
# temp_path = ut.CURRENT_DATA_FOLD + 'data_objects/ANALYSIS/influencer_LABEL/dict_influencer_005.pickle'

# dict_influencer, dict_stats = load_pickle(temp_path)

# proportion = {}
# for topic, scores in dict_stats.items():
#     pr_inf = scores[0]
#     pr_normal = scores[1]
#     pr_all = scores[2]
    
#     proportion[topic] = round(sum(pr_inf) / sum(pr_all)*100,2)
    
# proportion = {k: v for k, v in sorted(proportion.items(), key=lambda item: item[1])}

# for topic, prop in proportion.items():
#     print(topic, ':', prop, ' -->', ut.data_zarate_labels[topic])
    
    
    
# #------------------------------ get metis label with user who tweets ! v2
# print(' ---- step1')
# topic = 'chloroquine'
# path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
# path_my_data = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/my_data/LIWC/'


# if(os.path.isfile(path_my_data + topic+'_LIWC.csv')):
#     print('!my_data!')
#     df_tweets = load_data_csv(path_my_data + topic+'_LIWC.csv')
# else:
#     df_tweets = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')
#     df_tweets = df_tweets[df_tweets['topic'] == topic]


# t = 'chloroquine'
# p = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+t+'/RETWEET/dict_usr_w_tweets.pickle'
# a = set(df_tweets[df_tweets['metis_label'] == 0]['user_id'].tolist())
# dd = {k:0 for k in a}
# print('1/', len(a))
# a = set(df_tweets[df_tweets['metis_label'] == 1]['user_id'].tolist())
# print('2/', len(a))
# for x in a:
#     dd[x] = 1
# print('> Total:', len(dd))
# pickle_save(p, dd)


# # ##### ######### #####
# print(' ---- step2')

# t = 'chloroquine'
# path_data = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+t+'/RETWEET/'
# _, dict_nodes, dict_edges, _, _, _, dict_id_to_user, _, dict_node_label_metis, _, _, _ = load_pickle(path_data+'data.pickle')
# dict_usr_w_tweets = load_pickle(path_data+'dict_usr_w_tweets.pickle')

# dict_node_label = {}
# for k, v in dict_node_label_metis.items():
#     dict_node_label[dict_id_to_user[k]] = v
    
# for k, v in dict_usr_w_tweets.items():
#     dict_node_label[k] = v+2

# print('go save')    
# to_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+t+'/RETWEET/'
# save_giphi_files(to_path, dict_node_label, dict_edges)
    


# ------------------ plot different scores -----------------

# # path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
# # df_tweets = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')
# # dict_prop = {}
# # for topic in dict_labels.keys():
# #     temp = df_tweets[df_tweets['topic'] == topic]
# #     c0 = temp[temp['metis_label'] == 0]
# #     c1 = temp[temp['metis_label'] == 1]

# #     dict_prop[topic] = round(min(len(c0), len(c1))/max(len(c0), len(c1)), 3)
# # write_json(path, dict_prop)

def print_log(dict_res, dict_labels):
    dict_res = {k: v for k, v in sorted(dict_res.items(), key=lambda item: item[1])}
    for k,v in dict_res.items():
        print(k, ':', v, dict_labels[k])
    print('         >> ROC_AUC : ', get_roc_auc_score(dict_res, dict_labels), '<< \n')

def plot_violin(df, to_plot):
    
    new_df = pd.DataFrame([], columns=['topic', 'type_score', 'score', 'label'])
    
    for the_score in to_plot:
        if(the_score in ['rwc_score', 'acc_bert']):
            for top, row in df.iterrows():
                new_df.loc[len(new_df)] = [top, the_score, row[the_score], int(row['label'])]
        elif(the_score == 'acc_bert_02'):
            for top, row in df.iterrows():
                if(row['proportion'] > 0.2):
                    new_df.loc[len(new_df)] = [top, the_score, row['acc_bert'], int(row['label'])]
        elif(the_score == 'rwc_score_02'):
            for top, row in df.iterrows():
                if(row['proportion'] > 0.2):
                    new_df.loc[len(new_df)] = [top, the_score, row['rwc_score'], int(row['label'])]
    
    # sns.violinplot(data=new_df, x="type_score", y="score", hue="label", split=True)
    sns.swarmplot(data=new_df, x="type_score", y="score", hue="label", dodge=True)

    
    return 1


dict_labels = ut.data_zarate_labels

print('----- proportion c1/c2 ------')
path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/TOPICS/dict_proportion.json'
dict_prop = read_json(path)
# print_log(dict_prop, dict_labels)

print('------- acc BERT -------')
path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/TOPICS/dict_results_by_topic_v2.json'
dict_results = read_json(path)
dict_acc = {k:v['test']['accuracy'] for k,v in dict_results.items() if(dict_prop[k] > 0.2)}
print_log(dict_acc, dict_labels)

# print('------- RWC score -------')
# path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/BASELINE_ALL_v1_ZAR/rwc_score.json'
# dict_results = read_json(path)
# dict_rwc = {k:v['rwc_score'] for k,v in dict_results.items()}
# print_log(dict_rwc, dict_labels)

print('------- RWC + acc BERT score -------')
# dict_comb = {k:v*dict_rwc[k] for k,v in dict_acc.items() } # if(dict_prop[k]>0.2)}
# print_log(dict_comb, dict_labels)

# # print('-------- PLOT VIOLIN ---------')
# df_results = pd.DataFrame({'topic': [k for k in dict_labels.keys()],
#                            'rwc_score': [dict_rwc[k] for k in dict_labels.keys()],
#                            'acc_bert': [dict_acc[k] for k in dict_labels.keys()],
#                            'proportion': [dict_prop[k] for k in dict_labels.keys()],
#                            'label': [int(dict_labels[k]) for k in dict_labels.keys()],
#                            })
# df_results = df_results.set_index('topic')
# plot_violin(df_results, ['rwc_score', 'acc_bert', 'rwc_score_02', 'acc_bert_02'])
# # plot_violin(df_results, ['rwc_score_02', 'acc_bert_02'])


