# Goal
This project aims to expa

Controversy refers to content attracting different point-of-views, as well as positive and negative feedback on a specific event, gathering users into different communities. Research on controversy led to two main categories of works: 
- controversy detection/quantification
- controversy explainability.
This project mainly contributes to the controversy explainability.
We analyze topic discussions on Twitter from the community perspective to investigate the power of text in classifying tweets into the right community. 
We propose a SHAP-based pipeline to quantify impactful text features on predictions of three tweet classifiers. 
The results we obtain from both SHAP plots and statistical analysis show clearly significant impacts of some text features in classifying tweets.

Benslimane, S., Azé, J., Bringay, S., Servajean, M., & Mollevi, C. (2023). A text and GNN based controversy detection method on social media. World Wide Web, 26(2), 799-825.

# Code

- liwc_polarization.py : this file contains the source code to train model and explain communities of a particular topic

- liwc_controversy.py : Does the same, but for the task of controversy (and not communities). The model learn if a tweet belong to a controversial topic or not.
