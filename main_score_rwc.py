#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 13 09:51:22 2022

@author: samy
"""
from sklearn.metrics import roc_auc_score

from sklearn.metrics import silhouette_score

import numpy as np
import pickle
import json
import time
import csv
import os

import util as ut


# from matplotlib import pyplot as plt 
# import numpy as np

import RWC_score_ZARATE as rwc #TODO


def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

def create_csv_files(path, dict_node_label, dict_coordinates, dict_edges):    
    print('node file..')
    node_file = path + 'nodes_from_GNN_QC_SCORE.csv'
    with open(node_file, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(['Id', 'dim1', 'dim2', 'community'])
    
        user_id = 0
        for k,v in dict_node_label.items():
            writer_node.writerow([str(k), str(dict_coordinates[k][0]), str(dict_coordinates[k][1]), str(v)])
            user_id+=1
        print(0, user_id-1, user_id-1, user_id)
    
    print('edge file..')
    maxi = 0
    edge_file = path + 'edges_from_GNN_QC_SCORE.csv'
    t = []
    with open(edge_file, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(['Source','Target'])
        for k,e in dict_edges.items():
            if(maxi<e[0]):
                maxi=e[0]
            elif(maxi<e[1]):
                maxi=e[1]
            t.append(e[0])
            t.append(e[1])
            writer_node.writerow([str(e[0]), str(e[1])])
        t=list(set(t))
        print(min(t), max(t), maxi, len(t))

    return 1

def save_score(result_path, code_xp, topic, type_score, score):
    filename = result_path + code_xp + '.json'
    
    results = {}
    if(os.path.isfile(filename) == True):
        with open(filename, 'r') as f:
            results = json.load(f)
            
    if(topic not in results.keys()):
        results[topic] = {}
        
    results[topic][type_score] = float(score)
    print(results)
    with open(filename, 'w') as f:
      json.dump(results, f)
      
    return results

def read_json(result_path, code_xp):
    filename = result_path + code_xp + '.json'
    
    results = {}
    if(os.path.isfile(filename) == True):
        with open(filename, 'r') as f:
            results = json.load(f)
            
    return results

def get_silhouette_score(dict_coordinates, dict_node_label, metric):
    X = []
    labels = []
    i=0
    for k,v in dict_node_label.items():
        X.append(np.asarray(dict_coordinates[k]))
        labels.append(v)
        i+=1
        
    sil_score = round(silhouette_score(np.stack(X), np.stack(labels), metric='euclidean'), 4)
    print('--> sil_score:', sil_score)
    
    return sil_score

def get_roc_auc_score(dict_accuracy, dict_labels):
    preds = []
    labs = []
    for k,v in dict_accuracy.items():
        preds.append(v)
        labs.append(dict_labels[k])
    
    score = roc_auc_score(labs, preds)
    
    return score
#----------------------------------------------------------------------------------------------------------------------

#---------------------------------
type_sevor = 'local' #jeanzay local
topics = ut.topic_qc_zarate
nbs_random_walks = 1000 #1000 3000
code_xp = 'BASELINE_ALL_v0_ZAR' #'BASELINE_ALL_ZAR' 'S_1A'

type_scores = ['rwc_score'] #ec_score
#---------------------------------

root = ut.CURRENT_DATA_FOLD + ut.object_folder + code_xp + '/'  
result_path = ut.CURRENT_DATA_FOLD + ut.object_folder + code_xp + '/'  
    
type_graph = 'RETWEET'
top_k_highest_degree = 0.02  #1200 300 0.15  #TODO

if __name__ == "__main__":
    print('****************** PARAMETERS *******************')
    print('CODE_XP : ', code_xp)
    print('topics : ', topics)
    print('type_graph : ', type_graph)
    print('rwc parameter : ')
    print('   + nbs_random_walks : ', nbs_random_walks)
    print('   + top_k_highest_degree : ', top_k_highest_degree)
    print('****************   *   ****   *   ******************')
    print('\n')
    
    results = read_json(result_path, 'rwc_score')
    cpt=1
    for topic in topics:
        t2 = time.time()
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>",cpt, '/', len(topics), ':', topic, "<<<<<<<<<<<<<<<<<<<>>>>><<<<<<<<<<<<<")
        if(os.path.isdir(root+topic) == False):
            print('--- Topic does not exist ! ---')
        else:
            print('1. create csv vizu graph files')
            path = root + topic + '/' + type_graph + '/'
            _, dict_nodes, dict_edges, _, _, _, dict_id_to_user, _, dict_node_label_metis, _, _, dict_new_edge_weights = load_pickle(path+'data.pickle')
            
            print(' GRAPH : <<NODES>>', len(dict_nodes), '| <<EDGES>>', len(dict_edges))
            dict_edge_weights = {}
            for k,v in dict_new_edge_weights.items():
                new_k = (dict_id_to_user[k[0]], dict_id_to_user[k[1]])
                dict_edge_weights[new_k] = v
                
            dict_node_label = {}
            for k,v in dict_node_label_metis.items():
                dict_node_label[dict_id_to_user[k]] = v
            
            print('---------------------------- I- Compute RWC score ------------------------------')
            type_score = 'rwc_score'
            if(type_score in type_scores):
                t3 = time.time()
                if(topic in results.keys() and type_score in results[topic]):
                    print('<<', type_score, 'for', topic, 'in xp', code_xp, 'already computed !! >>')
                    print(type_score, ':', results[topic][type_score])
                else:
                    print('> start  FULL timer rwc :' , round(((time.time() - t2)/60), 2), 'min ---')     
                    print('1. get graph G')
                    G = rwc.create_graph_G(dict_edges, dict_edge_weights)
                    
                    print('2. get commu')
                    dict_right, right, dict_left, left = rwc.separate_communites(dict_node_label)
                    
                    print('3. get rwc score')
                    rwc_score = rwc.compute_rwc_score(G, dict_right, right, dict_left, left) #ZARATE
                    # rwc_score = rwc.compute_rwc_score(G, dict_right, right, dict_left, left, nbs_random_walks, top_k_highest_degree) #CLASSIC
                    save_score(result_path, 'rwc_score', topic, type_score, rwc_score)    
                    print('> RWC time :' , round(((time.time() - t3)/60), 2), 'min ---')
            else:
                print('    <<<< no', type_score, '>>>>')
        
        cpt+=1
        print('-----------------------------------------------------------------------------\n')
    

    print('compute roc_auc')
    results = read_json(result_path, 'rwc_score')
    dict_accuracy = {k:v['rwc_score'] for k,v in results.items()}
    roc_auc = get_roc_auc_score(dict_accuracy, ut.data_zarate_labels)
    
    print('$$$$$$$$$$     roc_auc = ', roc_auc, '     $$$$$$$$$$')
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FINISH <<<<<<<<<<<<<<<<<<<>>>>><<<<<<<<<<<<<")
    
    
    