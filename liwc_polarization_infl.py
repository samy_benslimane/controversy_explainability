#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 09:54:26 2022

@author: samy
"""


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import text
from shap.plots import waterfall, force

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import scipy as sp
import pickle
import pickle5
import random
import torch
import time
import json
import shap
import csv
import os

import bert_model_train as bmt
import model_train as mt
import util as ut


"""
get user label
"""
def get_user_node_label(path, label_method, topic):
    filename = path +topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        dict_labels = json.load(f)
        
    labels = [v for k,v in dict_labels.items()]
    n_lab = len(list(set(labels)))
    print('---------- LABEL_QC ----------')
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('-------------------------------')
    
"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1
def pickle_save5(path, data):
    with open(path, 'wb') as file:
        pickle5.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str,
         'metis_label': int, 'LABEL_QC_label': int}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels


def get_random_samples(x1, x2):
    sample_x1 = []
    sample_x2 = []
    
    if(len(x1)<len(x2)):
        min_lab = len(x1)
        idx = list(random.sample(set(range(len(x2))), min_lab))
        
        sample_x2 = [x2[i] for i in idx]
        sample_x1 = x1   
    elif(len(x2)<len(x1)):
        min_lab = len(x2)
        idx = list(random.sample(set(range(len(x1))), min_lab))
        
        sample_x1 = [x1[i] for i in idx]
        sample_x2 = x2
    else:
        min_lab = len(x1)
        sample_x1 = x1
        sample_x2 = x2
        
    return sample_x1, sample_x2


def predictor_list(list_x):
    print('hiiiiiiiiiiihi +- > (0000000000)')
    out = []
    val = []
    for x in list_x:
        o, v = predictor_l(x)
        out.append(o)
        val.append(v)
        
    return out, val

def predictor_l(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()} 
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
        
    outputs, out_embedding = p_model(new_inputs)
        
    outputs = outputs.detach().cpu().numpy()

    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return outputs, val



def predictor(x):
    inputs = tokenizer([x], add_special_tokens=True,
                                        truncation=True,
                                        max_length = 256,  # maximum length of a sentence
                                        pad_to_max_length=True,  # Add [PAD]s
                                        return_attention_mask = True,  # Generate the attention mask
                                        return_tensors="pt")
    inputs = {k:v.type(torch.long).cuda((device[0])) for k,v in inputs.items()}
    new_inputs = [inputs['input_ids'], inputs['attention_mask']]
    
    outputs, out_embedding = p_model(new_inputs)
    
    outputs = outputs.detach().cpu().numpy()
    
    from_logit = outputs[:,1]
    to_logit = []
    for t in from_logit:
        if(t > 0.999):
            to_logit.append(t-0.000000001)
        elif(t < 0.001):
            to_logit.append(t+0.000000001)
        else:
            to_logit.append(t)
    to_logit = np.array(to_logit)
    val = sp.special.logit(to_logit)
    
    return val


def f_batch(x):
    val = np.array([])
    for i in x:
      val = np.append(val, predictor(i))
    return val



#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


topic = '1Direction'
code_label = 'LC_8A'
path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
path_label_qc = ut.CURRENT_DATA_FOLD + ut.object_folder + 'LABELS/' +code_label+'/'+topic+'/RETWEET/'


true_label = ['metis_label', [-10], [0,1]] #['metis_label', [-10], [0,1]], ['LABEL_QC_label', [-20,0], [1,2]]
device = [0]
    
######################################################################################################################################################
#################################################### 2. train models #################################################################################

if __name__ == "__main__":
    print('------------------------------- TRAIN CLASSIFICATION MODEL (influencer c1 v c2):', topic, '------------------------------')
    t1 = time.time()
    
    model = 'bert' #'decision_tree', 'random_forest', 'bert'
    features = 'text' #'tf-idf', 'liwc', 'tf-idf_liwc', 'text', 'text_liwc'
    
    print(' -----', model, ' |', features, '-----')

    root = ut.CURRENT_DATA_FOLD + ut.object_folder + '/ANALYSIS/'+topic+'_inf/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
    root+= true_label[0] + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    temp_path = root + topic+'.pickle'
    if(os.path.isfile(temp_path) == True):
        print(' >> find pickle data file ! ')
        dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels = load_pickle(temp_path)
        print(' > proportion train/test =', round(len(train_tweets)/(len(train_tweets)+len(test_tweets)),3))
    else:
        #1. load data
        print(' 1. load data')
        dict_node_label_sample = {}
        df_tweets = load_data_csv(path_liwc+topic+'_inf_LIWC.csv')
        df_tweets = df_tweets[df_tweets['topic'] == topic]
        
        #2. clean data
        print(' 2. clean data')
        all_labels = set(df_tweets[true_label[0]].tolist())
        print(' > Labels \'', true_label[0] ,'\' :', set(df_tweets[true_label[0]].tolist()))
        for l in all_labels:        
            print('    > lab', l, ':', len(list(set(df_tweets[df_tweets[true_label[0]] == l]['user_id'].tolist()))) ,'users who tweets')
            print('            > nbs tweets:', len(df_tweets[df_tweets[true_label[0]] == l]))
            
        for rm_lab in true_label[1]:
            df_tweets = df_tweets[df_tweets[true_label[0]] != rm_lab]
        df_tweets = df_tweets.rename(columns={"Text": "clean_text"})
        
        #3. prepare data
        print(' 3. prepare data')
        dict_features = {}
        dict_text = {}
        dict_labels = {}
        for idx, row in df_tweets.iterrows():
            t_id = row['tweet_id']
            
            dict_features[t_id] = row[ut.LIWC_FEATURES]
            dict_text[t_id] = row['clean_text']
            dict_labels[t_id] = true_label[2].index(row[true_label[0]]) # dict_labels[t_id] = row[true_label[0]]
            
            
        #4. split dataset
        print(' 4. split dataset (putting manual tweets on test)')
        dict_manual_labels = {}
        train_tweets, test_tweets = bmt.split_train_test_dataset(dict_labels, dict_manual_labels, keep_manual_test=False)

        pickle_save(temp_path, (dict_node_label_sample, df_tweets, dict_features, dict_text, dict_labels, train_tweets, test_tweets, dict_manual_labels))
        print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---')
    
    #--------------------------------------------------------------------------------------------------------------------------
    
    root = root + model + '_' + features + '/'
    if(os.path.isdir(root) == False):
        os.mkdir(root)
        
    ########################################## 5. train BERT model ##########################################
    print(' 5. train model')
    if(model == 'bert'):
        print(' ----', model, '----')
        if(features == 'text'):
            combined_dict_f = None
        elif(features == 'text_liwc'):
            combined_dict_f = {k:v.tolist() for k,v in dict_features.items()}

        p_model, tokenizer = bmt.bert_fine_tuning(train_tweets, test_tweets, [], [], dict_labels, dict_text, combined_dict_f, topic, features, device, root, true_label[0],
                            bert_epochs = 30, bert_batches = 32, max_length = 256, lr_rate = 2e-5,
                            truncate = True, discr_lr = True, k_fold = 'NO', task='metis_label_inf')
        p_model.eval()
            
        print('  5.4 get shap values')
        if(features == 'text_liwc'):
            print(' > no shap !!')
        else:
            
            print('       5.4.0 get test data')   
            if(features == 'text'):
                other_f = None            
                      
            # ------------------------------- # ------------------------------- # -------------------------------
            print('----------- 6. GET SHAP VALUES TRAINING !!!! -----------')
            X, X_train, X_test, y, y_train, y_test, _ = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_text, combined_dict_f)
            print('##### 6.1 on TRAIN #####')
            print('  >> 6.1.1 sampling data over', len(X_train), 'records')
            sample_X = X_train
            sample_y = y_train
            
            sample_X_list = bmt.clean_text_v2(sample_X[0].tolist())
            print(' >1. explainer')
            explainer = shap.Explainer(f_batch, tokenizer)  
            print(' >1bis. shap training', len(sample_X_list))
            shap_values = explainer(sample_X_list, fixed_context=1)
            
            pickle_save(root+'/shap_temp_train.pickle', (shap_values, sample_X, sample_y))
            
            
            print('----------- 6. GET SHAP VALUES TEST !!!! -----------')
            X, X_train, X_test, y, y_train, y_test, _ = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_text, combined_dict_f)
            print('##### 6.1 on TEST #####')
            print('  >> 6.1.1 sampling data over', len(X_test), 'records')
            sample_X = X_test
            sample_y = y_test
            
            sample_X_list = bmt.clean_text_v2(sample_X[0].tolist())
            print(' >1. explainer')
            explainer = shap.Explainer(f_batch, tokenizer)  
            print(' >1bis. shap training', len(sample_X_list))
            shap_values = explainer(sample_X_list, fixed_context=1)
            
            pickle_save(root+'/shap_temp_test.pickle', (shap_values, sample_X, sample_y))

    
    
    ########################################## 5. train OTHER model ##########################################
    elif(model == 'decision_tree' or 'random_forest'):
        print(' ----', model, '----')
        print('  5.1 get features')
        
        if(features == 'liwc'):
            feature_names = ut.LIWC_FEATURES
            dict_f = dict_features
        elif(features == 'tf-idf' or features == 'tf-idf_liwc'):
            my_stop_words = []
            print(' > rm stop words')
            my_stop_words = text.ENGLISH_STOP_WORDS
            vectorizer = TfidfVectorizer(min_df=3, stop_words=my_stop_words)
            print('clean 1')
            train_tf = [bmt.clean_text_v2(dict_text[k], False) for k in train_tweets]
            vectorizer.fit(train_tf)
            
            print('clean 2')
            dict_f = {}
            all_set = [bmt.clean_text_v2(v, False) for k,v in dict_text.items()]
            all_set = vectorizer.transform(all_set)
            cpt=0
            for k,v in dict_text.items():
                if(features == 'tf-idf'):
                    dict_f[k] = all_set[cpt].toarray()[0]
                elif(features == 'tf-idf_liwc'):
                    dict_f[k] = np.array(list(all_set[cpt].toarray()[0]) + list(dict_features[k]))
                cpt+=1
                
            if(features == 'tf-idf'):
                feature_names = vectorizer.get_feature_names() #[str(i) for i in range(len(all_set[0].toarray()[0]))] 
            elif(features == 'tf-idf_liwc'):
                feature_names = vectorizer.get_feature_names() + ut.LIWC_FEATURES #[str(i) for i in range(len(all_set[0].toarray()[0]))]
        print('    > len features:', len(feature_names))

        print('  5.2 get split dataset')
        X, X_train, X_test, y, y_train, y_test, (X_train_idx, X_test_idx) = bmt.get_train_test_set(train_tweets, test_tweets, dict_labels, dict_f, feature_names=feature_names)
        print(' > train shape:', X_train.shape)
        print(' > test shape:', X_test.shape)


        print('  5.3 training')
        temp_path = root + model + '_' + features + '.pickle'
        if(os.path.isfile(temp_path) == True):
            print('- found model')
            importance, clf = load_pickle(temp_path)
        else:
            no = pd.DataFrame()
            clf, importance = mt.train_model(model, X_train, X_test, no, y_train, y_test, no, features, root, data_to_save='test')
            

        print('  5.4 get shap values')
        
        print('      5.4.0 [TRAIN] Explaining over', len(X_train), 'records')
        clf_sample = clf
        sample_X = X_train
        sample_y = y_train
        time_ex = time.time()
        explainer = shap.TreeExplainer(clf_sample, sample_X)
        print(' >1.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_obj = explainer(sample_X, check_additivity=False)
        print(' >2.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_values = explainer.shap_values(sample_X, check_additivity=False)
        print(' >3.', round(((time.time() - time_ex)/60), 2), 'min')
        
        temp_path = root + '/shap_train_ALL.pickle'
        pickle_save(temp_path, (shap_obj, shap_values, sample_X))

        print(' >5.', round(((time.time() - time_ex)/60), 2), 'min')    
        
        
        print('      5.4.1 [TEST] Explaining over', len(X_test), 'records')
        clf_sample = clf
        sample_X = X_test
        sample_y = y_test
        time_ex = time.time()
        explainer = shap.TreeExplainer(clf_sample, sample_X)
        print(' >1.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_obj = explainer(sample_X, check_additivity=False)
        print(' >2.', round(((time.time() - time_ex)/60), 2), 'min') 
        shap_values = explainer.shap_values(sample_X, check_additivity=False)
        print(' >3.', round(((time.time() - time_ex)/60), 2), 'min')
        
        temp_path = root + '/shap_test_ALL.pickle'
        pickle_save(temp_path, (shap_obj, shap_values, sample_X))
        
        print(' >5.', round(((time.time() - time_ex)/60), 2), 'min') 
        
    print(' >>> time run ', round(((time.time() - t1)/60), 2), 'min ---') 
    
    print('-----------------------------------------------------------------------------')