#!/bin/bash

#SBATCH --job-name=job_samy_tweet_analysis # nom du job

#SBATCH  --cpus-per-task=4
#SBATCH --partition=gpu_p2

#SBATCH --gres=gpu:2  # nombre de GPU à réserver
#SBATCH --time=19:00:00             # (HH:MM:SS)
#SBATCH --output="/gpfswork/rech/kdj/unn89uy/TWEET_ANALYSIS/log/test_%j.out"
#SBATCH --error="/gpfswork/rech/kdj/unn89uy/TWEET_ANALYSIS/log/test_%j.err"

#SBATCH -A kdj@v100

module load pytorch-gpu/py3/1.8.0 metis/5.1.0

# echo des commandes lancées
set -x

# exécution shell for all subreddit #SBATCH  --mem=160G
sh "/gpfswork/rech/kdj/unn89uy/TWEET_ANALYSIS/jz_tweet_analysis.sh"

