#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 09:38:20 2022

@author: samy
"""

import bert_model_train as bmt
import util as ut

from deep_translator import GoogleTranslator
from collections import Counter
import seaborn as sns
import pandas as pd
import collections
import random
import pickle
import json
import time
import csv
import html
import os
import re


all_topics = ['bolsonaro27',
              'menciones-20-27marzo',
              'impeachment-5-10',
              'menciones-1-10enero',
              'mothersday', #do not use it
              'area51', 
              'OTDirecto20E',
              'bolsonaro28', 
              'VanduMuruganAJITH',
              'nintendo',
              'messicumple', 
              'wrestlemania',
              'kingjacksonday',
              'kavanaugh06-08',
              'bolsonaro30', 
              'notredam',
              'Thanksgiving',
              'halsey', 
              'feliznatal', 
              'kavanaugh16',
              'kavanaugh02-05',
              'EXODEUX', 
              'bigil', 
              'lula_moro_chats',
              'menciones-05-11abril',
              'LeadersDebate',
              'championsasia', 
              'menciones05-11mayo',
              'pelosi', 
              'SeungWooBirthday',
              'menciones-11-18marzo']
# all_topics = ['pelosi', 'SeungWooBirthday']

code_xp_LABEL_QC = 'LC_8A'

def clean_text_v2(tweet):
    #remove retweet
    tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
    
    #remove URL
    tweet = re.sub(r'http\S+', r'URL', tweet)
    
    # # remove @
    tweet = re.sub(r'@[^\s]+',r'HASHTAG', tweet)
    
    #remove non-ascii words and characters
    # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
    # tweet = ''.join(tweet)
    # tweet = re.sub(r'_[\S]?',r'', tweet)
    # tweet = re.sub(r'\n',r' ', tweet)
    
    # lower case and strip white spaces at both ends
    # tweet = tweet.lower()
    # tweet = tweet.strip()
    
    tweet = html.unescape(tweet)
        
    return tweet

"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'tweet_id': str, 'is_retweet': bool,
         'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
            
    return 1

"""
load json data
"""
def load_json(path):
    with open(path) as f:
        labels = json.load(f)
      
    return labels



# if __name__ == "__main__":
#     print('-------- START 1 --------')
#     cpt=1
#     t1 = time.time()
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + '/data_ZARATE_translated_v3/' + topic + '.csv' #data_ZARATE_translated_v2
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_ZARATE_for_analysis/' + topic + '.csv'
        
#         data_obj_path = ut.CURRENT_DATA_FOLD + ut.object_folder + 'S_1A/' + topic + '/RETWEET/data.pickle'
        
#         label_qc_path = ut.CURRENT_DATA_FOLD + ut.object_folder + 'LABELS/' +code_xp_LABEL_QC + '/' + topic + '/RETWEET/' + topic+'_labels_'+code_xp_LABEL_QC+'.json'
        
#         if(os.path.isfile(to_path) == True):
#             print(' <<<<<< data already created >>>>>>')
#         else:
#             #1. load data
#             print("1. load data")
#             if(os.path.isfile(data_obj_path) == True):
#                 _, dict_nodes, _, dict_new_nodes, _, dict_user_to_id, dict_id_to_user, _, dict_node_label_metis, _, _, _ = load_pickle(data_obj_path)
#                 dict_user_label = {dict_id_to_user[k]:v for k,v in dict_node_label_metis.items()}
                
#             dict_label_unsupervised = {}
#             if(os.path.isfile(label_qc_path) == True):
#                  dict_label_unsupervised = load_json(label_qc_path)
                 
#             if(os.path.isfile(from_path) == True):   
#                 init_df = load_data_csv(from_path)
            
#             final_df = pd.DataFrame({'topic': [],
#                                      'user_id': [],
#                                      'tweet_id': [],
#                                      'is_retweet': [],
#                                      'text': [],
#                                      'metis_label': [],
#                                      'LABEL_QC_label': []})
                        
#             #3. add metis labels
#             print('2. add metis+unsupervised label')
#             not_found_metis = 0
#             unsup_usr = []
            
#             tweets_done = []
            
#             for idx, row in init_df.iterrows():
#                 still = True
#                 if(row['is_retweet'] == False):
#                     u_id = row['user_id']
#                     t_id = row['status_id']
#                     is_rt = row['is_retweet']
#                     text = row['text_translated']
                    
#                     if(t_id in tweets_done):
#                         still = False
#                     else:
#                         #metis label
#                         if(u_id in dict_user_label.keys()):
#                            metis_label = dict_user_label[u_id]
#                         else:
#                             metis_label = -10
#                             not_found_metis+=1
                            
#                         #label_qc
#                         if(u_id in dict_label_unsupervised.keys()):
#                             LABELQC_labels = dict_label_unsupervised[u_id]
#                             unsup_usr.append(u_id)
#                         else:
#                             LABELQC_labels = -20                 
                       
#                 else:
#                     u_id = row['retweet_user_id']
#                     t_id = row['retweet_status_id']
#                     is_rt = row['is_retweet']
#                     text = row['text_translated']
                    
#                     if(t_id in tweets_done):
#                         still = False
#                     else:
#                         #metis label
#                         if(u_id in dict_user_label.keys()):
#                            metis_label = dict_user_label[u_id]
#                         else:
#                             metis_label = -10
#                             not_found_metis+=1
                            
#                         #label_qc
#                         if(u_id in dict_label_unsupervised.keys()):
#                             LABELQC_labels = dict_label_unsupervised[u_id]
#                             unsup_usr.append(u_id)
#                         else:
#                             LABELQC_labels = -20     
                
#                 if(still == True):
#                     final_df.loc[len(final_df)] = [topic, u_id, t_id, is_rt, text, metis_label, LABELQC_labels]
#                     tweets_done.append(t_id)
                                    
#             print(' > NOT found metis label =', not_found_metis)
#             print(' > found unsupervised label =', len(list(set(unsup_usr))))

    
#             # 4. write in files
#             print('4. write result')
#             print('verif size:', len(tweets_done), len(final_df))
#             create_csv_file(final_df, to_path)
            
#         cpt+=1
#         print('--------------------------')
        
#     print('-------- END --------')
            

#####################################################################################################
    
# if __name__ == "__main__":
#     print('-------- START 1: concat --------')
#     cpt=1
#     length = 0
#     final_df = None
    
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/TWEETS_ALL_TOPICS.csv'
    
#     #1. concat results
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/' + topic + '.csv'

#         df = load_data_csv(from_path)
#         length += len(df)
        
#         x =  len(df) == len(list(set(df['tweet_id'].tolist())))
#         print('   >', x)
        
#         if(final_df is None):
#             final_df = df
#         else:
#             final_df = pd.concat([final_df, df])
#         cpt+=1
            
            
#     # 2. write in files
#     print('4. write result')
#     print('verif size:', length, len(final_df))
    
#     create_csv_file(final_df, to_path)
            
#####################################################################################################

# if __name__ == "__main__":
#     print('-------- START 2: clean text --------')
#     cpt=1
#     length = 0
#     final_df = None
        
#     #1. concat results
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis/' + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'tweets_analysis_2/' + topic + '.csv'

#         df = load_data_csv(from_path)
#         new_df = df.copy()
        
#         new_df['clean_text'] = new_df.apply(lambda x: clean_text_v2(x['text']), axis=1)
        
#         cpt+=1
            
#         # 2. write in files
#         print('verif size:', len(df), len(new_df))
    
#         create_csv_file(new_df, to_path)


#####################################################################################################
    
# if __name__ == "__main__":
#     print('-------- START 3: add topic label --------')
#     cpt=1
#     length = 0
#     final_df = None
    
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LIWC.csv'

#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'

#     print('1. load')
#     df = load_data_csv(from_path)
    
#     print('2. apply')
#     df['controversy_LABEL'] = df.apply(lambda x: ut.data_zarate_labels[x['topic']], axis=1)
    
#     print('3. write')
#     create_csv_file(df, to_path)
            

#####################################################################################################


# """
# define active user: high degree (in an out)
# """
# if __name__ == "__main__":
#     print('-------- START 4: add active label --------')
#     cpt=1
#     topic = 'pelosi'
    
#     thresold_set = 7
    
#     object_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/pelosi/RETWEET/data.pickle'
#     plot_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/ANALYSIS/pelosi/'
    
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_LIWC.csv'
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'+topic+'_ACTIVE_LIWC.csv'
    
#     dict_tweets, dict_nodes, dict_edges, _, _, _, _, _, _, _, _, _ = load_pickle(object_path)
    
#     dict_degree_by_user = {}
#     for k,v in dict_edges.items():
#         if(v[0] not in dict_degree_by_user.keys()):
#             dict_degree_by_user[v[0]] = 0
#         dict_degree_by_user[v[0]] += 1
        
#         if(v[1] not in dict_degree_by_user.keys()):
#             dict_degree_by_user[v[1]] = 0
#         dict_degree_by_user[v[1]] += 1
    
#     print('>>> step1: get the activ person thresold : DONE')
#     list_distribution = [v for _,v in dict_degree_by_user.items()]
#     list_uniq_degree = list(set(list_distribution))
#     list_distribution_inf = [v for _,v in dict_degree_by_user.items() if v<=thresold_set]
#     list_distribution_sup = [v for _,v in dict_degree_by_user.items() if v>thresold_set]
#     print('  ** <=7:', len(list_distribution_inf), '| >7:', len(list_distribution_sup), '| all:', len(list_distribution))
    
#     # dict_uniq = Counter(list_distribution_sup)
#     # dict_uniq = collections.OrderedDict(sorted(dict_uniq.items()))
#     # list_count_uniq = [v for _,v in dict_uniq.items()]
#     # list_keys = list(dict_uniq.keys())
    
    
#     # plot = sns.histplot(x=list_keys, y=list_count_uniq)
#     # fig = plot.get_figure()
#     # fig.savefig(plot_path+'histogram_100.png') 
    
#     # # plot = sns.ecdfplot(y=list_distribution_sup)
#     # # fig = plot.get_figure()
#     # # fig.savefig(plot_path+'cum_distribution_066.png') 
    
#     # # plot = sns.ecdfplot(y=list_uniq_degree)
#     # # fig = plot.get_figure()
#     # # fig.savefig(plot_path+'cum_uniq_distribution_066.png') 
    
    
#     print('>>> step 2: write active tweets from users: Done')
#     # print('1. load')
#     # df = load_data_csv(from_path)
#     # print('2. apply')
#     # df['nbs_degree'] = df.apply(lambda x: dict_degree_by_user[x['user_id']] if(x['user_id'] in dict_degree_by_user) else -30, axis=1)
#     # df['is_user_active'] = df.apply(lambda x: True if(x['nbs_degree'] > thresold_set) else False, axis=1)
#     # print('3. write')
#     # create_csv_file(df, to_path)


#####################################################################################################
    
# #5 colimn to delete, from 126 to 121
# if __name__ == "__main__":
#     print('-------- START 5: filter data for thomas, 1 tweet by user (independance) --------')
#     cpt=1
#     length = 0
#     final_df = None
#     file = 'pelosi_ACTIVE_LIWC' #'TWEETS_ALL_TOPICS_LABELLED_LIWC' / 'pelosi_LIWC'
#     label = ['nbs_degree', [-30]] #['controversy_LABEL', []] / ['metis_label', [-10]] \ ['metis_label', [-10]]
#     sample_by = 'MEAN' #'UNIQUE' 'MEAN'
    
#     from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/' + file+'.csv' 
#     to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/'+file+'_thomas_'+sample_by+'.csv'

#     print('1. load')
#     df = load_data_csv(from_path)
#     print('  >> after 1:', len(list(set(df['user_id'].tolist()))), '(', len(df), ')')
    
#     print('2. remove not labelled data')
#     if(len(label[1]) != 0): #we have to delete data
#         print(' lets remove!')
#         df = df[df[label[0]] != label[1][0]]
#     print('  >> after 2:', len(list(set(df['user_id'].tolist()))), '(', len(df), ')')
    
#     if(sample_by == 'MEAN'):
    
#         print('3. kept column')
#         features_and_label = ut.LIWC_FEATURES + [label[0]] + ['is_user_active']
#         features_kept = ['topic', 'user_id'] + features_and_label
#         df = df[features_kept]
#         print('  >> after 3:', len(list(set(df['user_id'].tolist()))))
    
#         print('4B. apply one random tweet by user (my function)')
#         temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/temp_MEAN.pickle'
#         new_df = bmt.get_one_sample_by_user(df, temp_path, label[0], features_and_label, sample_by = sample_by)
#         print('  >> after 4B:', len(new_df))
        
#     elif(sample_by == 'UNIQUE'):    
#         print('4B. apply one random tweet by user (my function)')
#         temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/temp_UNIQUE.pickle'
#         new_df = bmt.get_one_sample_by_user(df, temp_path, label[0], features_and_label = None, sample_by = sample_by)
#         print('  >> after 4B:', len(new_df))
        
#         print('3. kept column')
#         features_and_label = ut.LIWC_FEATURES + [label[0]]
#         features_kept = ['topic', 'user_id'] + features_and_label
#         new_df = new_df[features_kept]
#         print('  >> after 3:', len(list(set(new_df['user_id'].tolist()))))
    
        

#     print('5. write')
#     create_csv_file(new_df, to_path)
    
    
#####################################################################################################


# if __name__ == "__main__":
#     print('-------- START 6: get nbs rt by tweet --------')
#     cpt=1
#     final_df = None
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + '/data_ZARATE_translated_v3/' + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/rt_by_tweet.pickle'
        
#         df = load_data_csv(from_path)
#         df = df[df['retweet_status_id'].isnull() == False]
#         df['topic'] = [topic] * len(df)
                
#         if(final_df is None):
#             final_df = df
#         else:
#             final_df = pd.concat([final_df, df])
#         cpt+=1
            
#     count_df = final_df.groupby(['topic', 'retweet_status_id']).size().reset_index(name='counts')


#     pickle_save(to_path, count_df)
    
#     print('<finish>')



# path_liwc = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/'
# df_tweets = load_data_csv(path_liwc+'TWEETS_ALL_TOPICS_LABELLED_LIWC.csv')

# count_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/rt_by_tweet.pickle'
# count_df = load_pickle(count_path)


# topk_df = count_df.groupby('topic',group_keys=False)\
#             .apply(lambda grp:grp.nlargest(n=2000,columns='counts').sort_index())
# topk_df = topk_df.rename(columns={"retweet_status_id": "tweet_id"})
# # yyy = topk_df.groupby('topic')['counts'].min()
        
            
# new_df_tweets = df_tweets.set_index(['topic', 'tweet_id'])
# new_topk_df = topk_df.set_index(['topic', 'tweet_id'])

# result = pd.concat([new_df_tweets, new_topk_df], axis=1, join="inner").reset_index()
# result = result.drop('counts', axis=1)


#####################################################################################################

aggr = 'MEAN'
if __name__ == "__main__":
    print('-------- START 7: sample by topic a small % --------')

    from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/TWEETS_ALL_TOPICS_LABELLED_LIWC_thomas_'+aggr+'.csv' 
    to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/LIWC_thomas/sampled/TWEETS_ALL_TOPICS_LABELLED_LIWC_thomas_'+aggr+'_10.csv' 
    
    df = load_data_csv(from_path)
    nbs_topic = len(list(set(df['topic'].tolist())))

    grouped = df.groupby('topic')
    new_df = grouped.apply(lambda x: x.sample(frac=0.1)).reset_index(drop=True)

    print('5. write')
    create_csv_file(new_df, to_path)
        
        