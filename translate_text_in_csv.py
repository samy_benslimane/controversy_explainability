#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  5 15:01:45 2022

@author: samy
"""

import util as ut

from deep_translator import GoogleTranslator, exceptions
import pandas as pd
import random
import pickle
import time
import copy
import nltk
nltk.download('punkt')

import csv
import os

all_topics = ['bolsonaro27',
              'menciones-20-27marzo',
              'impeachment-5-10',
              'menciones-1-10enero',
              # 'mothersday', #do not use it
              'area51', 
              'OTDirecto20E',
              'bolsonaro28', 
              'VanduMuruganAJITH',
              'nintendo',
              'messicumple', 
              'wrestlemania',
              'kingjacksonday',
              'kavanaugh06-08',
              'bolsonaro30', 
              'notredam',
              'Thanksgiving',
              'halsey', 
              'feliznatal', 
              'kavanaugh16',
              'kavanaugh02-05',
              'EXODEUX', 
              'bigil', 
              'lula_moro_chats',
              'menciones-05-11abril',
              'LeadersDebate',
              'championsasia', 
              'menciones05-11mayo',
              'pelosi', 
              'SeungWooBirthday',
              'menciones-11-18marzo']

topic_to_translate = ['impeachment-5-10',
                    'menciones-1-10enero',
                    'menciones-20-27marzo',
                    'OTDirecto20E',
                    'bolsonaro27',
                    'bolsonaro28',
                    'messicumple',
                    'bolsonaro30',
                    'notredam',
                    'feliznatal',
                    'lula_moro_chats',
                    'menciones-05-11abril',
                    'championsasia',
                    'menciones05-11mayo',
                    'SeungWooBirthday',
                    'menciones-11-18marzo']


"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data


"""
translate texts
"""
def translate_texts(inputs, path_temp_object, batch_size=1000):
    
    if(os.path.isfile(path_temp_object) == True):    
       txt_batches, translated_text, next_index  = load_pickle(path_temp_object)
       print(' >> load data object ! From index', next_index, '/', len(txt_batches))
    else:
        txt_batches = []
        s=0
        e=batch_size
        for i in range(int(len(inputs)/batch_size)+1):
            if(e>len(inputs)):
                batch = inputs[s:]
            else:
                batch = inputs[s:e]      
            s+=batch_size
            e+=batch_size
            txt_batches.append(batch)
            
        translated_text = []
        next_index=0
        
 
    print('    > we translate (', len(txt_batches) ,'batches )...')
    for x in range(next_index, len(txt_batches)):
        inp = txt_batches[x]
        inp = [nltk.tokenize.sent_tokenize(i)[0] for i in inp]
        # idx_over = []
        # final_imp = []
        # for i in range(len(inp)):
        #     if(len(inp[i]) > 5000):
        #         idx_over.append(i)
        #         print('xxxx:', inp[i])
        #     else:
        #         final_imp.append(inp[i])
        # if(len(idx_over) > 0):
        #     print(' > hop,', len(idx_over), 'idx over 5000')
        final_imp = inp
            
        try:
            res = GoogleTranslator(source='auto', target='en').translate_batch(final_imp)
        except (exceptions.NotValidPayload, exceptions.TranslationNotFound) as e:
            print("error batch", x, ':', e)
            res = final_imp
            
        # final_res = []
        # for r in range(len(res)):
        #     if(r in idx_over):
        #         final_res[r] = inp[r]
        #     else:
        #         final_res[r] = res[r]
        final_res = res


        translated_text+=final_res        
        next_index+=1        
        pickle_save(path_temp_object, (txt_batches, translated_text, next_index))
        
        if(len(txt_batches)<100 or next_index%int((len(txt_batches)/100))==0):
                    print("    loading.. ", round((next_index/len(txt_batches))*100, 2), '%')
    
    print('---- verif it is everything ok -------')
    print(len(inputs), '=', len(translated_text))
       
    test = random.randint(0, len(inputs)-1)
    print('index', test, ':', inputs[test], '----->', translated_text[test])
    print('------------------')
    
    return translated_text


"""
load data from csv to dataframe
"""
def load_data_csv(path_file):
    t = {'user_id': str, 'status_id': str, 'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)

# """
# export dataframe to csv file
# """       
# def write_data_csv(df, filename):
#     df.to_csv(filename, index=False)


# if __name__ == "__main__":
#     print('-------- START --------')
#     cpt=1
#     t1 = time.time()
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v2/' + topic + '.csv'
#         data_temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_temp_objects/' + topic + '.pickle'

#         if(os.path.isfile(to_path) == True):
#             print(' >>> topic already translated !')
#         else:
#             dataframe = load_data_csv(from_path)
            
#             if(topic not in topic_to_translate):
#                 print(' >>> !TOPIC NOT TRANSLATED !')
#                 dataframe['text_translated'] = dataframe['text']
#             else:
#                 print('start translate...')
                
#                 list_translated = translate_texts(dataframe['text'].tolist(), data_temp_path)
#                 dataframe['text_translated'] = list_translated
            
#                 print('end translate...')
                
#             create_csv_file(dataframe, to_path)
            
#             print(' **', round(((time.time() - t1)/60), 2), 'min *') 
#             t1 = time.time()
        
#         cpt+=1
#         print('--------------------------')
        
#     print('-------- END --------')


"""
export dataframe to csv file
"""       
def write_data_csv(df, filename):
    df.to_csv(filename, index=False)


if __name__ == "__main__":
    print('-------- START translate the one already in english --------')
    t1 = time.time()

    from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LABELLED_LIWC.csv'
    to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'LIWC/TWEETS_ALL_TOPICS_LABELLED_LIWC_alltranslated.csv'
    data_temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_temp_objects/ALL_TOPICS.pickle'

    if(os.path.isfile(to_path) == True):
        print(' >>> topic already all translated !')
    else:
        dataframe = load_data_csv(from_path)
        dataframe['Text_v2'] = dataframe['Text']
        
        new_df = dataframe[~dataframe['topic'].isin(topic_to_translate)]
        list_topic = set(new_df['topic'].tolist())
        print('topics todo:', list_topic)
        
        print('start translate...')
        
        list_translated = translate_texts(new_df['Text'].tolist(), data_temp_path)
        new_df['Text_v2'] = list_translated
        
        print('end translate...')
            
        # put in full file
        print(' > put in full file')
        cpt=0
        for idx, row in new_df.iterrows():
            dataframe.at[idx,'Text_v2'] = row['Text_v2']
            cpt+=1
            
        print('updated', cpt, '/', len(new_df), '!!!')
        create_csv_file(dataframe, to_path)        

    print('--------------------------')
    print(' ** FINAL ', round(((time.time() - t1)/60), 2), 'min *')   
    print('-------- END --------')
    
    
# if __name__ == "__main__":
#     print('-------- START --------')
#     cpt=1
#     t1 = time.time()
#     for topic in all_topics:
#         print('-----', cpt, '/', len(all_topics), ':', topic, '-----')
#         from_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + topic + '.csv'
#         temp_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v2/' + topic + '.csv'
#         to_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE + 'data_ZARATE_translated_v3/' + topic + '.csv'

#         init_df = load_data_csv(from_path)
#         trans_df = load_data_csv(temp_path)
#         init_df['text_translated'] = trans_df['text_translated']
        
#         create_csv_file(init_df, to_path)
            
#         cpt+=1
#         print('--------------------------')
        
#     print('-------- END --------')